
let PRODUCT_CONFIG = {};

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
]

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const STITCH_JCQ = '0000FF';
const STITCH_WDE = 'FF0000';
const STITCH_CLR = '00FF00';

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_MARINA_BLUE = '35469d'; // ceramic blue
const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
const COLOR_ORANGE = 'df6536'; // orange
const COLOR_FINE_FLANNEL = '737474'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];


// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_LEVIS_BLACK : {
        name: 'Black',
        feeder: 1,
        value: COLOR_BLACK_SWAN,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_MARINA_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_MARINA_BLUE,
    },
    COLOR_CLASSIC_CAMEL : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_CLASSIC_CAMEL,
    },
    COLOR_ORANGE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_ORANGE,
    },
    COLOR_FINE_FLANNEL : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_FINE_FLANNEL,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
}

const PRODUCTS = {

    'default' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                group: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'stitch',
                        value: 'Front_Back_stitch.png',
                        key: 'front-stitch'
                    },

                ],
            },
            {
                name: 'Back',
                key: 'back',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'Front_Back_stitch.png',
                        key: 'back-stitch'
                    },

                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'Sleeve_stitch.png',
                        key: 'sleeve-stitch'
                    },
                
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'Rib_Collar_stitch.png',
                        key: 'collar-stitch'
                    },

                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'Cuff_stitch.png',
                        key: 'cuff-stitch'
                    },

                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'Rib_Collar_stitch.png',
                        key: 'rib-stitch'
                    },

                ],
            },
        ],
        ui: [

            {
                name: 'Test Color',
                key: 'test',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
            },

            {
                name: 'Front Image',
                key: 'front-image',
                group: ['front'],
                rules: {
                    unique: true,
                },
                type: 'image-upload',

            },
        ],
    },


   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    // if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || 'default';
    getProductConfig(handle);

})();