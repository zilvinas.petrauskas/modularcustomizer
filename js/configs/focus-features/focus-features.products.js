
let PRODUCT_CONFIG = {};

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
]

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_NAVY_BLUE = '2d2b39'; // ceramic blue
const COLOR_ICY_LAKE = 'ccd5d8'; // brown
const COLOR_MISTY_LAKE = '6a8590'; // orange
const COLOR_SUNFLOWER = 'e8b023'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_NAVY_BLUE,
    COLOR_ICY_LAKE,
    COLOR_MISTY_LAKE,
    COLOR_SUNFLOWER,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];


// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_BLACK_SWAN : {
        name: 'Black',
        feeder: 1,
        value: COLOR_BLACK_SWAN,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_NAVY_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_NAVY_BLUE,
    },
    COLOR_ICY_LAKE : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_ICY_LAKE,
    },
    COLOR_MISTY_LAKE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_MISTY_LAKE,
    },
    COLOR_SUNFLOWER : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_SUNFLOWER,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
}

const PRODUCTS = {

    '3C' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: '3C_M0001_Focus_Features_Emma_F.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'front-logo',
                        group: 'logo',
                        type: 'color',
                        value: COLOR_SUNFLOWER,
                    },                    
                    {
                        key: 'front-shadow',
                        group: 'shadow',
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                    },
                    {
                        key: 'front-silhouette',
                        group: 'silhouette',
                        type: 'color',
                        value: COLOR_MISTY_LAKE,
                    },
                    {
                        key: 'front-sky',
                        group: 'sky',
                        type: 'color',
                        value: COLOR_ICY_LAKE,
                    },  

                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: '3C_M0001_Focus_Features_Emma_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'back-shadow',
                        group: 'shadow',
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                    },
                    {
                        key: 'back-silhouette',
                        group: 'silhouette',
                        type: 'color',
                        value: COLOR_MISTY_LAKE,
                    },
                    {
                        key: 'back-sky',
                        group: 'sky',
                        type: 'color',
                        value: COLOR_ICY_LAKE,
                    },                    

                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: '3C_M0001_Focus_Features_Emma_Slv.png',
                        key: 'sleeve-texture'
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Sleeve_Stitch.png',
                        key: 'sleeve-stitch',
                    },
                    {
                        key: 'sleeve-color',
                        group: 'shadow',
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [

                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'collar-stitch',
                    },
                   {
                        key: 'default',
                        type: 'color',
                        group: 'shadow',
                        value: COLOR_NAVY_BLUE,
                   },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                   {
                       type: 'stitch',
                       value: '2C_M0002_Cuff_Stitch.png',
                       key: 'cuff-stitch',
                   },
                   {
                        key: 'default',
                        group: 'shadow',
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'rib-stitch',
                    },
                    {
                        key: 'default',
                        group: 'shadow',
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                    },
                ],
            },
        ],
        ui: [
            {
                name: 'Logo Color',
                key: 'body',
                group: ['logo'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
            },

            {
                name: 'Color 1',
                key: 'sky',
                group: ['sky'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
            },

            {
                name: 'Color 2',
                key: 'silhouette',
                group: ['silhouette'],
                value: ALL_COLORS,
                rules: {
                    // unique: true,
                },
            },

            {
                name: 'Color 3',
                key: 'shadow',
                group: ['shadow'],
                value: ALL_COLORS,
                rules: {
                    // unique: true,
                },
            },
        ],
    },


    '2C' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: '2C_M0001_Focus_Features_F.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'front-color',
                        type: 'color',
                        group: 'body',
                        value: COLOR_WHITE_COTTON,
                    },
                    {
                        key: 'front-logo',
                        type: 'color',
                        group: 'logo',
                        value: COLOR_BLACK_SWAN,
                    }

                ],
            },
            {
                name: 'Back',
                key: 'back',
                group: 'body-back',
                layers: [
                    {
                        type: 'texture',
                        value: '2C_M0001_Focus_Features_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'back-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                    },
                    {
                        type: 'text',
                        key: 'back-letter',
                        group: 'logo',
                    },
                    

                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: '2C_M0001_Focus_Features_Slv.png',
                        key: 'sleeve-texture'
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Sleeve_Stitch.png',
                        key: 'sleeve-stitch',
                    },
                    {
                        key: 'sleeve-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [

                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'collar-stitch',
                    },
                   {
                       type: 'collar-color',
                       type: 'color',
                       key: 'default',
                       value: COLOR_WHITE_COTTON,
                   },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                   {
                       type: 'stitch',
                       value: '2C_M0002_Cuff_Stitch.png',
                       key: 'cuff-stitch',
                   },
                   {
                        key: 'default',
                        group: 'body',
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'rib-stitch',
                    },
                    {
                        key: 'default',
                        group: 'body',
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                    },
                ],
            },
        ],
        ui: [
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
            },

            {
                name: 'Logo Color',
                key: 'logo',
                group: ['logo'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
            },
            {
                name: 'Back Letters',
                key: 'back-letter',
                group: ['body-back'], // body-back, body-sleeve
                value: '?', // default string value?
                type: 'input',
                rules: {
                    limit: 7,
                    // color: COLOR_RED_HOT,
                    // stitch: STITCH_WDE,
                    required: true, 
                    position: { 
                        x: 50,
                        y: 25, 
                    },
                    font: '900 40px Arial', // use html font styling convention
                    uppercase: true, // if user inputed text should be transformed to uppercase letters
                }
            },
        ],
    },

   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    // if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || '2C';
    getProductConfig(handle);

})();