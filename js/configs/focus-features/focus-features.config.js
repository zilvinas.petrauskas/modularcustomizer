const WEBSITE_CONFIG = {

    type: 'focus-features',

    customizerCanvasParent: '.customizerCanvasParent', /* parent element of customizer CANVAS */
    customizeProductButton: '#product-customize-button', /* button in shopify template that shows customizer */

    lowersCanvasHeight : [ // put all elements that influence canvas height and subtract their height from 100vh
        '#site-header',
        '.top-banner-container',
    ],

};
