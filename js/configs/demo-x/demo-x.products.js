
let PRODUCT_CONFIG = {};

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
    {
        type: 'stitch_text',
        value: 'stitch_jacquard.png',
        key: 'JCQ_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_wide.png',
        key: 'WDE_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_tuck.png',
        key: 'TCK_text',
    }
]

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_MARINA_BLUE = '35469d'; // ceramic blue
const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
const COLOR_ORANGE = 'df6536'; // orange
const COLOR_FINE_FLANNEL = '737474'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const COLOR_NAVY_BLUE = '262847';

const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];

const COLORS_FOR_X3 = [
    COLOR_NAVY_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
]

// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_BLACK_SWAN : {
        name: 'Black',
        feeder: 1,
        value: COLOR_BLACK_SWAN,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_MARINA_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_MARINA_BLUE,
    },
    COLOR_CLASSIC_CAMEL : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_CLASSIC_CAMEL,
    },
    COLOR_ORANGE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_ORANGE,
    },
    COLOR_FINE_FLANNEL : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_FINE_FLANNEL,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
    COLOR_NAVY_BLUE : {
        name: 'Navy Blue',
        feeder: 9,
        value: COLOR_NAVY_BLUE,
    },
}

const PRODUCTS = {

    'X1' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'X1_F_B.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'front-stitch'
                    },

                    {
                        key: 'front-ornament',
                        group: 'ornament',
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                    },
                    {
                        key: 'front-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                    }
                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'X1_F_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'back-ornament',
                        group: 'ornament',
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                    },
                    {
                        key: 'back-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                    }
                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: 'X1_SLV.png',
                        key: 'sleeve-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'sleeve-stitch'
                    },
                    {
                        key: 'sleeve-ornament',
                        group: 'ornament',
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                    },
                    {
                        key: 'sleeve-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                    }
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default',
                        group: 'collar',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'collar-stitch'
                    },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default', // or default-color 
                        group: 'cuff',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'cuff-stitch', // default-stitch ??
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default',
                        group: 'rib',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'rib-stitch'
                    },
                ],
            },
        ],
        ui: [
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            },
            {
                name: 'Ornament Color',
                key: 'ornament',
                group: ['ornament'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            }
        ],
    },

    'X2' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'X2_F.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'front-body-color',
                        value: COLOR_BLACK_SWAN,
                        group: 'body',
                    },
                    {
                        key: 'letters',
                        group: 'letters',
                        value: COLOR_WHITE_COTTON,
                    },

                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'X2_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'back-body-color',
                        value: COLOR_BLACK_SWAN,
                        group: 'body',
                    },
                    {
                        key: 'letters',
                        group: 'letters',
                        value: COLOR_WHITE_COTTON,
                    },
                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: 'X2_SLV.png',
                        key: 'sleeve-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'sleeve-stitch'
                    },
                    {
                        key: 'sleeve-body-color',
                        group: 'body',
                        value: COLOR_BLACK_SWAN,
                    },
                    {
                        key: 'letters',
                        group: 'letters',
                        value: COLOR_WHITE_COTTON,
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                        key: 'default',
                        group: 'collar',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'collar-stitch'
                    },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                        key: 'default', // or default-color 
                        group: 'cuff',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'cuff-stitch', // default-stitch ??
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_BLACK_SWAN,
                        key: 'default',
                        group: 'rib',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'rib-stitch'
                    },
                ],
            },
        ],
        ui: [
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules:{
                    unique: true
                }
            },
            {
                name: 'Text Color',
                key: 'letters',
                group: ['letters'],
                value: ALL_COLORS,
                rules: {
                    unique: true
                }
            }
        ],
    },

    'X3' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'X3_F.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'upper-line',
                        group: 'upper',
                        value: COLOR_RED_HOT,
                    },
                    {
                        key: 'lower-line',
                        group: 'lower',
                        value: COLOR_NAVY_BLUE,
                    },

                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'X3_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'upper-line',
                        group: 'upper',
                        value: COLOR_RED_HOT,
                    },
                    {
                        key: 'lower-line',
                        group: 'lower',
                        value: COLOR_NAVY_BLUE,
                    },
                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: 'X3_SLV.png',
                        key: 'sleeve-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'sleeve-stitch'
                    },
                    {
                        key: 'upper-line',
                        group: 'upper',
                        value: COLOR_RED_HOT,
                    },
                    {
                        key: 'lower-line',
                        group: 'lower',
                        value: COLOR_NAVY_BLUE,
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'upper',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_RED_HOT,
                        key: 'default',
                        group: 'collar',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'collar-stitch'
                    },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'lower',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                        key: 'default', // or default-color 
                        group: 'cuff',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'cuff-stitch', // default-stitch ??
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'lower',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_NAVY_BLUE,
                        key: 'default',
                        group: 'rib',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'rib-stitch'
                    },
                ],
            },
        ],
        ui: [

            /* COLORS_FOR_X3 */ 
            {
                name: 'Upper Stripe',
                key: 'upper',
                group: ['upper'],
                value: COLORS_FOR_X3,
                rules: {
                    unique: true,
                }
            },
            {
                name: 'Lower Stripe',
                key: 'lower',
                group: ['lower'],
                value: COLORS_FOR_X3,
                rules: {
                    unique: true,
                }
            },


        ],
    },

    'X4' : {
        model: {
            name: 'demo-sweater'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'X4_F_B.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'body-color',
                        group: 'body',
                        value: COLOR_WHITE_COTTON,
                    },
                    {
                        key: 'letter-color',
                        group: 'letters',
                        value: COLOR_BLACK_SWAN,
                    },
                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'X4_F_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'body-color',
                        group: 'body',
                        value: COLOR_WHITE_COTTON,
                    },
                    {
                        key: 'letter-color',
                        group: 'letters',
                        value: COLOR_BLACK_SWAN,
                    },
                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: 'X4_SLV.png',
                        key: 'sleeve-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'blue.png',
                        key: 'sleeve-stitch'
                    },
                    {
                        key: 'body-color',
                        group: 'body',
                        value: COLOR_WHITE_COTTON,
                    },
                    {
                        key: 'letter-color',
                        group: 'letters',
                        value: COLOR_BLACK_SWAN,
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                        key: 'default',
                        group: 'collar',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'collar-stitch'
                    },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                        key: 'default', // or default-color 
                        group: 'cuff',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'cuff-stitch', // default-stitch ??
                    },
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'color',
                        value: COLOR_WHITE_COTTON,
                        key: 'default',
                        group: 'rib',
                    },
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'rib-stitch'
                    },
                ],
            },
        ],
        ui: [
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true
                }
            },
            {
                name: 'Text Color',
                key: 'letters',
                group: ['letters'],
                value: ALL_COLORS,
                rules: {
                    unique: true
                }
            }
        ],
    },
    
   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    // if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || 'X1';
    console.log('-=-=-=-=-=',handle,'=-=-=-=-=-');
    getProductConfig(handle);

})();