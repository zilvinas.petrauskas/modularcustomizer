const WEBSITE_CONFIG = {

    type: 'demo-x',

    customizerCanvasParent: '.customizerCanvasParent', /* parent element of customizer CANVAS */
    customizeProductButton: '#product-customize-button', /* button in shopify template that shows customizer */

};
