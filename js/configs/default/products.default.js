
let PRODUCT_CONFIG = {};

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
    {
        type: 'stitch_text',
        value: 'stitch_jacquard.png',
        key: 'JCQ_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_wide.png',
        key: 'WDE_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_tuck.png',
        key: 'TCK_text',
    }
]

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_MARINA_BLUE = '262847'; // darkish blue
const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
const COLOR_ORANGE = 'df6536'; // orange
const COLOR_FINE_FLANNEL = '737474'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];

// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_BLACK_SWAN : {
        name: 'Black',
        feeder: 1,
        value: COLOR_BLACK_SWAN,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_MARINA_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_MARINA_BLUE,
    },
    COLOR_CLASSIC_CAMEL : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_CLASSIC_CAMEL,
    },
    COLOR_ORANGE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_ORANGE,
    },
    COLOR_FINE_FLANNEL : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_FINE_FLANNEL,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
}

const PRODUCTS = {
    
    
    // Chevron Stripe Pillow
    // pillow with arrow facing down
    /*
        Customizations
        - Top stripe color (white)
        - Bottom stripe color (blue/dark)
        - Front arrow color
        - Back arrow color
        - 4 unique colors
        - front arrow cant be green
    */
    'CCP001' : {
        model: {
            name: 'calamigos-pillow',
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        key: 'front-texture',
                        value: 'CCp001_F.png',
                    },
                    {
                        type: 'stitch',
                        key: 'front-stitch',
                        value: 'pillow01_stitch.png'
                    },
                    {
                        type: 'color',
                        key: 'top-stripe',
                        value: 'f7f8e6',
                        group: 'top',
                    },
                    {
                        type: 'color',
                        key: 'bottom-stripe',
                        value: '262847',
                        group: 'bottom'
                    },
                    {
                        type: 'color',
                        key: 'front-arrow',
                        value: 'f26522',
                        group: 'front-arrow',
                    },
                    
                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        key: 'back-texture',
                        value: 'CCp001_B.png',
                    },
                    {
                        type: 'stitch',
                        key: 'back-stitch',
                        value: 'pillow01_stitch.png'
                    },
                    {
                        type: 'color',
                        key: 'top-stripe',
                        value: 'f7f8e6',
                        group: 'top',
                    },
                    {
                        type: 'color',
                        key: 'bottom-stripe',
                        value: '262847',
                        group: 'bottom'
                    },
                    {
                        type: 'color',
                        key: 'back-arrow',
                        value: 'a07e5e',
                        group: 'back-arrow',
                    }
                ],
            },
            {
                name: 'Flap',
                key: 'flap',
                group: 'body',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'flap-stitch'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: 'f7f8e6'
                    },
                ],
            }
        ],
        ui: [

            {
                name: 'Top Stripe Color',
                key: 'top-stripe',
                group: ['top','body'],
                value: ALL_COLORS,
                rules: {
                    unique: true, // unique color, cant be used on other parts
                }
                
            },
            {
                name: 'Bottom Stripe Color',
                key: 'bottom-stripe',
                group: ['bottom'],
                value: ALL_COLORS,
                rules: {
                    unique: true, 
                }
                
            },
            {
                name: 'Front Arrow Color',
                key: 'front-arrow',
                group: ['front-arrow'],
                value: ALL_COLORS,
                rules: {
                    unique: true, 
                    excluded: [ // which colors are excluded from list (value)
                        COLOR_PINE_FOREST
                    ], 
                }
                
            },
            {
                name: 'Back Arrow Color',
                key: 'back-arrow',
                group: ['back-arrow'],
                value: ALL_COLORS,
                rules:  {
                    unique: true, 
                }
                
            },
        ],
    },

    
     // Forest Stripes Pillow
     // pillow with 3 stripes
    /*
        Customizable
        - Body color
        - Top stripe color + Bottom stripe color
        - 3 different colors active at any time, no color duplication
        - bottom stripe/edge color cant be green
    */
    'CCP002' : {
        model: {
            name: 'calamigos-pillow',
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        key: 'front-texture',
                        value: 'ccp002_f_b.png',
                    },
                    {
                        type: 'stitch',
                        key: 'front-stitch',
                        value: 'pillow01_stitch.png'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: '0d331f',
                        group: 'body',
                    },
                    {
                        key: 'outer-line',
                        type: 'color',
                        value: 'f15d29',
                        group: 'outer'
                    },
                    {
                        key: 'inner-line',
                        type: 'color',
                        value: 'a11d2d',
                        group: 'inner'
                    },
                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        key: 'back-texture',
                        value: 'ccp002_f_b.png',
                    },
                    {
                        type: 'stitch',
                        key: 'back-stitch',
                        value: 'pillow01_stitch.png'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: '0d331f',
                        group: 'body',
                    },
                    {
                        key: 'outer-line',
                        type: 'color',
                        value: 'f15d29',
                        group: 'outer'
                    },
                    {
                        key: 'inner-line',
                        type: 'color',
                        value: 'a11d2d',
                        group: 'inner'
                    },
                ],
            },
            {
                name: 'Flap',
                key: 'flap',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'flap-stitch'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: '0d331f'
                    },
                ],
            }
        ],
        ui:[
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules:  {
                    unique: true
                }
                
            },
            {
                key: 'top-stripe',
                name: 'Top and Bottom Stripe color',
                group: ['outer'],
                value: ALL_COLORS,
                rules:  {
                    unique: true,
                }
            },
            {
                key: 'inner-stripe',
                name: 'Inner Stripe Color',
                group: ['inner'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                    excluded: [
                        COLOR_PINE_FOREST,
                    ]
                }
            },
        ]
    },


    // Calamigos All Over Pillow
    // pillow with calamigos letters all over
    /*
        Customizable
        - body color
        - text color
        - outline/edge color
        - must be 3 unique colors at any time
        - outline cant be green
    */
    'CCP003' : {

        model: {
            name: 'calamigos-pillow',
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'ccp003_f_b.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'front-stitch'
                    },
                    {
                        type: 'color',
                        key: 'outline',
                        value: 'df6536',
                        group: 'body',
                    },
                    {
                        type: 'color',
                        key: 'inside',
                        group: 'inside',
                        value: '173321'
                    },
                    {
                        type: 'color',
                        key: 'letters',
                        group: 'stitches',
                        value: 'f5f3e3', //'a6a6a5'
                    }
                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'ccp003_f_b.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'back-stitch'
                    },
                    {
                        type: 'color',
                        key: 'outline',
                        value: 'df6536',
                        group: 'body'
                    },
                    {
                        type: 'color',
                        key: 'inside',
                        group: 'inside',
                        value: '173321'
                    },
                    {
                        type: 'color',
                        key: 'letters',
                        group: 'stitches',
                        value: 'f5f3e3', //'a6a6a5'
                    }
                ],
            },
            {
                name: 'Flap',
                key: 'flap',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'flap-stitch'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: 'df6536',
                    },
                ],
            }
        ],
        ui: [
           
            {
                key: 'body',
                name: 'Body Color',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            },
            
            {
                key: 'letters',
                name: 'Text Color',
                group: ['stitches'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            },
            {
                key: 'inside',
                name: 'Inside Color',
                group: ['inside'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            }
        ],

    },

    // Malibu Color Block Pillow
    // pillow with MALIBU text on it
    /*
        Customization
        - Primary color (+ malibu text) - TODO: ATM BOTTOM IS NOT THE SAME COLOR AS TEXT
        - Secondary color - white atm
        - Line color
        - 3 unique colors at any time
        - line cant be green
    */
    'CCP004' : {

        model: {
            name: 'calamigos-pillow',
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'ccp004_F.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'primary',
                        type: 'color',
                        value: 'f6f6e4',
                        group: 'primary'
                    },
                    {
                        key: 'secondary',
                        type: 'color',
                        value: '090a07',
                        group: 'secondary'
                    },
                    {
                        key: 'line',
                        type: 'color',
                        value: '9f1c28',
                        group: 'line'
                    },

                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: 'ccp004_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'primary',
                        type: 'color',
                        value: 'f6f6e4',
                        group: 'primary'
                    },
                    {
                        key: 'secondary',
                        type: 'color',
                        value: '090a07',
                        group: 'secondary'
                    },
                    {
                        key: 'line',
                        type: 'color',
                        value: '9f1c28',
                        group: 'line'
                    },
                ],
            },
            {
                name: 'Flap',
                key: 'flap',
                group: 'body',
                layers: [
                    {
                        type: 'stitch',
                        value: 'pillow01_stitch.png',
                        key: 'flap-stitch'
                    },
                    {
                        type: 'color',
                        key: 'default',
                        value: 'f6f6e4'
                    },
                    // {
                    //     type: 'stitch',
                    //     value: 'pillow01_stitch.png',
                    //     key: 'flap-stitch'
                    // },
                  
                    // {
                    //     type: 'stitch',
                    //     size: {

                    //         width: 800,
                    //         height: 100,

                    //     },
                    //     key: 'default',
                    //     value: 'f6f6e4',
                    // }
                ],
            }
        ],
        ui: [
            {
                name: 'Primary Color',
                key: 'primary',
                group: ['body','primary'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            },
            {
                name: 'Secondary color',
                key: 'secondary',
                group: ['secondary'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                }
            },
            {
                name: 'Line color',
                key: 'line',
                group: ['line'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                    excluded: [
                        COLOR_PINE_FOREST
                    ]
                }
            }
        ],

    },


    // sweater - OLD AND NOT UPDATE CONFIG AS OF OCTOBER 31, 2019
    'CC001' : {
        // info for product CC001 - sweater
        model: {
            name: 'calamigos-sweater',
        },
        
        parts: [
            {
                name: 'Collar', // visible name
                key: 'Collar', // relation to class (in code)
                group: 'Collar', // when color for GROUP is changed thru UI this triggers all elements within this group to change texture/color
                layers: [
                    {
                        type: 'color',
                        key: 'default',
                        value: '173321',
                    },

                    {
                        type: 'stitch',
                        size: {

                            width: 240,
                            height: 15,

                        },
                        key: 'default',
                        value: '00FF00',
                    },

                ],
            },
            {
                name: 'Back',
                key: 'Back',
                group: 'Body',
                layers : [ // TODO - apply texture using layers
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'CC006_B.png',
                        key: 'back-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'CC006_F_stitch.png',
                        key: 'back-stitch',
                    },
                    {
                        type: 'color',
                        key: 'base',
                        value: '173321',
                    },
                ],
            },
            {
                name: 'Front',
                key: 'Front',
                group: 'Body',
                layers : [
                   
                    {
                        type: 'texture',
                        value: 'CC006_F.png',
                        key: 'front-texture',
                    },

                    {
                        type: 'stitch',
                        value: 'CC006_F_stitch.png',
                        key: 'front-stitch',
                    },

                    {
                        type: 'color',
                        key: 'base',
                        value: '173321',
                    },

                    {
                        type: 'color',
                        key: 'top-pinstripe', // its a KEY not a visible name, key should match whatever is in UI
                        value: 'f5f3e3',
                    },

                    {
                        type: 'color',
                        key: 'bottom-pinstripe',
                        value: '737474',
                    }
                  
                ],
            },
            {
                name: 'Sleeve',
                key: 'Sleeve',
                group: 'Body',
                layers : [
                    {
                        type: 'texture',
                        value: 'CC006_F.png',
                        key: 'sleeve-texture',
                    },

                    {
                        type: 'stitch',
                        value: 'CC006_F_stitch.png',
                        key: 'sleeve-stitch',
                    },
                ],
            },
            {
                name: 'Cuff',
                key: 'Cuff',
                group: 'Cuff',
                layers : [
                    {
                        type: 'stitch',
                        size: {

                            width: 240,
                            height: 30,

                        },
                        key: 'default',
                        value: '00FF00',
                    },

                ],
            },
            {
                name: 'Rib',
                key: 'Rib',
                group: 'Rib',
                layers : [
                    {
                        type: 'stitch',
                        size: {

                            width: 240,
                            height: 30,

                        },
                        key: 'default',
                        value: '00FF00',
                    },
                ],
            },
        ],
        
        pins : [ // [TODO] PINS MIGHT BE A JSON FILE IN NEAR FUTURE (WITH ADMIN EDITOR) - load thru config.ts?
            {
                group: ['Body'],
                position: {x: -30, y: 45, z: 0}, 
            },
        ],
        ui : [
            {
                key: 'base', // group key used in 'parts'
                name: 'Body Color', // visible text in HTML
                type: 'color', // type, can be color/texture/text
                value: [ // [TODO] - support a array of colors or 'string-value' of predefined colors
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                    'df6536',
                    '737474',
                    '9f1c28',
                    '173321',
                ],
                unique: false, // [TODO]cant match other groups in color
                different: ['Collar'], // [TODO] if sleeve has green color selected then on body green is disabled
            },            
            {
                key: 'Collar', // group key used in 'parts'
                name: 'Collar Color', // visible text in HTML
                type: 'color', // type, can be color/texture/text
                value: [
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                ],
                unique: false, // cant match other groups in color
                different: [], // if sleeve has green color selected then on body green is disabled
            },
            {
                key: 'Rib', // group key used in 'parts'
                name: 'Rib Color', // visible text in HTML
                type: 'color', // type, can be color/texture/text
                value: [
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                ],
                unique: false, // cant match other groups in color
                different: [], // if sleeve has green color selected then on body green is disabled
            },
            {
                key: 'Cuff', // group key used in 'parts'
                name: 'Cuff Color', // visible text in HTML
                type: 'color', // type, can be color/texture/text
                value: [
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                ],
                unique: false, // cant match other groups in color
                different: [], // if sleeve has green color selected then on body green is disabled
            },
            {
                key: 'top-pinstripe',
                name: 'Top Pinstripe',
                type: 'color',
                value: [
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                    'df6536',
                    '737474',
                    '9f1c28',
                    '173321',
                ]
            },
            {
                key: 'bottom-pinstripe',
                name: 'Top Pinstripe',
                type: 'color',
                value: [
                    '090a07',
                    'f5f3e3',
                    '262847',
                    '9f7d5e',
                    'df6536',
                    '737474',
                    '9f1c28',
                    '173321',
                ]
            }
        ],
    },


   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || 'CCP004';
    console.log('-=-=-=-=-=',handle,'=-=-=-=-=-');
    getProductConfig(handle);

})();