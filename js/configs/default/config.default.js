const WEBSITE_CONFIG = {
    customizerCanvasParent: '.customizerCanvasParent', /* parent element of customizer CANVAS */
    customizeProductButton: '#product-customize-button', /* button in shopify template that shows customizer */

    silentMode : true, // if customizer should start loading in the background

    api : [
        {
            // test
            hostname: 'calamigos-test.myshopify.com',
            api: 'https://customizer2.calamigosdrygoods.com/api/'
        },
        {
            // live
            hostname: 'calamigosdrygoods.com',
            api: 'https://customizer.calamigosdrygoods.com/api/'
        }
    ],

};
