
// key -> param
// value -> key??

// TODO - recheck configs here

const KEY_HANDLE =  {
    key: 'handle',
    value: 'shopify_product_handle',
    group: 'product',
}

const KEY_ORIGINAL_HANDLE = {
    key: 'originalHandle',
    group: 'product',
}

const KEY_VARIANT_TEXT = {
    key: 'variantText',
}

const KEY_ID = {
    key: 'id',
    value: 'shopify_product_id',
    group: 'product',
}

const KEY_TYPE = {
    key: 'type',
    value: 'product_type',
    group: 'product',
}

const KEY_SIZE = {
    key: 'size',
    value: 'product_size',
    group: 'product',
    optional: true,
}

const KEY_VARIANT = {
    key: 'variant',
    value: 'shopify_product_variant',
    group: 'product',
}

const KEY_QUANTITY = {
    key: 'quantity',
    value: 'default',
    default: 1,
    group: 'product',
}

const KEY_COLORS = {
    key: 'colors',
    value: 'product_colors',
    group: 'product',
}

const KEY_VISUAL_CUSTOM_TEXT = {
    key: 'customText',
    value: 'product_custom_text',
    optional: true,
    group: 'product',
}

const KEY_VISUAL_CUSTOM_COLORS = {
    key: 'customColors',
    value: 'product_custom_colors',
    optional: true,
    group: 'product',
}

const KEY_MASKS = {
    key: 'masks',
    value: 'product_textures',
}

const KEY_SCREENSHOT = {
    key: 'screenshot',
    value: 'product_screenshot',
    width: 400,
    height: 400,
}

const KEY_GENDER = {
    key: 'gender',
    value: 'product_gender',
}

const MODEL_CONFIG = [
    {
        info: 'product exporter configuration - used for sending data to backend',
        product: 'calamigos-sweater', // calamigos-sweater, calamigos-blanket, calamigos-pillow
        config : [
            KEY_HANDLE,
            KEY_ID,
            KEY_VARIANT,
            KEY_QUANTITY,
            KEY_COLORS,
            KEY_MASKS,
            KEY_SCREENSHOT,
            KEY_VISUAL_CUSTOM_TEXT,
            KEY_VISUAL_CUSTOM_COLORS,
            KEY_TYPE,
            KEY_SIZE,
            KEY_GENDER
        ],
    },
    {
        product: 'calamigos-blanket',
        config : [
            KEY_HANDLE,
            KEY_ID,
            KEY_VARIANT,
            KEY_QUANTITY,
            KEY_COLORS,
            KEY_MASKS,
            KEY_SCREENSHOT,
            KEY_VISUAL_CUSTOM_TEXT,
            KEY_VISUAL_CUSTOM_COLORS,
            KEY_TYPE,
            KEY_SIZE,
        ],
    },
    {
        product: 'calamigos-pillow',
        config : [
            KEY_HANDLE,
            KEY_ID,
            KEY_VARIANT,
            KEY_QUANTITY,
            KEY_COLORS,
            KEY_MASKS,
            KEY_SCREENSHOT
        ],
    },
]