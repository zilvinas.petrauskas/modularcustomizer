
const UI_CONFIG = {
    version: 'v1',
    parentElement: '.customizer-ui-wrapper',
    dynamicPropertiesContainer: '#dynamicProductProperties',
    optionsWrapper: '.customizer-options',
    mobileToggler: '.customizer-mobile-options',
    closeCustomizer: '.customizerCancel',
    addToCart: '.customizer-add-cart',
}