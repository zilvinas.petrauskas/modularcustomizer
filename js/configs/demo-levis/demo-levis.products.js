
let PRODUCT_CONFIG = {};

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
    {
        type: 'stitch_text',
        value: 'stitch_jacquard.png',
        key: 'JCQ_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_wide.png',
        key: 'WDE_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_tuck.png',
        key: 'TCK_text',
    }
]
const COLOR_LEVIS_BLACK = '030404';

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_MARINA_BLUE = '35469d'; // ceramic blue
const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
const COLOR_ORANGE = 'df6536'; // orange
const COLOR_FINE_FLANNEL = '737474'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const ALL_COLORS = [
    COLOR_LEVIS_BLACK,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];


// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_LEVIS_BLACK : {
        name: 'Black',
        feeder: 1,
        value: COLOR_LEVIS_BLACK,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_MARINA_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_MARINA_BLUE,
    },
    COLOR_CLASSIC_CAMEL : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_CLASSIC_CAMEL,
    },
    COLOR_ORANGE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_ORANGE,
    },
    COLOR_FINE_FLANNEL : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_FINE_FLANNEL,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
}

const PRODUCTS = {

    '2' : {
        model: {
            name: 'demo-levis'
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: '2C_M0002_Front.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'front-stitch'
                    },
                    {
                        key: 'front-color',
                        type: 'color',
                        group: 'body',
                        value: COLOR_LEVIS_BLACK,
                    },
                    {
                        key: 'front-logo',
                        type: 'color',
                        group: 'logo',
                        value: COLOR_WHITE_COTTON,
                    }

                ],
            },
            {
                name: 'Back',
                key: 'back',
                layers: [
                    {
                        type: 'texture',
                        value: '2C_M0002_Back.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: '2C_M0002_Front_Back_Stitch.png',
                        key: 'back-stitch'
                    },
                    {
                        key: 'back-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_LEVIS_BLACK,
                    },
                    

                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: '2C_M0002_Sleeve.png',
                        key: 'sleeve-texture'
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Sleeve_Stitch.png',
                        key: 'sleeve-stitch',
                    },
                    {
                        key: 'sleeve-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_LEVIS_BLACK,
                    },
                    {
                        key: 'sleeve-logo',
                        type: 'color',
                        group: 'logo',
                        value: COLOR_WHITE_COTTON,
                    }
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                group: 'body',
                layers: [

                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'collar-stitch',
                    },
                   {
                       type: 'collar-color',
                       type: 'color',
                       key: 'default',
                       value: COLOR_LEVIS_BLACK,
                   },
                    
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                group: 'body',
                layers: [
                   {
                       type: 'texture',
                       value: '2C_M0002_Cuff.png',
                       key: 'cuff-texture'
                   },
                   {
                       type: 'stitch',
                       value: '2C_M0002_Cuff_Stitch.png',
                       key: 'cuff-stitch',
                   },
                   {
                        key: 'cuff-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_LEVIS_BLACK,
                    },
                    {
                        key: 'cuff-logo',
                        type: 'color',
                        group: 'logo',
                        value: COLOR_WHITE_COTTON,
                    }
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                group: 'body',
                layers: [
                    {
                        type: 'texture',
                        value: '2C_M0002_Rib.png',
                        key: 'rib-texture'
                    },
                    {
                        type: 'stitch',
                        value: '2C_M0002_Rib_Stitch.png',
                        key: 'rib-stitch',
                    },
                    {
                        key: 'rib-color',
                        group: 'body',
                        type: 'color',
                        value: COLOR_LEVIS_BLACK,
                    },
                    {
                        key: 'rib-logo',
                        type: 'color',
                        group: 'logo',
                        value: COLOR_WHITE_COTTON,
                    }
                ],
            },
        ],
        ui: [
            {
                name: 'Body Color',
                key: 'body',
                group: ['body'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
                pins: {
                    position: [43, -44, 7],
                    view: [103, 0, 67], // camera position for pin viewing
                }
            },

            {
                name: 'Logo and Stripe Color',
                key: 'logo',
                group: ['logo'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },
                pins: {
                    position: [21, 27, -2],
                    view: [28, -6, 181], // camera position for pin viewing
                }
            },
        ],
    },


   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    // if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || '2';
    getProductConfig(handle);

})();