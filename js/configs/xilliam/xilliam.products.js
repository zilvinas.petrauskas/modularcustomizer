
let PRODUCT_CONFIG = {};

// Q: maybe each product should be in json file? 
// PROS: easier to modify, can be done without dev, only 1 file is loaded
// CONS: harder to match colors to one format, not error proof
    //  What if colors exist as a separate file? So if JSON needs to be adjusted slightly, it will register to all

const DEFAULT_STITCHES = [// best case scenario - each product should have their own stitches defined, but for now they can be here and included into each product
    
    {   // EACH PRODUCT SHOULD HAVE STITCH IN LAYERS - these will be preloaded
        type: 'stitch_normal',
        value: 'JCQ_normal.png',
        key: 'JCQ_normal',
    },
    {
        type: 'stitch_normal',
        value: 'WDE_normal.png',
        key: 'WDE_normal',
    },
    {
        type: 'stitch_normal',
        value: 'CLR_normal.png',
        key: 'CLR_normal',
    },
    {
        type: 'stitch_mask',
        value: 'JCQ_mask.png',
        key: 'JCQ_mask'
    },
    {
        type: 'stitch_mask',
        value: 'WDE_mask.png',
        key: 'WDE_mask'
    },
    {
        type: 'stitch_mask',
        value: 'CLR_mask.png',
        key: 'CLR_mask'
    },
    {
        type: 'stitch_text',
        value: 'stitch_jacquard.png',
        key: 'JCQ_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_wide.png',
        key: 'WDE_text',
    },
    {
        type: 'stitch_text',
        value: 'stitch_tuck.png',
        key: 'TCK_text',
    }
]

const COLOR_BLACK_SWAN = '090a07'; // black
const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
const COLOR_MARINA_BLUE = '35469d'; // ONLY TEMPORARY : FOR XILLIAM DEMO
// const COLOR_MARINA_BLUE = '262847'; // darkish blue
const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
const COLOR_ORANGE = 'df6536'; // orange
const COLOR_FINE_FLANNEL = '737474'; // gray
const COLOR_RED_HOT = '9f1c28'; // red
const COLOR_PINE_FOREST = '173321'; // forest green

const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST,
];

// used for backend
const COLOR_FEEDER_DATA = {
    COLOR_BLACK_SWAN : {
        name: 'Black',
        feeder: 1,
        value: COLOR_BLACK_SWAN,
    },
    COLOR_WHITE_COTTON : {
        name: 'White',
        feeder: 2,
        value: COLOR_WHITE_COTTON,
    },
    COLOR_MARINA_BLUE : {
        name: 'Blue',
        feeder: 3,
        value: COLOR_MARINA_BLUE,
    },
    COLOR_CLASSIC_CAMEL : {
        name: 'Brown',
        feeder: 4,
        value: COLOR_CLASSIC_CAMEL,
    },
    COLOR_ORANGE : {
        name: 'Orange',
        feeder: 5,
        value: COLOR_ORANGE,
    },
    COLOR_FINE_FLANNEL : {
        name: 'Grey',
        feeder: 6,
        value: COLOR_FINE_FLANNEL,
    },
    COLOR_RED_HOT : {
        name: 'Red',
        feeder: 7,
        value: COLOR_RED_HOT,
    },
    COLOR_PINE_FOREST : {
        name: 'Green',
        feeder: 8,
        value: COLOR_PINE_FOREST,
    },
}

const STITCH_JCQ = '0000FF';
const STITCH_WDE = 'FF0000';
const STITCH_CLR = '00FF00';

const PRODUCTS = {
    
    'CCXTEST' : {

        model: {
            name: 'demo-sweater', /* temp model until Xilliam models are created */
        },
        parts: [
            {
                name: 'Front',
                key: 'front',
                group: 'body-front',
                layers: [
                    ...DEFAULT_STITCHES,
                    {
                        type: 'texture',
                        value: 'X1_F_B.png',
                        key: 'front-texture',
                    },
                    {
                        type: 'stitch',
                        value: 'X1_F_B_stitch.png',
                        key: 'front-stitch'
                    },
                    { 
                        type: 'text', 
                        key: 'front-letter', 
                        group: 'letter', 
                    },
                    {
                        type: 'color',
                        key: 'front-base',
                        value: COLOR_MARINA_BLUE,
                        group: 'base-color',
                    },
                    {
                        type: 'color',
                        key: 'front-line',
                        value: COLOR_BLACK_SWAN,
                        group: 'line-color',
                    },

                ],
            },
            {
                name: 'Back',
                key: 'back',
                group: 'body-back',
                layers: [
                    {
                        type: 'texture',
                        value: 'X1_F_B.png',
                        key: 'back-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'X1_F_B_stitch.png',
                        key: 'back-stitch'
                   
                    },

                    {
                        type: 'text',
                        key: 'back-letter',
                        group: 'letter',
                    },

                    {
                        type: 'color',
                        key: 'back-base',
                        value: COLOR_MARINA_BLUE,
                        group: 'base-color',
                    },
                    {
                        type: 'color',
                        key: 'back-line',
                        value: '090a07',
                        group: 'line-color',
                    },
                  
                ],
            },
            {
                name: 'Sleeve',
                key: 'sleeve',
                group: 'body-sleeve',
                layers: [
                    {
                        type: 'texture',
                        value: 'X1_SLV.png',
                        key: 'sleeve-texture',
                    },
                    { 
                        type: 'stitch',
                        value: 'X1_SLV_stitch.png',
                        key: 'sleeve-stitch'
                    },
                    {
                        type: 'color',
                        key: 'sleeve-base',
                        value: '35469d',
                        group: 'base-color',
                    },
                    {
                        type: 'color',
                        key: 'sleeve-line',
                        value: '090a07',
                        group: 'line-color',
                    },
                ],
            },
            {
                name: 'Collar',
                key: 'collar',
                layers: [
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'collar-stitch'
                    },
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default',
                        group: 'collar',
                    }
                ],
            },
            {
                name: 'Cuff',
                key: 'cuff',
                layers: [
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'cuff-stitch', // default-stitch ??
                    },
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default', // or default-color 
                        group: 'cuff',
                    }
                ],
            },
            {
                name: 'Rib',
                key: 'rib',
                layers: [
                    { 
                        type: 'stitch',
                        value: 'green.png',
                        key: 'rib-stitch'
                    },
                    {
                        type: 'color',
                        value: COLOR_MARINA_BLUE,
                        key: 'default',
                        group: 'rib',
                    }
                ],
            },
          
        ],

        // pins are basically UI elements
        // information within UI - might get complicated, easier to follow grouping, can use existing functions for ui parts detect
        // separated into PINS - cleaner

        ui: [
         
            {
                name: 'Collar',
                key: 'collar-color',
                group: ['collar'],
                value: ALL_COLORS,
                rules: {
                    // unique: true,
                },

            },
            {
                name: 'Rib & Cuff',
                key: 'rib-and-cuff',
                group: ['rib','cuff','ribcuff'],
                value: ALL_COLORS,
                rules: {
                    // unique: true,
                },

            },
            {
                name: 'Base Color',
                key: 'base-colors',
                group: ['base-color'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },

            }, 
            {
                name: 'Line Color', // visible name of UI element
                key: 'line-colors', // unique key for this ui element, NO RELATIONS TO ASSIGNED HERE
                group: ['line-color'], // shows relations to layers or parts, either layer should have "group: 'line-color'" OR part should have it.
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },

            },
            {
                name: 'Custom Letter Color',
                key: 'custom-letter-colors',
                group: ['letter'],
                value: ALL_COLORS,
                rules: {
                    unique: true,
                },

            },

            {
                name: 'Front Letters',
                key: 'front-letter', // #2
                group: ['body-front'], // "body-front" for input
                value: 'X', // default string value?
                type: 'input',
                rules: {
                    limit: 1,
                    // color: COLOR_RED_HOT,
                    // stitch: STITCH_WDE,
                    required: true, 
                    position: { 
                        x: 65,
                        y: 30,
                    },
                    font: 'bold 200px Arial', // use html font styling convention
                    uppercase: true, // if user inputed text should be transformed to uppercase letters
                },

            },

            {
                name: 'Back Letters',
                key: 'back-letter',
                group: ['body-back'], // body-back, body-sleeve
                value: '?', // default string value?
                type: 'input',
                rules: {
                    limit: 3,
                    // color: COLOR_RED_HOT,
                    // stitch: STITCH_WDE,
                    required: true, 
                    position: { 
                        x: 50,
                        y: 50, 
                    },
                    font: 'bold 700px Arial', // use html font styling convention
                    uppercase: true, // if user inputed text should be transformed to uppercase letters
                }
            },

            // {
            //     name: 'Sleeve Letters',
            //     key: 'sleeve-letter',
            //     group: ['body-sleeve'], // body-back, body-sleeve
            //     value: '#', // default string value?
            //     type: 'input',
            //     rules: {
            //         limit: 3,
            //         // color: COLOR_RED_HOT,
            //         // stitch: STITCH_WDE,
            //         required: true, 
            //         position: { 
            //             x: 50,
            //             y: 90, 
            //         },
            //         font: 'bold 75px Arial', // use html font styling convention
            //     }
            // },
           
        ],

    },
   

};

// assign product_config from the list using the handle
// keep this function here in case in future we will be using non-shopify template
function getProductConfig(handle){
    
    // for shopify - get handle from URL

    let product = null;

    for(let p in PRODUCTS){
        if(p.toLowerCase() == handle.toLowerCase()){
            product = PRODUCTS[p];
            break;
        }
    }

    if(product === null){
        console.warn('PRODUCT not found!', handle, PRODUCTS);
    }

    product.handle = handle.toLowerCase();

    PRODUCT_CONFIG = product;

}

// if you have parameter id in url it will catch that, e.g. ?id=CC001
function getProductIdFromUrl(){
    if( ! window.location.href.includes('localhost')) return undefined;
    var url = new URL(window.location.href);
    var quickID = url.searchParams.get("id");
    return quickID;
}

function getProductHandle(){
    if(typeof PRODUCT_HANDLE !== 'undefined'){
        return PRODUCT_HANDLE.toLowerCase();
    }
    return null;
}


(function(){

    const handle = getProductIdFromUrl() || getProductHandle() || 'CCXTEST';
    console.log('-=-=-=-=-=',handle,'=-=-=-=-=-');
    getProductConfig(handle);

})();