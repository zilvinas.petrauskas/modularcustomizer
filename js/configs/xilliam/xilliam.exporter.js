
const KEY_HANDLE =  {
    id: 'shopify_product_handle', // used for getting info
    key: 'handle', // key goes together with value to backend
    group: 'product', // belongs to a group or not
}

const KEY_ORIGINAL_HANDLE = {
    id: 'shopify_original_handle',
    key: 'originalHandle',
    group: 'product',
}

const KEY_ID = {
    id: 'shopify_product_id',
    key: 'id',
    group: 'product',
}

const KEY_TYPE = {
    id: 'product_type',
    key: 'type',
    group: 'product',
}

const KEY_SIZE = {
    id: 'shopify_product_size',
    key: 'size',
    group: 'product',
    optional: true,
}

const KEY_VARIANT = {
    id: 'shopify_product_variant',
    key: 'variant',
    group: 'product',
}

const KEY_VARIANT_TEXT = {
    id: 'shopify_product_variant_text',
    key: 'variantText',
}

const KEY_QUANTITY = {
    id: 'shopify_quantity',
    key: 'quantity',
    group: 'product',
}

const KEY_COLORS = {
    id: 'product_colors',
    key: 'colors',
    group: 'product',
}

const KEY_VISUAL_CUSTOM_TEXT = {
    id: 'product_custom_text',
    key: 'customText',
    optional: true,
    group: 'product',
}

const KEY_VISUAL_CUSTOM_COLORS = {
    id: 'product_custom_colors',
    key: 'customColors',
    optional: true,
    group: 'product',
}

const KEY_MASKS = {
    id: 'product_masks',
    key: 'masks',
}

const KEY_SCREENSHOT = {
    id: 'product_screenshot',
    key: 'screenshot',
}

const KEY_GENDER = {
    id: 'product_gender',
    key: 'gender',
}

const KEY_CUSTOMIZED = {
    id: 'product_customized',
    key: 'customized',
}


const EXPORTER_CONFIG = [

    {
        product: 'demo-sweater',
        config : [
            KEY_HANDLE,
            KEY_ORIGINAL_HANDLE,
            KEY_ID,
            KEY_TYPE,
            KEY_SIZE,
            KEY_VARIANT,
            KEY_VARIANT_TEXT,
            KEY_QUANTITY,
            KEY_COLORS,
            KEY_VISUAL_CUSTOM_TEXT,
            KEY_VISUAL_CUSTOM_COLORS,
            KEY_MASKS,
            KEY_SCREENSHOT,
            KEY_GENDER,
            KEY_CUSTOMIZED,
        ],
    },

]; 