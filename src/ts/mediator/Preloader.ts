import { Root } from 'Root';
import { EVENT_PRELOADER_START, EVENT_PRELOADER_ADD, TYPE_TEXTURE, EVENT_PRELOADER_FINISHED, TYPE_GLB, EVENT_PRELOADER_LIST, TYPE_IMAGE, EVENT_PROGRESS_ADD, EVENT_ADJUST_PROGRESS_CONFIG, EVENT_PROGRESS_BAR_STARTED } from 'helpers/constants';
import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader';
import * as THREE from 'three';
import { getBaseUrl, getExternalAssetsUrl } from 'helpers/helpers';

// class to load textures
export class Preloader{

    private queue = [];
    private busy = false;
    private loaded = {};
    private done = false;

    constructor(){

       this.listen();
    }

    protected listen(){

        Root.instance.events.register(EVENT_PRELOADER_ADD, this.addThruEvent.bind(this)); // just use Root.instance.preloader.add() instead!
        Root.instance.events.register(EVENT_PRELOADER_START, this.load.bind(this));
        Root.instance.events.register(EVENT_PRELOADER_FINISHED, this.callback.bind(this));

        Root.instance.events.register(EVENT_PRELOADER_LIST, this.getList.bind(this));
    }

    public add(name: string, url: string, type: string, external: boolean = false){

        let fullUrl = external === true ? getExternalAssetsUrl() + url : getBaseUrl() + url;

        this.queue.push({
            name: name,
            url: fullUrl,
            type: type,
        });

    }

    // name: string, 
    // url: string, 
    // type : string
    protected addThruEvent(...args){

        alert('Just use Root.instance.preloader.add() instead!');
        return;

    }

    protected async load(){

        // console.log('- preloader::load', this.queue);

        const scope = this;

        await Root.instance.events.call(EVENT_ADJUST_PROGRESS_CONFIG, null, null, 'preloader', this.queue.length);
        await Root.instance.events.call(EVENT_PROGRESS_BAR_STARTED, null, null);


        // go thru items
        // load each items - can be image, 3d model file, other?
        // callback once all items are loaded

        for(let i=0;i<this.queue.length;i++){

            let item = this.queue[i];
            
            if(item.type == TYPE_TEXTURE){

                await this.loadTexture(item.url)
                .then((loadedItem)=>{
                    scope.onItemLoad(item.name, loadedItem);
                })
                .catch((err)=>{
                    console.warn('item not loaded', err);
                });

            }else if(item.type == TYPE_IMAGE){

                await this.loadImage(item.url)
                .then((image) =>{
                    scope.onItemLoad(item.name, image);
                })
                .catch((err)=>{
                    console.warn('Image failed to load', err);
                })

            }else if(item.type == TYPE_GLB){

                // load GLB model with THREE loader
                await this.loadGlb(item.url)
                .then((loadedItem: GLTF) =>{
                    scope.onItemLoad(item.name, loadedItem.scene);
                })
                .catch((err)=>{
                    console.warn('GLB file not loaded', err);
                })

            }else{
                console.warn('- unkonwn type at preloader', item.type, item);
            }

        }

        this.finish();
    }

    private onItemLoad(itemName, loadedItem){
        this.loaded[itemName] = loadedItem;
        Root.instance.events.call(EVENT_PROGRESS_ADD, null, null, 'preloader');
    }

    private async loadGlb(url: string){

        const loader = new GLTFLoader();

        return new Promise((resolve, reject) => {

            loader.load(url, (gltf: GLTF) => {
            
                resolve(gltf);

            }, (progressEvent)=>{
                if (progressEvent.lengthComputable) {
                    // TODO - total load minus already loaded
                    // this.progressBar.update(progressEvent.loaded, progressEvent.total);
                }
            });

        })

		
    }


    private async loadImage(url: string){

        return new Promise((resolve, reject)=>{

            const img = new Image();
            img.onload = ()=>{
                resolve(img);
            }
            img.onerror = reject;
            img.src = url;

        });

    }

    public loadTexture(url: string) {

		return new Promise((resolve, reject) => {
			const loader = new THREE.TextureLoader();
			loader.load(
				url,
				(texture) => {
					resolve(texture)
				},
				undefined,
				(err) => {
                    console.warn('Texture failed to load', url, err);
					reject();
				}
			)
		});
	}

    private finish(){

        // console.log('- preloader::finish', this.loaded);

        this.done = true;
        Root.instance.events.call(EVENT_PRELOADER_FINISHED, null, null, this.loaded);

    }

    private getList(){
        return this.loaded;
    }
   
    private callback(){
        return this.getList();
    }

}