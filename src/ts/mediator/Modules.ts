
export class Modules{


    private list = {};

    constructor(){

    }


    public async add(moduleName, moduleClass ){

        return new Promise(resolve => {

            if( this.list.hasOwnProperty(moduleName) ){
                console.log('Module with name "'+moduleName+'" already exist in a list!');
                return;
            }
    
            let module = this.list[moduleName] = moduleClass;

            resolve(module); // return is made here
            
        })

    }

}