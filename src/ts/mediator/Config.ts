
import { getWebsiteConfig, getProductConfig, getModelConfig, getUiConfig, getExporterConfig } from 'helpers/helpers';
import { Root } from 'Root';
import { ALL_CONFIGS_LOADED } from 'helpers/constants';
import { ConfigController } from 'module/config/ConfigController';

export class Config {

    public website; //  {name}.config.js - contains info about customizer version, html elements queries
    public product; // {name}.products.js - information about products, their relation with UI elements
    public model; // {name}.model.js - should contain some specific information about model (we currently dont need that). maybe we will some time in future
    public ui; // {name}.ui.js - UI version, html containers' queries
    public exporter; // {name}.exporter.js - each product type should have its exporter config which is sent to backend to process

    public async init() {

        await this.websiteConfig();
        await this.productConfig();
        await this.modelConfig();
        await this.uiConfig();
        await this.exporterConfig();

        this.done();
    }

    protected async websiteConfig() {

        this.website = new ConfigController(getWebsiteConfig());
        await this.website.init();
    }

    protected async productConfig() {

        this.product = new ConfigController(getProductConfig());
        await this.product.init();
    }

    protected async modelConfig() {

        this.model = new ConfigController(getModelConfig());
        await  this.model.init();
    }

    protected async uiConfig() {

        this.ui = new ConfigController(getUiConfig());
        await this.ui.init();
    }

    protected async exporterConfig(){

        this.exporter= new ConfigController(getExporterConfig());
        await this.exporter.init();
    }


    protected done() {
        Root.instance.events.call(ALL_CONFIGS_LOADED, null, null);
    }


}
