import { EVENT_GET_MODEL_DIMENSIONS } from 'helpers/constants';

export class Events{

    private events = {};
    private listeners = {};
    private ids = {};

    private debug: boolean = false; // writes all events into console.log

    constructor(){

        /*
            add - create event and make it triggerable thru 'trigger'
            remove - remove event, so it cant be called trhu 'trigger'
            listen - classes/functions can listen for event completion and will receive a callback (broadcast)
            trigger - trigger an event outside of scope of some class/function, once event is complete it will be broadcasted and sent to all that listens
        */

        
    }

    public exists(eventName){

        return this.events.hasOwnProperty(eventName);
    }

    // { unique: false }
    public register(eventName, callback, _options?){

        const options = {
            unique : true,
        }

        if(typeof _options !== 'undefined' && _options !== null){
            for(let o_O in _options) options[o_O] = _options[o_O];
        }


        // if event should be unique (only one with that name)
        if( options.unique && this.events.hasOwnProperty(eventName) ){
            console.warn('Event "'+eventName+'" is already defined', this.events[eventName]);
            return;
        }

        if( ! options.unique){
            if( ! this.events.hasOwnProperty(eventName)) this.events[eventName] = [];
            this.events[eventName].push(eventParams());
        }else{
            this.events[eventName] = eventParams();
        }

        function eventParams(){
            return {
                name: eventName,
                fn: (...args) => callback(...args), // this.testChange
                callback: callback,
                options: options,
            }
        }

    }

    
    public remove(event){
        
        if( this.events.hasOwnProperty(event) ){
            this.events[event] = undefined;
        }

    }

    public async call(eventName, _options, callback2, ...args){ // ..args = [1,2,23,3,4,5,,6]

        if(this.debug){
            
            if(eventName !== 'EVENT_GET_CANVAS_SIZE' && eventName !== 'EVENT_GET_UI_SIZE'){
                console.log('- event call', eventName, ...args);
            }

        }

        const options = {
            passTriggerCallback: false, // instead of passing arguments pass a callback2 with arguments - used for 
        }
        if(typeof _options !== 'undefined' && _options !== null){
            for(let o_O in _options) options[o_O] = _options[o_O];
        }

        if( ! this.events.hasOwnProperty(eventName) ){
            console.warn('- event.trigger - Cant trigger an event that doesnt exist', eventName);
            return;
        }

        let sent = false;
        let events = this.events[eventName];
        let eventsArray = Array.isArray(events) ? [...events] : [events];

        for(let i=0;i<eventsArray.length;i++){

            let event = eventsArray[i];

            if(eventsArray.length > 1){ // for example, assign parts. Its rare to have non unique events

                let completed = event.fn(...args); 

                if(typeof callback2 == 'function'){
    
                    sent = true;
                    callback2(completed); 

                }else if( callback2 === null){
                    // cool, do not return anything, just trigger

                }else{
                    console.warn('Multi-event callback should be either a function or a null (silent) ', typeof callback2, callback2);
                }

            }else{

                if(options.passTriggerCallback === true){

                    let completed = event.callback((...args)=>callback2(...args));
    
                    return new Promise(resolve => {
                        resolve(completed);
                    })
    
                }else{
    
                    let completed = await event.fn(...args); 
    
                    return new Promise(resolve => {
    
                        if(typeof callback2 == 'function'){
    
                            sent = true;
                            callback2(completed); 
                            resolve(completed);
    
                        }else if(callback2 === true){ 
    
                            sent = true;
                            resolve(completed);
    
                        }else if(this.listeners.hasOwnProperty(eventName)){ // resolves with broadcasting included
                        
                            this.broadcast(eventName, completed);
                        }
    
                        resolve(completed);
    
                    })
                }

            }

           

        }

        if(eventsArray.length > 1){
            return new Promise(resolve => {
                resolve();
            })
        }

    }

    /*
        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, ()=>{
            alert('Loading is complete');
        });

        

    */

    public listen(eventName, callback){

        // when some event is triggered

        if( ! this.listeners.hasOwnProperty(eventName) ){
            this.listeners[eventName] = [];
        }

        this.listeners[eventName].push({
            fn: (...args) => callback(...args),
        });

    }

    public broadcast(eventName, args){

        if(this.debug)  console.log('- broadcast', eventName);
        
        if( this.listeners.hasOwnProperty(eventName) ){
            for(let i=0;i<this.listeners[eventName].length;i++){
                this.listeners[eventName][i].fn(args); // trigger saved callbacks with arguments (optionally)
            }
        }

    }




    private getId(){
        
        let done = false;
        let id;

        while( ! done ){
            id = this.generateEventId();
            if( ! this.ids.hasOwnProperty(id) ){
                done = true;
            }
        }

        return id;
    }

    private generateEventId(){
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}