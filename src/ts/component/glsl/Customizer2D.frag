precision mediump float;
// MASK_COUNT is defined with glsl-inject-defines in JAVASCRIPT

uniform vec2 resolution;
varying vec2 v_texCoord;
uniform int blendMode1;
uniform int blendMode2;

uniform sampler2D color_base; // 		BGR
uniform sampler2D color_reference; //	REF
uniform sampler2D color_overlay; //		SHD

uniform vec4 color_identifier[MASK_COUNT]; //		Original Mask Colors
uniform vec4 color_replace[MASK_COUNT]; //			Active Colors

#pragma glslify: blendOverlay = require(glsl-blend/overlay)


vec3 when_eq(vec3 x, vec3 y) {
  return 1.0 - abs(sign(x - y));
}

void main() {

	vec4 b = texture2D(color_base, v_texCoord);
	vec4 c = texture2D(color_reference, v_texCoord);
	vec4 o = texture2D(color_overlay, v_texCoord);
	
	float e = 0.5;
	vec3 eps = vec3(e,e,e);

	vec3 color1, color2;
	color1 = b.rgb;
	color2 = b.rgb;

	for (int x = 0; x < MASK_COUNT; x ++ ) {

		vec4 ii = color_identifier[x];
		vec4 rr = color_replace[x];
		// vec4 i = vec4(ii[0], ii[x+1], ii[x+2], ii[x+3]);
		// vec4 r = vec4(rr[x], rr[x+1], rr[x+2], rr[x+3]);
		vec4 i = vec4(ii.r, ii.g, ii.b, ii.a);
		vec4 r = vec4(rr.r, rr.g, rr.b, rr.a);

		if (all(greaterThanEqual(c, vec4(ii.rgb-eps, 1.0))) && all(lessThanEqual(c, vec4(ii.rgb+eps, 1.0))))
			color1 = rr.rgb;

	}

	if (o.g > (o.b - 0.15) && o.g < (o.b+0.15))
		color2 = blendOverlay(o.rbb, color1.rgb, 1.0);

	// color1 = vec3(c.r, c.g, c.b);

	gl_FragColor = vec4(color2, 1.0);
}  
