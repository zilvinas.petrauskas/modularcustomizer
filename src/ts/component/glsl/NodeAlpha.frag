precision mediump float;
// MASK_COUNT is defined with glsl-inject-defines in JAVASCRIPT

uniform vec2 resolution;
varying vec2 v_texCoord;

uniform sampler2D base; // 		BGR

uniform vec4 mask_color; //		Original Mask Colors


vec3 when_eq(vec3 x, vec3 y) {
  return 1.0 - abs(sign(x - y));
}

void main() {

	vec4 b = texture2D(base, v_texCoord);
	vec4 m = mask_color;
	float e = 0.015;
	vec3 eps = vec3(e,e,e);

	vec4 alpha;
	alpha = vec4(0.0, 0.0, 0.0, 1.0);



	if (all(greaterThanEqual(b, vec4(m.rgb-eps, 1.0))) && all(lessThanEqual(b, vec4(m.rgb+eps, 1.0))))
		alpha = vec4(1.0, 1.0, 1.0, 1.0);

	gl_FragColor = alpha;
}  
