precision mediump float;

uniform vec2 u_resolution;
varying vec2 v_texCoord;
uniform sampler2D u_mask;

uniform vec3 u_color[COLOR_COUNT];

void main() {

	vec4 b = texture2D(u_mask, gl_FragCoord.xy/u_resolution.xy);
    vec3 color = vec3(1.0, 1.0, 1.0);

    float shortest;
    int pick;

    for (int i = 0; i < COLOR_COUNT; i++) {

        float test = abs(length(b.rgb - u_color[i].xyz));
        if (i == 0) {
            shortest = test;
            pick = i;
            color = u_color[i];
        } else {
            if (test < shortest) {
                shortest = test;
                pick = i;
                color = u_color[i];
            }
        }
    }

	gl_FragColor = vec4(color, 1.0);

}  
