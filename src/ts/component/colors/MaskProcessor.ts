import { hexToShaderRGB } from 'helpers/helpers';
import { NodeUtils } from '../../module/material/NodeUtils';

var injectDefines = require('glsl-inject-defines');

var GlslCanvas = require('glslCanvas/dist/GlslCanvas');

var frag = require('./MaskProcessor.frag').default;
var vert = require('./MaskProcessor.vert').default;

export class MaskProcessor{

    private canvas;

    private width;
    private height;

    constructor(width, height) {

        this.width = width;
        this.height = height;

        this.initCanvas(width, height);
    }

    public initCanvas(width, height) {

        this.canvas = document.createElement('canvas');

        this.canvas.width = width;
        this.canvas.height = height;

    }

    public async processCanvas(mask, colors) {

        var newFrag = injectDefines(frag, {
            COLOR_COUNT: colors.length
        })

        mask.width = this.width;
        mask.height = this.height;

        var processor = await new GlslCanvas(this.canvas, {preserveDrawingBuffer: true}, {preserveDrawingBuffer: true});
        await processor.load(newFrag);
        await processor.setUniform("u_mask", await mask.src);

        let rgb = [];

        for (let i = 0 ; i < colors.length; i++) {

            rgb[i] = hexToShaderRGB(colors[i]);
            await processor.setUniform("u_color[" + i.toString() + "]", rgb[i].r, rgb[i].g, rgb[i].b);

        }

        // TEMP
        let wrapper = document.getElementsByClassName('customizer-ui-wrapper');
        await wrapper[0].appendChild(this.canvas);

        var offscreenCanvas = document.createElement("canvas");
        offscreenCanvas.width = this.width;
        offscreenCanvas.height = this.height;
        var ctx = offscreenCanvas.getContext("2d");

        await processor.render();
        // await processor.renderPrograms();

        await ctx.drawImage(this.canvas, 0, 0, this.width, this.height);
        var imageData = await ctx.getImageData(0,0, this.width, this.height);
        var pixelData = imageData.data;
        console.log("image data is " , imageData);
        console.log(processor);

        let gl = await processor.gl;
        // let gl = processor.canvas.getContext('webgl', {preserveDrawingBuffer:true});
        // console.log(gl);
        // var pixelData = new Uint8Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4);
        // console.log(pixelData);
        // await gl.readPixels(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight, gl.RGBA, gl.UNSIGNED_BYTE, pixelData);
        let colorWeights = await NodeUtils.analyzeColorWeights(pixelData, 3);
        console.log(colorWeights);


        return this.canvas;

    }

}