import { Root } from 'Root';
import { EVENT_GET_COLOR_DATA, EVENT_CHECK_COLOR_DATA, EVENT_CHECK_COLOR_ALLOWED } from 'helpers/constants';
import { parseColor, isDefined } from 'helpers/helpers';

/*
    pass product configs
    does all the checks and returns list to UI
    then UI disables colors in html
*/

interface RulesList{
    [key: string] : any,
}
interface RulesDifferent{
    [key: string] : string,
}
interface RulesInterface{
    list: RulesList,
    different: RulesDifferent
}

// TODO - where active colors for each line/group is stored???

export class ColorRules{

    private rules: RulesInterface;
    private productConfig : any;

    constructor(){

       this.prepare();
       this.listen();
    }

    protected async prepare(){

        this.productConfig = Root.instance.config.product.get();

        this.rules = this.parseRules(this.productConfig);

    }

    protected listen(){

        Root.instance.events.register(EVENT_CHECK_COLOR_DATA, this.check.bind(this), null);
        Root.instance.events.register(EVENT_CHECK_COLOR_ALLOWED, this.colorAllowed.bind(this), null);

    }


    /*
        check if color is allowed for specific group (defined by key)

        arguments
        groupKey - unique string of ui group
        color - color string/value wanna be
        colorData - information about colors, might even get it within check FN?

    */
    public async check(){ // + use parsed rules in this class

        const colorData = await Root.instance.events.call(EVENT_GET_COLOR_DATA, null, true); // returns color data grouped by part 
        
        const uiGroupColors = this.getActiveColorsForUiGroups(colorData);

        // compare with rules
        const checked = {};
        
        for(let groupKey in uiGroupColors){
            let color = uiGroupColors[groupKey];
            let groupParsedConfig = this.rules.list[groupKey]; // list[key]{}, different{}
            let data = {
                groupKey : groupKey,
                color : parseColor(color, 'clean'),
            }
            checked[groupKey] = {...groupParsedConfig, ...data}; // merge
        }

        return checked;
    }

    private getActiveColorsForUiGroups(colorData){

        let uiGroupColors = {}

        for(let partKey in colorData){
            for(let layerKey in colorData[partKey]){

                let color = colorData[partKey][layerKey];
                let uiGroupKey = this.findColorUiGroup(partKey, layerKey, this.productConfig);
                if( isDefined(uiGroupKey) && !uiGroupColors.hasOwnProperty(uiGroupKey)){
                    uiGroupColors[uiGroupKey] = color;
                }
            }

        }

        return uiGroupColors;
    }

    private findColorUiGroup(partKey, layerKey, productConfig){

        // console.log('!!! looking for ', partKey, layerKey);

        let matchingPart = productConfig.parts.filter((part)=>{
            return part.key == partKey;
        });
        if(matchingPart.length > 0){
            // console.log('!!! found part', matchingPart);
            let matchingGroup = matchingPart[0].layers.filter((layer)=>{
                return (layer.type == 'color' || layer.type == undefined || layer.type == 'text') && layer.key == layerKey;
            });

            if(matchingGroup.length > 0){

                let lookForGroup = matchingGroup[0].group;

                // AND THEN check which ui config row contains this 'group' and take an actual group-key used
                const actualGroupKeys = productConfig.ui.filter((row) => {
                    return row.group.includes(lookForGroup);
                });

                if(actualGroupKeys.length > 0){
                    return actualGroupKeys[0].key;
                }

            }
        }

        return null;
    }

    // check if color allowed to be created in color HTML list
    /*
        arguments
        [0] - groupKey : string
        [1] - color code
    */
    public colorAllowed(...args){

        const groupKey = args[0];
        const color = args[1];

        let allowed = true;

        if(this.rules.list.hasOwnProperty(groupKey) && this.rules.list[groupKey].hasOwnProperty('excluded')){
            if(this.rules.list[groupKey].excluded.includes(color)){ // lol
                allowed = false;
            }
        }

        return allowed;
    }
    
    protected parseRules(productConfig){

        const parsedRules = {
            list: {},
            different : {}, // what is this for?
        };


        for(let i=0;i<productConfig.ui.length;i++){

            let line = productConfig.ui[i];

            if( ! line.hasOwnProperty('rules')){
                console.warn('UI line doesnt have rules', line, i, productConfig.ui);
                continue;
            }

            parsedRules.list[ line.key ] = this.parseLineRules(line);
            
        }

        return parsedRules;
    }

    protected parseLineRules(row){

        let lineRules = this.defaultRules();

         // color is unique thru all other parts
         if(row.rules.hasOwnProperty('unique')){
            lineRules.isUnique = row.rules.unique;
        }

        // exclude color from list
        if(row.rules.hasOwnProperty('excluded') && row.rules.excluded.length > 0){
            lineRules.excluded = row.rules.excluded;
        }

        // part/layer color is different from other part/layer
        // array of keys that shouldnt match
        // e.g front arrow and back arrow should be different, so different = ['front-arrow'] and on back-arrow its different: ['back-arrow']
        if(row.rules.hasOwnProperty('different') && row.rules.different.length > 0 ){

            // if rule is created for e.g. Front and it should be different than color used on e.g. Back, then code will created backwards rule for Back to be different from Front too
            for(let d=0;d<row.rules.different.length;d++){
                lineRules.different[ row.key ] = row.rules.different[d];
                lineRules.different[ row.rules.different[d] ] = row.key; // would be great to only have to define one part, like if front-arrow wants to be different from back-arrow, then back arrow auto detects that!
            }
        }

        return lineRules;
    }
    
    protected defaultRules(){
        return {
            isUnique : false,
            excluded : [],
            different: {},
        } as any;
    }
}