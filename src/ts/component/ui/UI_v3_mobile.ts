import { qs, parseColor, isDefined } from 'helpers/helpers';
import { UiGroupInterface } from 'module/ui/UI';
import { Root } from 'Root';
import { EVENT_UI_SELECT_GROUP, EVENT_UI_COLOR_CHANGED, EVENT_CHECK_COLOR_DATA, EVENT_UI_INPUT_CHANGED, EVENT_HIDE_CUSTOMIZER, EVENT_TRIGGER_ZOOM_OUT } from 'helpers/constants';
import { defaultTextParams, getAppliedRulesData } from 'module/ui/UiHelpers';

export class UI_v3_mobile{

    protected parent : any;
    protected colorElementGroups : any = {};
    protected containers: any = {};

    constructor(parent){

        this.parent = parent; // UI_v3
        this.getContainers();

    }

    protected getContainers(){

        this.containers = {
            parent: qs('.customizer-mobile-options-wrapper')[0],
            options: qs('.customizer-mobile-options')[0],
            arrow: qs('.customizer-mobile-back')[0],
            close: qs('.customizer-mobile-close')[0],
            // steps
            one: qs('#customizerMobileStep1 .step-options')[0],
            two: qs('#customizerMobileStep2 .step-options')[0],
        }

    }

    public create(parsedConfig){
        this.createUiHtml(parsedConfig);
    }

    public update(){
        this.updateColorElement();
    }

    public async applyRules(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;
        this.parent.clearColorsElementClasses(this.colorElementGroups);

        let check = getAppliedRulesData(checkedData);

        for(let key in check.active){
            let color = check.active[key];
            this.parent.setActiveColor(key, color, this.colorElementGroups);
        }

        this.parent.disableInList(check.disable, this.colorElementGroups);

    }


    public select(groupKey){

        // TODO - go thru step2 divs and leave visible with key that matches groupKey
        // TODO - change step to #2
        for(let key in this.colorElementGroups){

            if(key == groupKey){
                this.colorElementGroups[key].line.style.display = 'block';
            }else{
                this.colorElementGroups[key].line.style.display = 'none';
            }

        }

        this.changeStep(2);

    }

    public back(){

        this.changeStep(1);
        Root.instance.events.broadcast(EVENT_TRIGGER_ZOOM_OUT, null);
    }

    protected changeStep(step=1){

        if(step == 2){

            this.containers.parent.classList.add('mobile-step2');

        }else{

            this.containers.parent.classList.remove('mobile-step2');

        }

    }

  

    protected addToStepOne(div){
        this.containers.one.appendChild(div);
    }
    protected addToStepTwo(div){
        this.containers.two.appendChild(div);
    }

    protected async updateColorElement(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;

        for(let groupKey in checkedData){
            let color = checkedData[groupKey].color;
            this.updateGroupCssColor(groupKey, color);
        }
    }

    protected updateGroupCssColor(groupKey, color){
        let groupActive = this.colorElementGroups[groupKey].active;
        for(let i=0;i<groupActive.length;i++){
            let element = groupActive[i].element;
            element.style.backgroundColor = parseColor(color, 'css');
        }
    }

    protected async createUiHtml(parsed){

        for (let groupKey in parsed) {

            const groupConfig: UiGroupInterface | any = this.parent.getGroupConfig(groupKey);
            let parts = parsed[groupKey];

            this.colorElementGroups[groupKey] = {

                group: undefined, // html for group
                line: undefined, // either group of colors or input wrapper   
                ul: undefined,
                li: [], // color, element
                active: [],
                expanded: false,     // not needed?           
               
            } as any;

            let options = {
                colors : groupConfig.type == 'color' || groupConfig.type == undefined ? true : false
            }

            // STEP #1
            let groupDiv = this.createUiGroup(groupKey, groupConfig, options);
            this.addToStepOne(groupDiv);

            // STEP #2
            let groupItems;
            if (groupConfig.type == 'color' || groupConfig.type == undefined) { // type = 'color' dont need to be defined, becaues thast basically a default value

                groupItems = await this.createColorPicker(groupConfig, parts, EVENT_UI_COLOR_CHANGED, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

            } else if (groupConfig.type == 'input') {

                groupItems = await this.createCustomTextInput(groupConfig, parts, EVENT_UI_INPUT_CHANGED, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

            } else {
                console.warn('Unknown UI part type', groupConfig.type);
                continue;
            }

            this.addToStepTwo(groupItems);

        }

        this.bindMobileEvents();

    }

    protected bindMobileEvents(){
        this.bindCloseButton();
        this.bindBackArrow();
    }

    protected bindCloseButton(){
        this.containers.close.addEventListener('click', ()=>{
            Root.instance.events.call(EVENT_HIDE_CUSTOMIZER, null, null);
        });
    }

    protected bindBackArrow(){
        this.containers.arrow.addEventListener('click', ()=>{
            this.back();
        })
    }


    protected createUiGroup(groupKey, groupConfig, options : any = {} ) {

        let wrapper = document.createElement('div');
        wrapper.setAttribute('class','group-row');
        wrapper.addEventListener('click', () => {
            Root.instance.events.broadcast(EVENT_UI_SELECT_GROUP, groupKey);
        });

        let div = document.createElement('div');
        div.setAttribute('class', 'block-title');
      

        let createColorPreview = true;
        if(options.hasOwnProperty('colors')) createColorPreview = options.colors;

        // colors
        if(createColorPreview){

            let colorElementsWrapper = document.createElement('div');
            for (let i = 0; i < groupConfig.group.length; i++) {
                let colorElement = document.createElement('div');
                colorElement.setAttribute('class', 'color-group-item');
                // colorElement.setAttribute('id', this.groupItemId(groupConfig.group[i]));
                colorElementsWrapper.appendChild(colorElement);
                this.colorElementGroups[groupKey].active.push({
                    element: colorElement,
                    key: groupConfig.group[i],
                });
            }
            let countGroupItemsClass = (groupConfig.group.length > 1) ? 'color-group-count-' + groupConfig.group.length : '';
            colorElementsWrapper.setAttribute('class', 'color-group ' + countGroupItemsClass);
            div.appendChild(colorElementsWrapper);

        }else{

            let iconGroup = document.createElement('div');
            iconGroup.setAttribute('class','icon-group');
            
            let icon = document.createElement('div');
            icon.setAttribute('class','text-line-icon');
            
            iconGroup.appendChild(icon);
            div.appendChild(iconGroup);

        }

        // text
        let text = document.createElement('div');
        text.classList.add('group-line-name', 'no-select');
        text.innerHTML = groupConfig.name;

        // append text
        div.appendChild(text);
        
        // assign globally
        this.colorElementGroups[groupKey].group = wrapper;

        wrapper.appendChild(div);

        return wrapper;

    }

    protected async createColorPicker(group, parts, eventName, groupKey){

		// let scope = this;
		const wrapper = document.createElement('div');
        wrapper.classList.add('block-colors');
        
        const div = document.createElement('div');
        div.classList.add('block-colors-inside');

		const ul = document.createElement('ul');
		ul.classList.add('color-group-wrapper');
        // ul.setAttribute('id', getColorsListIdName(groupKey));

        this.colorElementGroups[groupKey].ul = ul;
        
        const title = document.createElement('div');
        title.classList.add('color-group-title','no-select');
        title.innerHTML = group.name;

		let colors = group.value;

		for (var i = 0; i < colors.length; i++) {

			const color = colors[i];

			if( ! await this.parent.colorAllowed(groupKey, color) ) continue;

			const colorElement = document.createElement('li');
            colorElement.style.backgroundColor = parseColor(color);
			colorElement.setAttribute('data-color', parseColor(color, 'clean'));
			// colorElement.setAttribute('data-index', String(i + 1)); // index not needed?

			colorElement.addEventListener('click', (event) => {
				let target = event.target as HTMLElement;
				// EVENT_UI_COLOR_CHANGED
				// this.triggerColorChange
				Root.instance.events.call(eventName, null, null, event, parts, target.dataset.color, groupKey );
			});
            ul.appendChild(colorElement);
            
			this.colorElementGroups[groupKey].li.push({
				color: parseColor(color, 'clean'),
				element: colorElement,
			});
		}

        
        div.appendChild(ul);
		div.appendChild(title);
       

        wrapper.appendChild(div);
        
        return new Promise(resolve => resolve(wrapper));
    }

    protected async createCustomTextInput(group, parts, eventName, groupKey){

        const scope = this;

        const wrapper = document.createElement('div');
        wrapper.classList.add('block-custom-text');

		const div = document.createElement('div');
		div.classList.add('block-custom-text-inside');

		const heading = document.createElement('div');
		heading.classList.add('color-group-title','no-select'); 
		heading.innerHTML = group.name || group.key;

		let isRequired = false;

		if(isDefined(group.rules) && isDefined(group.rules.required)){
			if(group.rules.required === true){
				heading.innerHTML += " (Required)";
				isRequired = true;
			}else{
				heading.innerHTML += " (Optional)";
			}
		}

		const input = document.createElement('input');
		input.classList.add('customizer-input');

		let maxTextLength = 1;

		if(isDefined(group.rules) && isDefined(group.rules.limit)){
            maxTextLength = group.rules.limit;
            let plural = group.rules.limit > 1 ? 's' : '';
			input.placeholder = `Max ${group.rules.limit} character${plural}`;
            input.maxLength = group.rules.limit;
            maxTextLength = group.rules.limit;
		} else {
			input.placeholder = `Enter 1 symbol`;
			input.maxLength = maxTextLength;
		}

		let defaultText = '';
		if (group.value && String(group.value).length > 0) {
			defaultText = group.value;
		}

		input.value = defaultText;
		let prevValue = defaultText;

        const textParams = defaultTextParams(groupKey, group);

		input.addEventListener('change', (event: any) => {

            textParams.value = scope.parent.applyRulesToChangedInput( event.target.value, group.rules );

			let key = group.key;
			console.log(key, 'changed to:"', textParams.value, '". String length?', textParams.value.length);

			if(prevValue == textParams.value) return;
			if(isRequired && textParams.value.length === 0) return;

			prevValue = textParams.value;

			// let target = event.target as HTMLElement;
			// EVENT_UI_INPUT_CHANGED
			// this.triggerInputChange
            Root.instance.events.call(eventName, null, null, event, parts, textParams);
            
		});


		
        div.appendChild(input);
        div.appendChild(heading);
        
        wrapper.appendChild(div);


		return new Promise(resolve => resolve(wrapper));
    }

}