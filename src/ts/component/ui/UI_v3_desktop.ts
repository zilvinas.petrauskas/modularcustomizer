import { Root } from 'Root';
import { EVENT_TEXTURES_APPLIED, EVENT_COLOR_CHANGE_COMPLETE, EVENT_CHECK_COLOR_DATA, EVENT_UI_COLOR_CHANGED, EVENT_UI_INPUT_CHANGED, EVENT_UI_UPLOAD_IMAGE, EVENT_UI_SELECT_GROUP, EVENT_HIDE_CUSTOMIZER, EVENT_UI_DESELECT_GROUP, EVENT_TRIGGER_ZOOM_IN, EVENT_TRIGGER_ZOOM_OUT, EVENT_GET_COLOR_DATA, EVENT_GET_MODEL_PARTS, EVENT_PRELOADER_LIST } from 'helpers/constants';
import { parseColor, isDefined, qs } from 'helpers/helpers';
import { UiGroupInterface } from 'module/ui/UI';
import { defaultTextParams, getAppliedRulesData } from 'module/ui/UiHelpers';

export class UI_v3_desktop{

    protected parent : any;
    protected colorElementGroups : any = {};
    protected containers: any = {};

    private lastActiveKey : string = '';

    constructor(parent){

        this.parent = parent; // UI_v3
        this.getContainers();

    }

    /*
        parsedConfig :  parseProductConfig(this.productConfig) in parent class UI_v3	
    */ 
    public create(parsedConfig){
        this.createUiHtml(parsedConfig);
        this.bindEvents();
    }

    public update(){
        this.updateColorElement();
    }

    public async applyRules(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;

        this.parent.clearColorsElementClasses(this.colorElementGroups);

        let check = getAppliedRulesData(checkedData);

        for(let key in check.active){
            let color = check.active[key];
            this.parent.setActiveColor(key, color, this.colorElementGroups);
        }

        this.parent.disableInList(check.disable, this.colorElementGroups);

    }

    
    public expand(groupKey){

         if(this.lastActiveKey === groupKey){ // collapse already expanded class
             this.lastActiveKey = '';
             this.collapseSection(groupKey);
             this.toggleExpandedClassName(false);
             Root.instance.events.broadcast(EVENT_TRIGGER_ZOOM_OUT, null);
             return;
         }

        
         
         this.lastActiveKey = groupKey;
                 
         for(let key in  this.colorElementGroups){
             if( this.colorElementGroups[key].expanded === true && groupKey != key ){
                 this.collapseSection(key);
             }
         }
 
         this.expandSection(groupKey);
         this.toggleExpandedClassName(true);

         Root.instance.events.broadcast(EVENT_TRIGGER_ZOOM_IN, groupKey);
    }

    protected collapseAll(){

        this.collapseSection(this.lastActiveKey);
        this.toggleExpandedClassName(false);
        this.lastActiveKey = '';

        Root.instance.events.broadcast(EVENT_TRIGGER_ZOOM_OUT, null);

    }

    protected toggleExpandedClassName(isExpanded){

        const className = 'expanded';
        if(isExpanded){
            this.containers.parent.classList.add(className);
        }else{
            this.containers.parent.classList.remove(className);
        }

    }

    protected collapseSection(key){

        this.colorElementGroups[key].group.classList.remove('expanded');
        this.animateCollapse( this.colorElementGroups[key].line, key );
        this.colorElementGroups[key].expanded = false;

    }

    protected expandSection(key){

        this.colorElementGroups[key].group.classList.add('expanded');
        this.animateExpand(this.colorElementGroups[key].line, key);
        this.colorElementGroups[key].expanded = true;

    }

    private animateExpand(element, key?) { // key for debuging only

        const sectionHeight = element.scrollHeight;
        
        element.style.height = sectionHeight + 'px';
        
        element.addEventListener('transitionend', function transitionEnd(e) {

            element.removeEventListener('transitionend', transitionEnd, false);
            element.style.height = 'auto';

        }, false);
        
    }
   

    private animateCollapse(element, key?) { // key for debuging only

        const sectionHeight = element.scrollHeight;
        
        const elementTransition = element.style.transition;
        element.style.transition = '';
        
        requestAnimationFrame(()=>{

            element.style.height = sectionHeight + 'px';
            element.style.transition = elementTransition;
            
            requestAnimationFrame(()=>{
                element.style.height = 0 + 'px';
            });
        });
        
    }
    

    protected getContainers(){

        this.containers = {
            parent: qs('.variant-customizer-container')[0],
            options: qs('.customizer-options')[0],
            close: qs('.customizer-close')[0],
            back: qs('.customizer-back')[0],
        }

    }

    protected bindEvents(){

        this.bindCloseButton();
        this.bindBackButton();
    }

    protected bindBackButton(){
        this.containers.back.addEventListener('click', ()=>{
            this.collapseAll();
        });
    }

    protected bindCloseButton(){
        this.containers.close.addEventListener('click', ()=>{
            Root.instance.events.call(EVENT_HIDE_CUSTOMIZER, null, null);
        });
    }

    protected addToOptionsContainer(div){
        this.containers.options.appendChild(div);
    }

    protected async updateColorElement(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;

        for(let groupKey in checkedData){
            let color = checkedData[groupKey].color;
            this.updateGroupCssColor(groupKey, color);
        }
    }

    protected updateGroupCssColor(groupKey, color){
        let groupActive = this.colorElementGroups[groupKey].active;
        for(let i=0;i<groupActive.length;i++){
            let element = groupActive[i].element;
            element.style.backgroundColor = parseColor(color, 'css');
        }
    }

    /*
        for grouped types
    */
    protected async createUiHtml(parsed) {

        for (let groupKey in parsed) {

            const groupConfig: UiGroupInterface | any = this.parent.getGroupConfig(groupKey);
            let parts = parsed[groupKey];

            this.colorElementGroups[groupKey] = {

                group: undefined, // html for group
                line: undefined, // either group of colors or input wrapper   
                ul: undefined,
                li: [], // color, element

                active: [],

                expanded: false,                
               
            } as any;

            let options = {
                colors : groupConfig.type == 'color' || groupConfig.type == undefined ? true : false
            }

            let groupDiv = this.createUiGroup(groupKey, groupConfig, options);

            let groupItems;
            if (groupConfig.type == 'color' || groupConfig.type == undefined) { // type = 'color' dont need to be defined, becaues thast basically a default value

                groupItems = await this.createColorPicker(groupConfig, parts, EVENT_UI_COLOR_CHANGED, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

            } else if (groupConfig.type == 'input') {

                groupItems = await this.createCustomTextInput(groupConfig, parts, EVENT_UI_INPUT_CHANGED, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

            } else if (groupConfig.type == 'image-upload') {
                
                groupItems = await this.createImageEditor(groupConfig, parts, EVENT_UI_UPLOAD_IMAGE, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

                // await ANALYZE IMAGE
                // -- parse colors to 3 true color
                // returning CONFIG, COLORS, NEW UI
                // 

            } else {
                console.warn('Unknown UI part type', groupConfig.type);
                continue;
            }

            groupDiv.appendChild(groupItems);
            this.addToOptionsContainer(groupDiv);

        }

    }

    protected createUiGroup(groupKey, groupConfig, options: any = {}) {

        let wrapper = document.createElement('div');
        wrapper.setAttribute('class','group-row');

        let div = document.createElement('div');
        div.setAttribute('class', 'block-title');
        div.addEventListener('click', () => {
            // this.expand
            Root.instance.events.broadcast(EVENT_UI_SELECT_GROUP, groupKey);
        });

         // text
         let text = document.createElement('div');
         text.classList.add('group-line-name', 'no-select');
         text.innerHTML = groupConfig.name;
         
         // arrow
         let arrowWrapper = document.createElement('div');
         arrowWrapper.setAttribute('class','group-line-arrow-wrapper');
         let arrow = document.createElement('div');
         arrow.setAttribute('class','group-line-arrow');

         arrowWrapper.appendChild(arrow);
         text.appendChild(arrowWrapper);

         // append text
         div.appendChild(text);

        // colors
        let createColorPreview = true;
        if(options.hasOwnProperty('colors')) createColorPreview = options.colors;

        if(createColorPreview){

            let colorElementsWrapper = document.createElement('div');
            for (let i = 0; i < groupConfig.group.length; i++) {
                let colorElement = document.createElement('div');
                colorElement.setAttribute('class', 'color-group-item');
                // colorElement.setAttribute('id', this.groupItemId(groupConfig.group[i]));
                colorElementsWrapper.appendChild(colorElement);
                this.colorElementGroups[groupKey].active.push({
                    element: colorElement,
                    key: groupConfig.group[i],
                });
            }
            let countGroupItemsClass = (groupConfig.group.length > 1) ? 'color-group-count-' + groupConfig.group.length : '';
            colorElementsWrapper.setAttribute('class', 'color-group ' + countGroupItemsClass);
            div.appendChild(colorElementsWrapper);

        }else{

            let iconGroup = document.createElement('div');
            iconGroup.setAttribute('class','icon-group');
            
            let icon = document.createElement('div');
            icon.setAttribute('class','text-line-icon');
            
            iconGroup.appendChild(icon);
            div.appendChild(iconGroup);
        }
        
        this.colorElementGroups[groupKey].group = wrapper;

        wrapper.appendChild(div);

        return wrapper;

    }

    // v3
    // v3
    // v3
    protected async createColorPicker(group, parts, eventName, groupKey){

		// let scope = this;
		const wrapper = document.createElement('div');
        wrapper.classList.add('block-colors');
        
        const div = document.createElement('div');
        div.classList.add('block-colors-inside');

		const ul = document.createElement('ul');
		ul.classList.add('color-group-wrapper');

        this.colorElementGroups[groupKey].ul = ul;
        
        const title = document.createElement('div');
        title.classList.add('color-group-title','no-select');
        title.innerHTML = group.name;
        title.dataset.key = groupKey;
        title.dataset.group = group;

		let colors = group.value;

		for (var i = 0; i < colors.length; i++) {

			const color = colors[i];

			if( ! await this.parent.colorAllowed(groupKey, color) ) continue;

			const colorElement = document.createElement('li');
            colorElement.style.backgroundColor = parseColor(color);
			colorElement.setAttribute('data-color', parseColor(color, 'clean'));
			// colorElement.setAttribute('data-index', String(i + 1)); // index not needed?

			colorElement.addEventListener('click', (event) => {
				let target = event.target as HTMLElement;
				// EVENT_UI_COLOR_CHANGED
				// this.triggerColorChange
				Root.instance.events.call(eventName, null, null, event, parts, target.dataset.color, groupKey );
			});
            ul.appendChild(colorElement);
            
			this.colorElementGroups[groupKey].li.push({
				color: parseColor(color, 'clean'),
				element: colorElement,
			});
		}

		// TODO - add title only when there are more than 1 element in group!
		// div.appendChild(title);
        div.appendChild(ul);

        wrapper.appendChild(div);
        
        return new Promise(resolve => resolve(wrapper));
    }

    
    protected async createImageEditor(group, parts, eventName, groupKey) {

        const scope = this;

        const wrapper = document.createElement('div');
        wrapper.classList.add('block-image-editor');

        const div = document.createElement('div');
        div.classList.add('block-image-editor-inside')

		const heading = document.createElement('div');
		heading.classList.add('color-group-title','no-select'); 
        // heading.innerHTML = group.name || group.key;
        heading.innerHTML = ""; // Text?

		// let isRequired = false;

        const input = document.createElement('input');
        input.type = "file";
        input.id = groupKey + "-upload";
        input.style.visibility = "hidden";
        input.style.display = "none";
        input.classList.add('customizer-upload');


        let loadedTextures = await Root.instance.events.call(EVENT_PRELOADER_LIST, null, null);
        console.log(parts)

        const canvas = document.createElement('canvas');
        canvas.id = group + "-canvas";

        const button = document.createElement('button');
        button.classList.add('customizer-upload-button');
        button.innerHTML = "Upload Image";

        let prevValue = null;
        
        const onInputChangeEvent = (event: any) => {

            // if(prevValue === event.target.value) return;

            if(event.target.files.length === 0){
                alert('Select and upload an image');
                return;
            }
    
            //@ts-ignore
            let file = event.target.files[0];
            let reader = new FileReader();
            reader.onload = async function() {

                let img = new Image();
                //@ts-ignore
                img.src = reader.result;
                img.onload = async function() {
    
                    
                    Root.instance.events.call(eventName, null, null, event, parts, img, groupKey );

                }
            }
            reader.readAsDataURL(file);

			// let target = event.target as HTMLElement;
			// EVENT_UI_INPUT_CHANGED
			// this.triggerInputChange
        }

        // let inputEvents = ['propertychange', 'change', 'click', 'keyup', 'input', 'paste']; // faster trigger
        let inputEvents = ['change'];

        for(let a in inputEvents)
            input.addEventListener(inputEvents[a], onInputChangeEvent);


        button.addEventListener("click", async function() {

            await this.triggerUploadButton(groupKey);

        }.bind(this))
        
		div.appendChild(heading);
        div.appendChild(input);
        div.appendChild(button);
        
        wrapper.appendChild(div);

		return new Promise(resolve => resolve(wrapper));


    }


    protected async triggerUploadButton(groupKey) {

        let uploadId = groupKey + "-upload";

        // let parts = await Root.instance.events.call(EVENT_GET_MODEL_PARTS, null, null, groupKey)

        // console.log(parts);

        let inputTrigger = document.getElementById(uploadId);
        //@ts-ignore
        inputTrigger.click(); 

    }


    protected async updateCanvas(e: any) {

        if(e.target.files.length === 0){
            alert('Select and upload an image');
            return;
        }

        //@ts-ignore
        let file = e.target.files[0];
        let reader = new FileReader();
        reader.onload = async function() {
            let img = new Image();
            //@ts-ignore
            img.src = reader.result;
            img.onload = async function() {

                // await Root.instance.events.call(EVENT_UPDATE_2D_CANVAS_IMAGE, null, null, img);
            
            }
        }
        reader.readAsDataURL(file);
    }



    protected async createCustomTextInput(group, parts, eventName, groupKey){


        const scope = this;

        const wrapper = document.createElement('div');
        wrapper.classList.add('block-custom-text');

		const div = document.createElement('div');
		div.classList.add('block-custom-text-inside');

		const heading = document.createElement('div');
		heading.classList.add('color-group-title','no-select'); 
        // heading.innerHTML = group.name || group.key;
        heading.innerHTML = ""; // Text?

		let isRequired = false;

		const input = document.createElement('input');
		input.classList.add('customizer-input');

        let maxTextLength = 1;
        let placeholder = "";

		if(isDefined(group.rules) && isDefined(group.rules.limit)){
            maxTextLength = group.rules.limit;
            let plural = group.rules.limit > 1 ? 's' : '';
			placeholder = `Max ${group.rules.limit} character${plural}`;
            input.maxLength = group.rules.limit;
            maxTextLength = group.rules.limit;
		} else {
			placeholder = `Max 1 character`;
			input.maxLength = maxTextLength;
        }

        input.placeholder = placeholder;
        heading.innerHTML = placeholder;
        
        if(isDefined(group.rules) && isDefined(group.rules.required)){
			if(group.rules.required === true){
				heading.innerHTML += " (Required)";
				isRequired = true;
			}else{
				heading.innerHTML += " (Optional)";
			}
		}

		let defaultText = '';
		if (group.value && String(group.value).length > 0) {
			defaultText = group.value;
		}

		input.value = defaultText;
        let prevValue = defaultText;
        
        const textParams = defaultTextParams(groupKey, group);

        const onInputChangeEvent = (event: any) => {

            if(prevValue === event.target.value) return;
            if(isRequired && event.target.value.length === 0) return;

            textParams.value = scope.parent.applyRulesToChangedInput( event.target.value, group.rules );

			prevValue = textParams.value;

			// let target = event.target as HTMLElement;
			// EVENT_UI_INPUT_CHANGED
			// this.triggerInputChange
			Root.instance.events.call(eventName, null, null, event, parts, textParams);
        }

        let inputEvents = ['propertychange', 'change', 'click', 'keyup', 'input', 'paste']; // faster trigger
        // let inputEvents = ['change'];

        for(let a in inputEvents)
            input.addEventListener(inputEvents[a], onInputChangeEvent);

		div.appendChild(heading);
        div.appendChild(input);
        
        wrapper.appendChild(div);


		return new Promise(resolve => resolve(wrapper));
    }
    
    


}