import { Model } from './Model';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_MODEL_READY, EVENT_PROGRESS_ADD, EVENT_ADD_TO_RENDERER } from 'helpers/constants';
import * as THREE from 'three';
import { BlanketPhysics } from 'component/physics/BlanketPhysics';

export class BlanketWithPhysics extends Model{

    protected physics: any;
    protected physicsCtrl: any;

    constructor(){
        super();
    }

    protected async create(){
         
        this.physics = this.initPhysics();

        this.model = await this.generatedModel();
        this.model = await this.applyOptions(this.model, this.modelConfig.options);

        this.physicsCtrl = this.physics.getController();

        // await Root.instance.events.call(EVENT_ASSIGN_PARTS, null, null, this.model);
        // await this.applyInitialMaterials();

        await Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, this.model);

        Root.instance.events.broadcast(EVENT_MODEL_READY, null);
        Root.instance.events.call(EVENT_PROGRESS_ADD, null, null, 'product');

        // add physics renderer to scene renderer
        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'physics-renderer', this.physicsCtrl.animate.bind(this.physicsCtrl));

    }

    // model created thru code to work with physics
    protected async generatedModel(){

        const clothMaterial = new THREE.MeshStandardMaterial( {
            name: 'BLANKET',
            color: new THREE.Color('white'),
            side: THREE.DoubleSide,
            alphaTest: 0.5,
            roughness: 0.89,
            refractionRatio: 0.98,
        } );

        const clothGeometry = new THREE.ParametricBufferGeometry( this.physics.getClothFunction(), this.physics.getXSegs(), this.physics.getYSegs() );  
        this.physics.setClothGeometry(clothGeometry);
        
        const blanket = new THREE.Mesh( clothGeometry, clothMaterial );

        return new Promise(resolve => resolve(blanket));
    }

     // TODO
    protected blanketFront(){}
    protected blanketBack(){}

    // TODO - should be reused for small/mini blanket
    protected initPhysics(){
        return new BlanketPhysics(25, 10, 10);
    }
}