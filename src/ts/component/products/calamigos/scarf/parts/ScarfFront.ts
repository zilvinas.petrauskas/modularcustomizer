import { Part } from 'component/products/Part';

export class ScarfFront extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['FRT'];
        this.partEventName = 'update-front';
        this.key = 'front';
        this.listen();
    }

}