import { Model } from 'component/products/Model';
import { ScarfFront } from './parts/ScarfFront';
import * as THREE from 'three';

export class CalamigosScarf extends Model{

    constructor(){
        super();

        this.modelConfig.model = {
            name: 'calamigos-scarf',
            file: 'scarf_v3.glb',
        }

        this.modelConfig.parts = [
            { 
                key: 'front',
                component: new ScarfFront(),
            },
        ];

        this.modelConfig.options = {
            scale: 20,
            position: new THREE.Vector3(0,0,0),
        }

        this.queueModel(); // push 3d model url to preloader
        this.listen(); // wait for preloader to finish and call 'init' function

    }  


    protected async afterModelCreation(){
        
        // TODO - prep mannequin to be transparent
        let mannequin = this.model.children.filter(child => child.name == "Mannequin")[0];

        if(mannequin){ // TODO - kinda looks bad when transparent...
            // mannequin.material.transparent = true;
            // mannequin.material.opacity = 0.5;
            // mannequin.material.needsUpdate = true;
        }

    }

}