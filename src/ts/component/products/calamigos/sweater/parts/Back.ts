import { Part } from 'component/products/Part';


export class Back extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['BCK'];
        this.partEventName = 'update-back';
        this.key = 'back';
        this.listen();
    }
}