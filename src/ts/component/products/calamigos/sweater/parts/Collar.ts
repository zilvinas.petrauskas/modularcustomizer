import { Part } from 'component/products/Part';


export class Collar extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['CLR'];
        this.partEventName = 'update-collar';
        this.key = 'collar';
        this.listen();
    }
}