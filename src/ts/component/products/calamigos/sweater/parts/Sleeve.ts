import { Part } from 'component/products/Part';

export class Sleeve extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['SLV'];
        this.partEventName = 'update-sleeve';
        this.key = 'sleeve';
        this.listen();
    }
}