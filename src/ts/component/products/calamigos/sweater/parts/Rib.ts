import { Part } from 'component/products/Part';


export class Rib extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['RIB'];
        this.partEventName = 'update-rib';
        this.key = 'rib';
        this.listen();
    }
}