import { Part } from 'component/products/Part';

export class Cuff extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['CUF'];
        this.partEventName = 'update-cuff';
        this.key = 'cuff';
        this.listen();
    }
}