import { Sleeve } from 'component/products/calamigos/sweater/parts/Sleeve';
import { Back } from 'component/products/calamigos/sweater/parts/Back';
import { Collar } from 'component/products/calamigos/sweater/parts/Collar';
import { Cuff } from 'component/products/calamigos/sweater/parts/Cuff';
import { Rib } from 'component/products/calamigos/sweater/parts/Rib';
import { Front } from 'component/products/calamigos/sweater/parts/Front';
import { Model } from '../../Model';
import * as THREE from 'three';
    
export class CalamigosSweater extends Model{ // default sweater for Calamigos - front, back, sleeve (2x but uses same texture) with textures and then collar, rib & cuff with just color change
    
    constructor(){
        super();

        this.modelConfig.model = {
            name: 'calamigos-sweater',
            file: 'new_shirt_v18.glb',
        }

        this.modelConfig.parts = [
            { 
                key: 'front',
                component: new Front(),
            },
            { 
                key: 'back',
                component: new Back(),
            },
            { 
                key: 'sleeve',
                component: new Sleeve(),
            },
            { 
                key: 'collar',
                component: new Collar(),
            },
            { 
                key: 'cuff',
                component: new Cuff(),
            },
            { 
                key: 'rib',
                component: new Rib(),
            },
        ];

        this.modelConfig.options = {
            scale: 40,
            position: new THREE.Vector3(0,60,-7),
        }

        this.queueModel(); // push 3d model url to preloader
        this.listen(); // wait for preloader to finish and call 'init' function

    }  

    

}