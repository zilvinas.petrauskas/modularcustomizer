import { Part } from 'component/products/Part';

/*
    Pillow FLAP part
*/
export class Flap extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['FLP'];
        this.partEventName = 'update-flap';
        this.key = 'flap';
        this.listen();
    }
}