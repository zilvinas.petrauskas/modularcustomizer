import { Part } from 'component/products/Part';


/*
    Pillow BACK part
*/
export class Back extends Part{

    constructor(...args){
        super(...args);

        this.parts = ['BCK'];
        this.partEventName = 'update-back';
        this.key = 'back';
        this.listen();
    }
}