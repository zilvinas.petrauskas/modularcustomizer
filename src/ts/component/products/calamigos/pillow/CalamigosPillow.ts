import { Model } from '../../Model';

import { Front } from './parts/Front';
import { Back } from './parts/Back';
import { Flap } from './parts/Flap';

import * as THREE from 'three';
    
export class CalamigosPillow extends Model{
    
    constructor(){
        super();

        this.modelConfig.model = {
            name: 'calamigos-pillow',
            file: 'pillow_v2.glb',
        }

        this.modelConfig.parts = [
            { 
                key: 'front',
                component: new Front(),
            },
            { 
                key: 'back',
                component: new Back(),
            },
            { 
                key: 'flap',
                component: new Flap(),
            },
        ];

        this.modelConfig.options = {
            scale: 50,
            position: new THREE.Vector3(0,0,0),
            rotation: new THREE.Euler(0, Math.PI/2 ,0, 'XYZ'), // rotate pillow 90 degrees on y axis
        }

        this.queueModel(); // push 3d model url to preloader
        this.listen(); // wait for preloader to finish and call 'init' function

    }  

    

}