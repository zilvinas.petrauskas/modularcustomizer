import { Root } from 'Root'
import { TYPE_GLB, EVENT_CREATE_MODEL, EVENT_UPDATE_MASK, EVENT_CHANGE_MASK, EVENT_PRELOADER_FINISHED, EVENT_COLOR_CHANGE, EVENT_ASSIGN_PARTS, EVENT_ADD_TO_SCENE, EVENT_TEXTURE_CHANGE, EVENT_UPDATE_MATERIAL, EVENT_BUILD_MATERIAL, EVENT_GET_MODEL, EVENT_GET_MODEL_DIMENSIONS, EVENT_MODEL_READY, EVENT_PROGRESS_ADD, EVENT_GET_PRODUCT_TYPE, EVENT_TEXTURES_APPLIED, EVENT_GET_COLOR_DATA, EVENT_COLOR_CHANGE_COMPLETE, EVENT_GET_MODEL_PARTS, EVENT_GET_PRODUCT_POSITION, EVENT_GET_MODEL_NAME, EVENT_CUSTOM_TEXT_CHANGE, EVENT_UPDATE_CUSTOM_TEXT, EVENT_GET_CUSTOM_TEXT_PARAMS, EVENT_TEXT_CHANGE_COMPLETE, EVENT_SAVE_CUSTOM_TEXT_VALUES } from 'helpers/constants';
import { createWhiteTexture } from 'helpers/helpers';
import * as THREE from 'three';
import { resolve } from 'url';
import { MaskProcessor } from 'component/colors/MaskProcessor';

interface PartsInterface{
    key: string,
    component: any,
}

interface ModelInterface{
    name: string,
    file: string,
}

interface ModelConfigInterface {
    model: ModelInterface,
    parts: PartsInterface[],
    options? : any,
}

export class Model{ // default sweater for Calamigos - front, back, sleeve (2x but uses same texture) with textures and then collar, rib & cuff with just color change

    public modelConfig = <ModelConfigInterface> {
        model: {},
        parts : []
    };

    protected model: any;

    protected preloaded: any;
    

    constructor(){
    }
   
    protected async init(...args){

        this.preloaded = args[0]; // loadTextures
        await this.create();
    }

    protected async create(){
        
        this.model = this.preloaded[ this.modelConfig.model.name ];
        this.model = await this.makeMaterialsUnique(this.model);
        this.model = await this.applyOptions(this.model, this.modelConfig.options);
 
        await this.afterModelCreation();

        await Root.instance.events.call(EVENT_ASSIGN_PARTS, null, null, this.model);
        
        await this.applyInitialMaterials();
        await Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, this.model);

        Root.instance.events.broadcast(EVENT_MODEL_READY, null);
        Root.instance.events.call(EVENT_PROGRESS_ADD, null, null, 'product');

        
    }

    protected async afterModelCreation(){
        
    }

    public getModelName(){
        return this.modelConfig.model.name;
    }

    // add 3d model to preloader
    protected queueModel(){
        Root.instance.preloader.add(this.modelConfig.model.name, this.modelConfig.model.file, TYPE_GLB, true);
    }

    protected listen(){

        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.init.bind(this) );
        Root.instance.events.register(EVENT_COLOR_CHANGE, this.changeColor.bind(this));
        Root.instance.events.register(EVENT_CUSTOM_TEXT_CHANGE, this.changeCustomText.bind(this));
        Root.instance.events.register(EVENT_CHANGE_MASK, this.changeMask.bind(this));

        Root.instance.events.register(EVENT_GET_MODEL, this.getModel.bind(this));
        Root.instance.events.register(EVENT_GET_MODEL_DIMENSIONS, this.getModelDimensions.bind(this));

        Root.instance.events.register(EVENT_GET_MODEL_PARTS, this.getModelParts.bind(this), null);

        Root.instance.events.register(EVENT_GET_PRODUCT_POSITION, this.getProductPosition.bind(this), null);

        Root.instance.events.register(EVENT_GET_MODEL_NAME, this.getModelName.bind(this));
        
    }


    protected getModel(){
        return this.model;
    }

    protected getModelParts(){
        return this.modelConfig.parts;
    }

    public getModelDimensions() {

        const box = new THREE.Box3().setFromObject( this.getModel() );
        const center = box.getCenter(new THREE.Vector3());
        const size = box.getSize(new THREE.Vector3());

        return {
            size: size, 
            center: center,
        };
    }

    protected async getProductPosition(){
        
        const dimensions = this.getModelDimensions();
        return new Promise(resolve => resolve(dimensions.center));

    }
   

    /* 
        set model scale (optional)
        set model position (optional)
    */
    protected async applyOptions(model, options){

        if(options.scale) model.scale.set(options.scale, options.scale, options.scale);
        if(options.position) model.position.copy(options.position);
        if(options.rotation) model.rotation.copy(options.rotation);

        return new Promise(resolve => {
            resolve(model);
        })
    }


    /*
        if material is reused when creating 3d model it might change multiple parts instead of just one because materials are cloned
    */
    protected async makeMaterialsUnique(model){

        return new Promise(resolve=>{
    
            model.traverse((node: any) => {
                if (node.type === 'Group') {
                    node.traverse((node2: any) => {
                        if (node2.isMesh) uniqueMaterial(node2);
                    });
                } else if (node.isMesh) {
                    uniqueMaterial(node);
                }
            });
    
            resolve(model);
    
        });
    
        function uniqueMaterial(_node){
            _node.castShadow = true; // enable shadows
            const material2 = _node.material.clone();
            if (!_node.material.map) material2.map = createWhiteTexture();
            _node.material = material2.clone();
            _node.material.needsUpdate = true;
            return _node;
       }
    }

    private getComponentByKey(key){

        for(let i=0;i< this.modelConfig.parts.length; i++){
            if(this.modelConfig.parts[i].key == key){
                return this.modelConfig.parts[i].component;
            }
        }
        
        return undefined;
    }

    // change color
    /*
        args[0] - array of parts objects: [{key: 'Front'}, {key: 'Collar'}];
        args[1] - new color, can be these formats: #FFFFFF, 0xFFFFFF
    */
    protected async changeColor(...args){

        const parts = args[0];
        const newColor = args[1];
        
        for(let i=0;i<parts.length;i++){

            let component = this.getComponentByKey(parts[i].key);
            if(component == undefined){
                console.warn('Component for color change not found!', parts[i].key, this.modelConfig.parts);
                continue;
            }

            await this.callColorChangeEventOnPart(component, parts[i], newColor );
           
        }

        Root.instance.events.broadcast(EVENT_COLOR_CHANGE_COMPLETE, null);

    }

   

     /*
        update color via events
    */
    private async callColorChangeEventOnPart(component, part, newColor){

        // sends event to receive mesh for specific part from main 3d model (for example gets FRT from Sweater )
        await Root.instance.events.call(component.getEventName(), {passTriggerCallback: true}, (mesh)=>{

            // sends event to update material with a mesh just received
            Root.instance.events.call(EVENT_UPDATE_MATERIAL, null, null, part.key, mesh, newColor, part.layerKey );
            
            return new Promise(resolve => resolve());

        });


    }

    protected async changeCustomText(...args){

        // console.log('- changeCustomText', args);

        const parts = args[0];
        const textParams = args[1];
        
        let changesMade = 0;
        
        for(let i=0;i<parts.length;i++){

            let component = this.getComponentByKey(parts[i].key);
            if(component == undefined){
                console.warn('Component for text change not found!', parts[i].key, this.modelConfig.parts);
                continue;
            }

            await this.callCustomTextChangeOnPart(component, parts[i], textParams );
            changesMade++;

        }

        if(changesMade > 0) Root.instance.events.broadcast(EVENT_TEXT_CHANGE_COMPLETE, null);

    }

    private async callCustomTextChangeOnPart(component, part, textParams){

         // sends event to receive mesh for specific part from main 3d model (for example gets FRT from Sweater )
         await Root.instance.events.call(component.getEventName(), {passTriggerCallback: true}, (mesh)=>{

            // sends event to update material with a mesh just received
            Root.instance.events.call(EVENT_UPDATE_CUSTOM_TEXT, null, null, part.key, mesh, textParams); // part.layerKey
            
            return new Promise(resolve => resolve());

        });

    }

    protected async changeMask(...args) {

        const parts = args[0];
        const newMask = args[1];
        const groupKey = args[2];

        // FOR TESTING pURPOSES
        // middleware can be vibrant.js
        // ---- categorizing colors into its constituents

        let colors = [
            "090a07",
            "f5f3e3",
            "9f1c28",
            '35469d',
            '9f7d5e',
            '737474',
            '173321',
            'df6536'
        ]
        
        // TEMP
        // HARDCODED
        var processedMask = new MaskProcessor(300, 500);
        let newCanvas = await processedMask.processCanvas(newMask, colors);
        // let colorWeights = await processedMask.getColorWeight(newCanvas);
        // console.log(colorWeights);


        // processedMask.await
        // await colorReduction;
        
        for(let i=0;i<parts.length;i++){

            let component = this.getComponentByKey(parts[i].key);
            if(component == undefined){
                console.warn('Component for color change not found!', parts[i].key, this.modelConfig.parts);
                continue;
            }

            // reduce color
            // place mask


            await this.callMaskChangeOnPart(component, parts[i], newMask );
           
        }

        // Root.instance.events.broadcast(EVENT_COLOR_CHANGE_COMPLETE, null);

    }

    private async callMaskChangeOnPart(component, part, newCanvas) {

        await Root.instance.events.call(component.getEventName(), {passTriggerCallback: true}, (mesh)=>{

            // sends event to update material with a mesh just received
            Root.instance.events.call(EVENT_UPDATE_MASK, null, null, part.key, mesh, newCanvas); // part.layerKey
            
            return new Promise(resolve => resolve());

        });

    }


    // TODO - move to MaterialFacade??
    protected async applyInitialMaterials(){

        for(let i=0;i< this.modelConfig.parts.length; i++){              
               await this.triggerMaterialChange(this.modelConfig.parts[i].component);
        }

        Root.instance.events.broadcast(EVENT_TEXTURES_APPLIED, null);

    }


    protected async triggerMaterialChange(component){
        
        
        // component event name - from e.g. component/products/calamigos/sweater/parts/Front.ts (or other parts)
        const componentEventName = component.getEventName();
        const componentKey = component.getKey();

        // const productType = await Root.instance.events.call(EVENT_GET_PRODUCT_TYPE, null, true);

        let textParams: any = [];
        if(Root.instance.events.exists(EVENT_GET_CUSTOM_TEXT_PARAMS)){
            textParams = await Root.instance.events.call(EVENT_GET_CUSTOM_TEXT_PARAMS, null, true, componentKey) as any; // componentKey = front, back, etc
        }

        // get material
        let material = await Root.instance.events.call(EVENT_BUILD_MATERIAL, null, true, componentKey, null, textParams) as THREE.Material;

        if(typeof material === 'undefined' || material === null){
            console.warn('Material for change not found', componentKey);
            return new Promise(resolve => resolve());
        }

        // save text params values
        for(let i=0;i<textParams.length;i++){
            let textParam = textParams[i];
            Root.instance.events.call(EVENT_SAVE_CUSTOM_TEXT_VALUES, null, null, componentKey, textParam);
        }

        // get parts as 3D objects that will be modified
        await Root.instance.events.call(componentEventName, {passTriggerCallback: true}, (mesh)=>{

            if(typeof mesh === 'undefined' || mesh === null){
                console.warn('Mesh not found in triggerMaterialChange', componentKey);
                return new Promise(resolve => resolve());
            }

            // console.log(material);

            if (mesh.material) mesh.material.dispose();

            material.needsUpdate = true;
            mesh.material = material;
            mesh.material.needsUpdate = true;
            
            return new Promise(resolve => resolve());

        });


        // console.log(mesh);

    }

    /*
        returns array of part names (from array with objects that has param 'key' that defines part's name )
    */
   private partsToChangeArray(parts){
        let partsToChange = [];
        for(let i=0;i<parts.length;i++){
            let partKey = parts[i].key;
            partsToChange.push(partKey);
        }
        return partsToChange;
    }
    

 

}