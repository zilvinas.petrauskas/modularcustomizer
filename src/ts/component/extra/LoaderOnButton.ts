import { Root } from 'Root';
import { EVENT_PRELOADER_START, EVENT_CUSTOMIZER_READY } from 'helpers/constants';
import { qs } from 'helpers/helpers';

export class LoaderOnButton{

    protected line : HTMLElement;
    protected parentElement : HTMLElement;
    protected options : any = {
        className: '',
        color: '',
        relativeParent: false,
    }
    protected currentProgress = 0; // whats visible
    protected waitingProgress = 0; // whats coming..
    protected loop : any;
    protected completed : boolean = false;

    constructor(query, options={}){

        this.parentElement = qs(query)[0];
        this.options = options;

        if(typeof this.parentElement === 'undefined'){
            console.warn('!! Parent element not found with query: ',query);
            return;
        }

        this.listen();
        this.html();

        this.startLoading();
        this.animate(true);
    }

    protected listen(){

        // Root.instance.events.listen(EVENT_PRELOADER_START, this.startLoading.bind(this));
        // Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.endLoading.bind(this));
        Root.instance.events.listen(EVENT_PRELOADER_START, this.endLoading.bind(this));
    }

   

    protected animate = (loop: boolean = false) => {

        if (!this.completed) {

            if (this.currentProgress >= 100) {

                this.completed = true;
                this.onComplete();

            } else if (this.currentProgress < this.waitingProgress) {
                this.currentProgress += 1;
                this.currentProgress = Math.min(100, this.currentProgress); // cap at 100
                this.updateVisibleProgressValue();
            }

            if (loop === true) {
                let normal = 1000 / 30;
                let faster = 1000 / 45; // shorter delay when visual progress is way behind actual progress
                let delay = this.waitingProgress - this.currentProgress > 33 ? faster : normal;
                this.loop = setTimeout(() => {
                    this.animate(true);
                }, delay);
            }

        }

    }

    protected updateVisibleProgressValue(){
        this.line.style.width = this.currentProgress+'%';
    }

    protected onComplete(){

        this.line.style.display = 'none';

    }

    protected set(value){
        if(value > this.waitingProgress) this.waitingProgress = value;
    }

    protected startLoading(){
        this.set(90);
    }

    protected endLoading(){
        this.set(100);
    }

    protected html(){

        let line = document.createElement('span');

        if(typeof this.options.className !== 'undefined' && this.options.className !== ''){
            line.setAttribute('class', this.options.className)
        }else{
            line = this.lineStyle(line);
        }

        this.line = line;
        this.parentElement.appendChild(line);
    }

    protected lineStyle(line){

        line.style.position = 'absolute';
        line.style.left = 0;
        line.style.bottom = 0;
        line.style.height = '4px';
        line.style.width = '0%';

        if(this.options.color !== ''){
            line.style.background = this.options.color;
        }else{
            line.style.background = "rgb(145, 178, 174)";
        }

        return line;
    }
}