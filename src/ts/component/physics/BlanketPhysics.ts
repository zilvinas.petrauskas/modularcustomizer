
import * as THREE from 'three';

export class BlanketPhysics{

    protected restDistance: number;
    protected xSegs: number;
    protected ySegs: number;
    protected clothFunction: any;
    protected clothWidth : number;
    protected clothHeight : number;
    protected ballSize: number = 60; //40

    protected clothGeometry: any;
    protected ball: any;

    public ctrl: any;

    constructor(restDistance: number, xSegs: number, ySegs: number){

        this.restDistance = restDistance || 25;
        this.xSegs = xSegs || 10;
        this.ySegs = ySegs || 10;

        this.clothWidth = restDistance * xSegs;
        this.clothHeight = restDistance * ySegs;

        this.clothFunction = this.plane( this.clothWidth, this.clothHeight );
        this.createBall();
    }

    public getController(){
        this.ctrl = this.hell();
        return this.ctrl;
    }

    public setClothGeometry(clothGeometry){
        this.clothGeometry = clothGeometry;
    }

    public getWidth(){
        return this.clothWidth;
    }

    public getHeight(){
        return this.clothHeight;
    }

    public getClothFunction(){
        return this.clothFunction;
    }

    public getXSegs(){
        return this.xSegs;
    }

    public getYSegs(){
        return this.ySegs;
    }

    public plane(_width?, _height?){

        let width = _width != undefined ? _width : this.restDistance * this.xSegs;
        let height = _height != undefined ? _height : this.restDistance * this.ySegs;

        return function ( u, v, target ) {
            let x = ( u - 0.5 ) * width;
            let y = height - ( v + 0.5 ) * height;
            let z = 0;
            target.set( x, y, z );
        };
    }

    public test(){

        /* testing cloth simulation */
        var pinsFormation = [];
        var pins = [ 6 ];
        pinsFormation.push( pins );
        pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];
        pinsFormation.push( pins );
        pins = [ 0 ];
        pinsFormation.push( pins );
        pins = []; // cut the rope ;)
        pinsFormation.push( pins );
        pins = [ 0, this.getWidth() ]; // classic 2 pins
        pinsFormation.push( pins );
        pins = pinsFormation[ 1 ];
        function togglePins() {
            pins = pinsFormation[ ~ ~ ( Math.random() * pinsFormation.length ) ];
        }

    }

    protected createBall(){

        var ballGeo = new THREE.SphereBufferGeometry( this.ballSize, 32, 16 );
        var ballMaterial = new THREE.MeshLambertMaterial();

        this.ball = new THREE.Mesh( ballGeo, ballMaterial );
        this.ball.visible = false;
        this.ball.castShadow = false;
        this.ball.receiveShadow = false;

    }

    public hell(){

        const scope = this;

        var params = {
            enableWind: true,
            showBall: false,
        };

        var DAMPING = 0.03;
        var DRAG = 1 - DAMPING;
        var MASS = 0.1;

        var cloth = new Cloth( scope.xSegs, scope.ySegs );
        var GRAVITY = 981 * 1.4;
        var gravity = new THREE.Vector3( 0, - GRAVITY, 0 ).multiplyScalar( MASS );
        var TIMESTEP = 18 / 1000;
        var TIMESTEP_SQ = TIMESTEP * TIMESTEP;
        var pins = [];
        var windForce = new THREE.Vector3( 0, 0, 0 );
        var ballPosition = new THREE.Vector3( 0, - 45, 0 );
        // var ballSize = 60; //40
        var tmpForce = new THREE.Vector3();
        var lastTime;

        function Particle( x, y, z, mass ) {
            this.position = new THREE.Vector3();
            this.previous = new THREE.Vector3();
            this.original = new THREE.Vector3();
            this.a = new THREE.Vector3( 0, 0, 0 ); // acceleration
            this.mass = mass;
            this.invMass = 1 / mass;
            this.tmp = new THREE.Vector3();
            this.tmp2 = new THREE.Vector3();
            // init
            scope.clothFunction( x, y, this.position ); // position
            scope.clothFunction( x, y, this.previous ); // previous
            scope.clothFunction( x, y, this.original );
        }
        // Force -> Acceleration
        Particle.prototype.addForce = function ( force ) {
            this.a.add(
                this.tmp2.copy( force ).multiplyScalar( this.invMass )
            );
        };
        // Performs Verlet integration
        Particle.prototype.integrate = function ( timesq ) {
            var newPos = this.tmp.subVectors( this.position, this.previous );
            newPos.multiplyScalar( DRAG ).add( this.position );
            newPos.add( this.a.multiplyScalar( timesq ) );
            this.tmp = this.previous;
            this.previous = this.position;
            this.position = newPos;
            this.a.set( 0, 0, 0 );
        };
        var diff = new THREE.Vector3();
        function satisfyConstraints( p1, p2, distance ) {
            diff.subVectors( p2.position, p1.position );
            var currentDist = diff.length();
            if ( currentDist === 0 ) return; // prevents division by 0
            var correction = diff.multiplyScalar( 1 - distance / currentDist );
            var correctionHalf = correction.multiplyScalar( 0.5 );
            p1.position.add( correctionHalf );
            p2.position.sub( correctionHalf );
        }
        function Cloth( w, h ) {
            w = w || 10;
            h = h || 10;
            this.w = w;
            this.h = h;
            var particles = [];
            var constraints = [];
            var u, v;
            // Create particles
            for ( v = 0; v <= h; v ++ ) {
                for ( u = 0; u <= w; u ++ ) {
                    particles.push(
                        new Particle( u / w, v / h, 0, MASS )
                    );
                }
            }
            // Structural
            for ( v = 0; v < h; v ++ ) {
                for ( u = 0; u < w; u ++ ) {
                    constraints.push( [
                        particles[ index( u, v ) ],
                        particles[ index( u, v + 1 ) ],
                        scope.restDistance
                    ] );
                    constraints.push( [
                        particles[ index( u, v ) ],
                        particles[ index( u + 1, v ) ],
                        scope.restDistance
                    ] );
                }
            }
            for ( u = w, v = 0; v < h; v ++ ) {
                constraints.push( [
                    particles[ index( u, v ) ],
                    particles[ index( u, v + 1 ) ],
                    scope.restDistance
                ] );
            }
            for ( v = h, u = 0; u < w; u ++ ) {
                constraints.push( [
                    particles[ index( u, v ) ],
                    particles[ index( u + 1, v ) ],
                    scope.restDistance
                ] );
            }
            // While many systems use shear and bend springs,
            // the relaxed constraints model seems to be just fine
            // using structural springs.
            // Shear
            // var diagonalDist = Math.sqrt(restDistance * restDistance * 2);
            // for (v=0;v<h;v++) {
            // 	for (u=0;u<w;u++) {
            // 		constraints.push([
            // 			particles[index(u, v)],
            // 			particles[index(u+1, v+1)],
            // 			diagonalDist
            // 		]);
            // 		constraints.push([
            // 			particles[index(u+1, v)],
            // 			particles[index(u, v+1)],
            // 			diagonalDist
            // 		]);
            // 	}
            // }
            this.particles = particles;
            this.constraints = constraints;
            function index( u, v ) {
                return u + v * ( w + 1 );
            }
            this.index = index;
        }
        function simulate( time ) {
            if ( ! lastTime ) {
                lastTime = time;
                return;
            }
            var i, j, il, particles, particle, constraints, constraint;
            // Aerodynamics forces
            if ( params.enableWind ) {
                var indx;
                var normal = new THREE.Vector3();
                var indices = scope.clothGeometry.index;
                var normals = scope.clothGeometry.attributes.normal;
                particles = cloth.particles;
                for ( i = 0, il = indices.count; i < il; i += 3 ) {
                    for ( j = 0; j < 3; j ++ ) {
                        indx = indices.getX( i + j );
                        normal.fromBufferAttribute( normals, indx );
                        tmpForce.copy( normal ).normalize().multiplyScalar( normal.dot( windForce ) );
                        particles[ indx ].addForce( tmpForce );
                    }
                }
            }
            for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {
                particle = particles[ i ];
                particle.addForce( gravity );
                particle.integrate( TIMESTEP_SQ );
            }
            // Start Constraints
            constraints = cloth.constraints;
            il = constraints.length;
            for ( i = 0; i < il; i ++ ) {
                constraint = constraints[ i ];
                satisfyConstraints( constraint[ 0 ], constraint[ 1 ], constraint[ 2 ] );
            }
            // Ball Constraints
            ballPosition.z = - Math.sin( Date.now() / 600 ) * 90; //+ 40;
            ballPosition.x = Math.cos( Date.now() / 400 ) * 70;
            if ( params.showBall ) {
                scope.ball.visible = true;
                for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {
                    particle = particles[ i ];
                    var pos = particle.position;
                    diff.subVectors( pos, ballPosition );
                    if ( diff.length() < scope.ballSize ) {
                        // collided
                        diff.normalize().multiplyScalar( scope.ballSize );
                        pos.copy( ballPosition ).add( diff );
                    }
                }
            } else {
                scope.ball.visible = false;
            }
            // Floor Constraints
            for ( particles = cloth.particles, i = 0, il = particles.length; i < il; i ++ ) {
                particle = particles[ i ];
                pos = particle.position;
                if ( pos.y < - 250 ) {
                    pos.y = - 250;
                }
            }
            // Pin Constraints
            for ( i = 0, il = pins.length; i < il; i ++ ) {
                var xy = pins[ i ];
                var p = particles[ xy ];
                p.position.copy( p.original );
                p.previous.copy( p.original );
            }
        }



        function animate() {

            var time = Date.now();

            var windStrength = (Math.cos( time / 7000 ) * 20 + 40) / 8;

            windForce.set( Math.sin( time / 2000 ), Math.cos( time / 3000 ), Math.sin( time / 1000 ) );
            windForce.normalize();
            windForce.multiplyScalar( windStrength );

            simulate( time );
            render();

        }

        function render() {

            var p = cloth.particles;

            for ( var i = 0, il = p.length; i < il; i ++ ) {

                var v = p[ i ].position;

                scope.clothGeometry.attributes.position.setXYZ( i, v.x, v.y, v.z );

            }

            scope.clothGeometry.attributes.position.needsUpdate = true;

            scope.clothGeometry.computeVertexNormals();

            scope.ball.position.copy( ballPosition );


        }

        function enableWind() {
            params.enableWind = true;
        }

        function disableWind() {
            params.enableWind = false;
        }

        return {
            animate: animate,
            enableWind: enableWind,
            disableWind: disableWind,
        }
    }

    

}

