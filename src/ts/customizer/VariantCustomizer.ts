import { Root } from 'Root';

import { Scene } from 'module/scene/Scene';
import { SceneControls } from 'module/scene/SceneControls';
import { GrassField } from 'module/environment/GrassField';
import { WhiteBackground } from 'module/environment/WhiteBackground';
import { DefaultPanorama } from 'module/environment/DefaultPanorama';
import { EVENT_PRELOADER_START } from 'helpers/constants';
import { DefaultLights } from 'module/lights/DefaultLights';
import { ProductFacade } from 'module/product/ProductFacade';
import { Product } from 'module/product/Product';
import { StateManager } from 'module/manager/StateManager';
import { ProgressBar } from 'module/progress/ProgressBar';
import { UiController } from 'module/ui/UiController';
import { NodeMaterialFacade } from 'facade/NodeMaterialFacade';
import { ExampleMaterialFacade } from 'facade/ExampleMaterialFacade';
import { Poles } from 'module/environment/Poles';
import { VariantPolesFacade } from 'facade/VariantPolesFacade';
import { TestMaterialFacade } from 'facade/TestMaterialFacade';
import { ShopifyEvents } from 'module/ui/ShopifyEvents';
import { qs } from 'helpers/helpers';
import { DebugCustomizer } from 'helpers/DebugCustomizer';
import { OldNormalMaterial } from 'module/material/OldNormalMaterial';
import { ProductTypeDetector } from 'module/detector/ProductTypeDetector';
import { Screenshot } from 'module/product/Screenshot';
import { DynamicPropertiesInjector } from 'module/order/DynamicPropertiesInjector';
import { NearestColor } from 'module/detector/NearestColor';
import { ColorRules } from 'component/colors/ColorRules';
import { Exporter } from 'module/exporters/Exporter';
import { OrderManager } from 'module/order/OrderManager';
import { Outro } from 'module/progress/Outro';
import { DefaultExporterFacade } from 'facade/DefaultExporterFacade';
import { LoaderOnButton } from 'component/extra/LoaderOnButton';

export namespace Variant{ 
    
    // init like this on page load -> var customizer = new Variant.Customizer();
    // if we need things differently, we can create XilliamCustomizer.ts with different namespace and init it like this: var customizer = new Xilliam.Customizer();

    export class Customizer{

        constructor(){
            console.log('Variant Customizer v0.8.0');
        }
        
        // load all modules using async/await
        // methods that require something to preload, should be loaded after preloader module
        public async init(){
            /*
                load modules (change order later on, e.g. three.js scene should load before product or environment)
                - product (depends on productConfig to load correct model, textures, etc))
                - three.js scene (can depend on websiteConfig - for zooming, control speed, etc)
                - environment (can partially depend on websiteConfig)
                - PINS? is this also an environment component? (should depend on websiteConfig?)
                - UI (depends on websiteConfig to konw which UI version to load)
                - 
            */

           await Root.instance.modules.add('product-type-detector', new ProductTypeDetector() );
           await Root.instance.modules.add('nearest-color-detector', new NearestColor() );
           await Root.instance.modules.add('color-rules', new ColorRules() );
           
           // slim progress bar line which shows percentages completes
           await Root.instance.modules.add('progress-bar', new ProgressBar(
               { 
                progressContainer: qs('#customizerProgressBar')[0],
                progressValueContainer: qs('#customizerProgressValue')[0],
                textContainer: qs('#customizerProgressText')[0],
               }
            ) ); 

            await Root.instance.modules.add('outro', new Outro(
                { 
                 progressContainer: qs('#customizerOutro')[0],
                 progressValueContainer: qs('#outroProgressValue')[0],
                 textContainer: qs('#outroProgressText')[0],
                }
             ) ); 

             // manages states like - preloading/loading/product creation/ui creation - once everything is done - show custromizer and hide progress bar
           await Root.instance.modules.add('state-manager', new StateManager() ); 


            // three.js scene
            // await Root.instance.modules.add('scene', new DummyScene() );

            await Root.instance.modules.add('scene', new Scene() );
            await Root.instance.modules.add('scene-controls', new SceneControls() );
            await Root.instance.modules.add('scene-lights', new DefaultLights() );
            await Root.instance.modules.add('scene-panorama', new DefaultPanorama() );

             // await Root.instance.modules.add('scene-grass', new GrassField() );
            await Root.instance.modules.add('scene-white-background', new WhiteBackground() ); // if model is black - disable this one

            /* all scene related modules could be wrapped under one SceneFacade if needed! TO make this part a bit cleaner */
            
            // await Root.instance.modules.add('scene-2D', new Scene2D() );

            // material/textures handling
            await Root.instance.modules.add('material-controller', new NodeMaterialFacade() ); 

            // product  - will need facade with more products??
            // await Root.instance.modules.add('product-facade', new ProductFacade() ); // controls logics between product and other modules
            
            await Root.instance.modules.add('product', new Product() ); // information about product  // TEMP disable
               
            
            // extras for model
            // await Root.instance.modules.add('product-pins', new Pins() );
        
            // extras for scene
            // await Root.instance.modules.add('scene-poles', new VariantPolesFacade() );
// 
            // UI (+version) based of config values
            await Root.instance.modules.add('ui', new UiController() );
           

            await Root.instance.modules.add('shopify-buttons', new ShopifyEvents() );
            
            // exporting product data to backend
            await Root.instance.modules.add('exporter', new DefaultExporterFacade() );


            await Root.instance.modules.add('dynamic-properties', new DynamicPropertiesInjector() ); // information about product 


             // add to cart functionality
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // await Root.instance.modules.add('cart', new ShopifyCart() );
            // new OrderManager( new ShopifyCart() );
            // or
            // new CalamigosOrderManager(); and inside call for ShopifyCart();

            // order manager
            await Root.instance.modules.add('order-manager', new OrderManager() );
            // await Root.instance.modules.add('order-manager', new CalamigosOrderManager() );


            await Root.instance.modules.add('scene-screenshot', new Screenshot() );

            await Root.instance.modules.add('debug-customizer', new DebugCustomizer() );


            // idea
            await Root.instance.modules.add('loader-on-button1', new LoaderOnButton('#product-customize-button', { color: '#8eb1ac' }) );


            // trigger texture load once modules are initiated - once preloader is done, modules will finish loading
            Root.instance.events.call(EVENT_PRELOADER_START, null, null);
        }
    
    
    }
}
