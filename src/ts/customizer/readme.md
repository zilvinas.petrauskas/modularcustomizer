Each different customizer will have its own class
E.g. Calamigos - will have a VariantCustomizer class (default one)
E.g. Xilliam - will have its own unique XilliamCustomizer class (different from default)