import { Root } from 'Root';

import { SceneControls } from 'module/scene/SceneControls';
import { WhiteBackground } from 'module/environment/WhiteBackground';
import { DefaultPanorama } from 'module/environment/DefaultPanorama';
import { EVENT_PRELOADER_START } from 'helpers/constants';
import { DefaultLights } from 'module/lights/DefaultLights';
import { Product } from 'module/product/Product';
import { StateManager } from 'module/manager/StateManager';
import { ProgressBar } from 'module/progress/ProgressBar';
import { UiController } from 'module/ui/UiController';
import { NodeMaterialFacade } from 'facade/NodeMaterialFacade';
import { qs } from 'helpers/helpers';
import { DebugCustomizer } from 'helpers/DebugCustomizer';
import { Screenshot } from 'module/product/Screenshot';
import { DynamicPropertiesInjector } from 'module/order/DynamicPropertiesInjector';
import { NearestColor } from 'module/detector/NearestColor';
import { ColorRules } from 'component/colors/ColorRules';
import { Exporter } from 'module/exporters/Exporter';
import { OrderManager } from 'module/order/OrderManager';
import { Outro } from 'module/progress/Outro';
import { XilliamProductTypeDetector } from 'module/detector/XilliamProductTypeDetector';
import { VariantPolesFacade } from 'facade/VariantPolesFacade';
import { XilliamScene } from 'module/scene/XilliamScene';
import { XilliamSceneControls } from 'module/scene/XilliamSceneControls';
import { GrassField } from 'module/environment/GrassField';
import { XilliamProgressBar } from 'module/progress/XilliamProgressBar';
import { Scene } from 'module/scene/Scene';
import { XilliamUiController } from 'module/ui/XilliamUiController';
import { InvisibleProgressBar } from 'module/progress/InvisibleProgressBar';
import { XilliamGreyBackground } from 'module/environment/XilliamGreyBackground';

export namespace XilliamDemo {

    export class Customizer {
        
        constructor() {

            window.scrollTo(0, 0);
        }

        public async init() {

            console.log('Xilliam DEMO Customizer v0.1.0');

            await Root.instance.modules.add('product-type-detector', new XilliamProductTypeDetector());
            await Root.instance.modules.add('nearest-color-detector', new NearestColor());
            await Root.instance.modules.add('color-rules', new ColorRules());

            // slim progress bar line which shows percentages completes
            await Root.instance.modules.add('progress-bar', new InvisibleProgressBar(
                {
                    progressContainer: qs('#customizerProgressBar')[0],
                    progressValueContainer: qs('#customizerProgressValue')[0],
                    textContainer: qs('#customizerProgressText')[0],
                }
            ));

            // manages states like - preloading/loading/product creation/ui creation - once everything is done - show custromizer and hide progress bar
            await Root.instance.modules.add('state-manager', new StateManager()); // TODO - fix event arguments coming to stateUpdate fn

            // three.js scene

            await Root.instance.modules.add('scene', new XilliamScene());
            //  await Root.instance.modules.add('scene', new Scene() );

            await Root.instance.modules.add('scene-controls', new XilliamSceneControls());
            //  await Root.instance.modules.add('scene-controls', new SceneControls() );

            await Root.instance.modules.add('scene-lights', new DefaultLights());

            // await Root.instance.modules.add('scene-white-background', new XilliamGreyBackground() );
            //  await Root.instance.modules.add('scene-panorama', new DefaultPanorama() );

            // await Root.instance.modules.add('scene-grass', new GrassField());
            // await Root.instance.modules.add('scene-poles', new VariantPolesFacade());

            // material/textures handling
            await Root.instance.modules.add('material-controller', new NodeMaterialFacade()); // PROBABLY XilliamNodeMaterialFacade !

            // will need facade with more products !!
            // await Root.instance.modules.add('product-facade', new ProductFacade() ); // controls logics between product and other modules
            await Root.instance.modules.add('product', new Product()); // information about product  // TEMP disable

            // await Root.instance.modules.add('dynamic-properties', new DynamicPropertiesInjector()); // TEMP disable

            // extras for model
            // await Root.instance.modules.add('product-pins', new Pins() );

            // UI (+version) based of config values
            // await Root.instance.modules.add('ui', new UiController());
            await Root.instance.modules.add('ui', new XilliamUiController());

            // await Root.instance.modules.add('shopify-buttons', new ShopifyEvents() );

            // exporting product data to backend
            // await Root.instance.modules.add('exporter', new Exporter()); // TEMP disable
            // await Root.instance.modules.add('exporter', new CalamigosProductExporter() );


            // add to cart functionality
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // TODO - shopify cart + order manager
            // await Root.instance.modules.add('cart', new ShopifyCart() );
            // new OrderManager( new ShopifyCart() );
            // or
            // new CalamigosOrderManager(); and inside call for ShopifyCart();

            // order manager
            // await Root.instance.modules.add('order-manager', new OrderManager()); // TEMP disable
            // await Root.instance.modules.add('order-manager', new CalamigosOrderManager() );


            // extras
            await Root.instance.modules.add('screenshot', new Screenshot());

            await Root.instance.modules.add('debug-customizer', new DebugCustomizer());


            // trigger texture load once modules are initiated - once preloader is done, modules will finish loading
            Root.instance.events.call(EVENT_PRELOADER_START, null, null);
        }
    }
}