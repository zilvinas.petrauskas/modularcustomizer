
import { ALL_CONFIGS_LOADED, EVENT_RESUME_CUSTOMIZER } from 'helpers/constants';

import { Modules } from 'mediator/Modules';
import { Events } from 'mediator/Events';
import { Preloader } from 'mediator/Preloader';
import { Config } from 'mediator/Config';

import { Variant } from 'customizer/VariantCustomizer';
import { Xilliam } from 'customizer/XilliamCustomizer';
import { XilliamDemo } from 'customizer/XilliamDemoCustomizer';
import { DemoLevis } from 'customizer/DemoLevisCustomizer';
import { Empty } from 'customizer/EmptyCustomizer';

// TODO make Root a base class for VariantRoot, XilliamRoot and then thru webpack compile only neccessary files for bundle. Bundle should be named as variant-customizer-calamigos-bundle.js for calamigos and variant-customizer-xilliam-bundle.js for xilliam, etc.. Maybe even create separate folders for each version of customizer?
// TODO - RootCtrl + on load events goes where then??

export class Root{

    public static instance: Root;
    public static getInstance(): Root{
        return Root.instance || new Root();
    }

    public customizer : Variant.Customizer | Xilliam.Customizer; // define all customizer possibilities by separating it with |
    public modules : Modules;
    public events : Events;
    public preloader : Preloader;
    public config : Config;

    constructor(){

        Root.instance = this;

        this.prepare();
    }

    private async prepare(){

        this.events = new Events();
        this.events.register(ALL_CONFIGS_LOADED, this.start.bind(this));

        this.preloader = new Preloader();

        this.modules = new Modules();

        this.config = new Config();
        await this.config.init();

    }

    private async start(){

        let customizerVersion = await this.config.website.get();

        // based on WEBSITE_CONFIG - load different customizer here
        switch(customizerVersion.type){

            case 'xilliam' : this.customizer = new Xilliam.Customizer(); // load xilliam customizer
                break;

            case 'demo-x' : this.customizer = new XilliamDemo.Customizer(); // load demo customizer
                break;

            case 'demo-levis' : this.customizer = new DemoLevis.Customizer(); // load demo customizer
                break;
            case 'focus-features' : this.customizer = new DemoLevis.Customizer(); // load demo customizer
                break;

            case 'empty' : this.customizer = new Empty.Customizer(); // load demo customizer
            break;

            // case 'louis-vuitton' : this.customizer = new LouisVuitton.Customizer();
                // break;

            default:        this.customizer = new Variant.Customizer(); // load default customizer
                break;
        }

        this.customizer.init();

    }

    // resume from pause
    public async resume(params){

        Root.instance.events.call(EVENT_RESUME_CUSTOMIZER, null, null, params);
    }

}

const RootCtrl = {
    initiated : false,
    model : undefined,
    busy : false,
    timer: ()=>{
        RootCtrl.busy = true;
        setTimeout(()=>{
            RootCtrl.busy = false;
        }, 1000);
    }
}

// example how to trigger customier on page load
window.addEventListener('load', ()=>{
    // customizerStart();
})

window.addEventListener('customizer:start', (event: any)=>{
    customizerStart(event.detail);
});

function customizerStart(params?){
    
    if(RootCtrl.busy) return;
    
    RootCtrl.timer();

    if( ! RootCtrl.initiated ){
        RootCtrl.initiated = true;
        RootCtrl.model = new Root();
    }else{
        RootCtrl.model.resume(params);
    }
}
