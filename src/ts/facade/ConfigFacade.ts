import { Config } from 'mediator/Config';


export class ConfigFacade{

    public mediator : Config; // configFacade controls its mediator (config)

    constructor(_mediator){

        this.mediator = _mediator; // parent Config class

    }

}