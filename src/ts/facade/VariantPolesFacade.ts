import { Root } from 'Root';
import { EVENT_MODEL_READY, EVENT_GET_MODEL_DIMENSIONS, TYPE_SWEATER, EVENT_GET_PRODUCT_TYPE, TYPE_BLANKET, TYPE_PILLOW } from 'helpers/constants';
import { Poles } from 'module/environment/Poles';

export class VariantPolesFacade{

    private poles: Poles;

    constructor(){

        this.init();
        this.listen();
    }

    protected listen(){

        Root.instance.events.listen(EVENT_MODEL_READY, this.create.bind(this));

    }

    protected init(){

        // get product confit?
        // decide which type product is
        // use different configuration for poles

        // TODO - for optimization - check which product is active and dont load poles at all for pillow?
        // TODO - need functions that determines product type - sweater/blanket/pillow

        this.poles = new Poles();
    }

    protected async create(){

        const productType = await Root.instance.events.call(EVENT_GET_PRODUCT_TYPE, null, true);

        if(productType === TYPE_SWEATER){ 

            this.poleForSweater();

        }else if(productType === TYPE_BLANKET){ 

            this.poleForBlanket();

        }else if(productType === TYPE_PILLOW){

            
        }

    }

    private poleForSweater(){

        const config = {
            offsetY: -4.6,
        }

        this.poles.parsePoleConfig(config);
        this.poles.init();

    }

    private async poleForBlanket(){

        const dimensions = await Root.instance.events.call(EVENT_GET_MODEL_DIMENSIONS, null, true) as any;

        const config = {
            width: dimensions.size.x * 1.2,
        }

        this.poles.parsePoleConfig(config);
        this.poles.init();

    }

}