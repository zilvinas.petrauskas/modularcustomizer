
import { Root } from 'Root';
import { EVENT_BUILD_MATERIAL, EVENT_UPDATE_MATERIAL } from 'helpers/constants';
import * as THREE from 'three';
import { ExampleMaterial2 } from 'module/material/ExampleMaterial';

export class ExampleMaterialFacade{

    private materials : any = [];

    constructor(){


        this.init();
    }

    protected init(){

        // called when model components require a material
        Root.instance.events.register(EVENT_BUILD_MATERIAL, this.material.bind(this), null);

        // when color changes - update material
        Root.instance.events.register(EVENT_UPDATE_MATERIAL, this.updateMaterial.bind(this), null); // not sure how this will work, but can pass 'part name' as first variable and save node material to global variable?

    }

    /*
        arguments
        [0] - name (assign to compoments)
        [1] - ??

        returns: material
    */
    protected async material(...args){

        console.log('GET EVENT_BUILD_MATERIAL', ...args);

        const name = args[0];
        const material = await ExampleMaterial2(...args);

        this.materials[name] = material;
        
        return material;
    }

    protected async updateMaterial(...args){  
        // TODO
        const name = args[0];
        const callback = args[1];

        console.log(name, args);

        if(this.materials.hasOwnProperty(name)){
            // material exists at this.materials[name]
            const material = this.materials[name];
            // callback(updateMaterial);
            // const updateMaterial = 
            await Root.instance.events.call(EVENT_UPDATE_MATERIAL, null, (updateMaterial)=>{
                callback(updateMaterial)
            }, material);

            const updateMaterial =  await Root.instance.events.call(EVENT_UPDATE_MATERIAL, null, true, material);
            callback(updateMaterial);
        }
    }

    // an example how material could be changed on a mesh
    protected example(...args){

        let dummyMesh = new THREE.Mesh();

        // event name, callback, options, arguments
        Root.instance.events.call(EVENT_BUILD_MATERIAL, { passTriggerCallback: true }, (material: THREE.Material)=>{

            dummyMesh.material = material;
            dummyMesh.material.needsUpdate = true;

        }, args);

    }

}