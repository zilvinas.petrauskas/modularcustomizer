import { Root } from 'Root';
import { EVENT_BUILD_MATERIAL, EVENT_UPDATE_MATERIAL } from 'helpers/constants';
import * as THREE from 'three';


/*
    Parent for NodeMaterial
    - manages all the events
    - passes correct data into NodeMaterial
    
*/
export class TestMaterialFacade{
    
    private nodes: any = {};

    constructor(){

        this.init();
    }    

    protected init(){

        Root.instance.events.register(EVENT_BUILD_MATERIAL, this.material.bind(this), null);
        Root.instance.events.register(EVENT_UPDATE_MATERIAL, this.updateMaterial.bind(this), null); // not sure how this will work, but can pass 'part name' as first variable and save node material to global variable?

    }

    // TODO - return a material here to a callback??

    // returns a node material
    /*  
        args
            [0] - name (of part)
    */
    protected async material(...args){

        const name = args[0];

        // this.nodes[name] = new NodeMaterial(...args);
        // let material = await this.nodes[name].init();

        // return material;
    }

    /*
        should receive part (3d object/model part) in arguments
        and ...?

        args
        [0] - part name
        [1] - part model (3d object/mesh)
        [2] - new color
    */
    protected async updateMaterial(...args){

        const name = args[0];
        const object = args[1]; // dont need it, but keep it just in case
        const newColor = args[2];

        return this.nodes[name].changeColors(newColor);
    }

}