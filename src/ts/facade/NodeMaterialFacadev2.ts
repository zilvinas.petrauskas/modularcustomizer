import { Root } from 'Root';
import { EVENT_BUILD_MATERIAL, EVENT_UPDATE_MATERIAL, EVENT_UPDATE_MASK, EVENT_GET_COLOR_DATA, EVENT_UPDATE_CUSTOM_TEXT, EVENT_SAVE_CUSTOM_TEXT_VALUES, EVENT_GET_CUSTOM_TEXT_DATA } from 'helpers/constants';
import { NodeMaterialExtensionv2 } from 'module/material/NodeMaterialExtensionv2';


/*
    Parent for NodeMaterial
    - manages all the events
    - passes correct data into NodeMaterial
    - gets the data from NodeMaterial if needed for events
*/
export class NodeMaterialFacadev2{
    
    private nodes: any = {};

    constructor(){

        this.init();
    }    

    protected init(){

        Root.instance.events.register(EVENT_BUILD_MATERIAL, this.material.bind(this), null);
        Root.instance.events.register(EVENT_UPDATE_MATERIAL, this.updateMaterial.bind(this)); // not sure how this will work, but can pass 'part name' as first variable and save node material to global variable?

        Root.instance.events.register(EVENT_UPDATE_CUSTOM_TEXT, this.updateCustomText.bind(this));
        Root.instance.events.register(EVENT_SAVE_CUSTOM_TEXT_VALUES, this.saveCustomTextValues.bind(this)); // ONLY CALLED ON INIT

        Root.instance.events.register(EVENT_UPDATE_MASK, this.updateMask.bind(this));
        // Root.instance.events.register(EVENT_SAVE_CUSTOM_TEXT_VALUES, this.saveCustomTextValues.bind(this)); // ONLY CALLED ON INIT

        Root.instance.events.register(EVENT_GET_COLOR_DATA, this.gatherColorsData.bind(this) );
        Root.instance.events.register(EVENT_GET_CUSTOM_TEXT_DATA, this.gatherCustomTextData.bind(this) );
    }

    /*
        arguments
        [0] - name
        [1] - options
        [2] - textParams[] (initial)
    */
    protected async material(...args){

        const name = args[0];
        this.nodes[name] = this.getNodeMaterialClass(...args);

        let material = await this.nodes[name].init();
        material.needsUpdate = true;

        return new Promise(resolve => resolve(material));      
    }

    // this way we change one function in the future and have another node material class plugged in
    protected getNodeMaterialClass(...args){

        return new NodeMaterialExtensionv2(...args);
    }

    // requires NodeMaterialExtension class initiated in material
    protected async gatherColorsData(...args){

        const partName = args[0];

        if(typeof partName !== "undefined"){
            this.catchColorDataErrors(partName);
            let colorData =  await this.nodes[partName].getColorData();
            return new Promise(resolve => resolve(colorData));
        }else{

            const allData = {};
            for(let name in this.nodes){
                this.catchColorDataErrors(name);
                allData[name] = await this.nodes[name].getColorData();
            }
            return new Promise(resolve => resolve(allData));
        }

    }

    private catchColorDataErrors(name){

        if( typeof this.nodes[name].getColorData !== 'function'){
            console.warn('Node Material does not have getColorData function!!', name, this.nodes[name]);
        }
    }

    protected async gatherCustomTextData(...args){

        const partName = args[0];
        let allData = {};

        if(typeof partName !== "undefined"){
            
            this.catchColorDataErrors(partName);
            let row = await this.nodes[partName].getCustomTextData();
            if(typeof row !== 'undefined' && row !== null){
                allData[row.key] = row.value;
            }

        }else{

            allData = {};

            for(let name in this.nodes){

                this.catchColorDataErrors(name);
                let customText = await this.nodes[name].getCustomTextData();

                if(typeof customText !== 'undefined' && customText !== null){
                    for(let key in customText){
                        let row = customText[key];
                        allData[row.key] = row.value;
                    }
                }
            }
        }

        return new Promise(resolve => resolve(allData));
    }


    /*
        args
        [0] - part name
        [1] - part model (3d object/mesh)
        [2] - new color
        [3] - layer key (optional) e.g. top-pinstripe, bottom-pinstripe, etc
    */
    protected async updateMaterial(...args){ // OLD VERSION

        const name = args[0];
        const mesh = args[1];
        const newColor = args[2];
        const layerKey = args[3]; 


        let material = await this.nodes[name].changeColors(newColor, layerKey);

        if (mesh.material) {
            mesh.material = material;
            mesh.material.needsUpdate = true;
        }

        return mesh;
    }

    // triggered on INPUT change
    /*
        arguments
        [0] - part name
        [1] - part's mesh
        [2] - text arguments {key, value as color, position{x,y}, font, stitch}
    */
    protected async updateCustomText(...args){

        // part.key, mesh, text, color, part.layerKey
        const name = args[0];
        const mesh = args[1];
        const textParams = args[2]; // text params
        

        // const text = args[2];
        // const color = args[3];
        // const layerKey = args[4]; // not sure if this even needed?? because text will be written on body part not on layer

        this.saveCustomTextValues(name, textParams);
        // this.nodes[name].saveCustomTextValues(t.key, t); // save values for later usage (requires NodeMaterialExtension)

        let material = await this.nodes[name].changeCustomText(textParams); // draw custom text

        if (mesh.material) {
            mesh.material = material;
            mesh.material.needsUpdate = true;
        }

        return mesh;

    }

    protected updateMask(...args) {

        console.log("UPDATING MASK")

    }

    /*
        arguments
        [0] - part name (string)
        [1] - text params (object)
    */
    protected saveCustomTextValues(...args){

        const partName = args[0];
        const textParams = args[1];

        if(textParams.hasOwnProperty('key') && typeof textParams !== 'undefined'){
            this.nodes[partName].saveCustomTextValues(textParams.key, textParams); // save values for later usage (requires NodeMaterialExtension)
        }

    }

}