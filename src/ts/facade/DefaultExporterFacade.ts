import { DefaultExporter } from 'module/exporters/DefaultExporter';
import { Root } from 'Root';
import { EVENT_COLORS_DATA_FOR_EXPORT, EVENT_COLLECT_PRODUCT_INFO, EVENT_GET_PRODUCT_TYPE, TYPE_PILLOW } from 'helpers/constants';
import { Exporter } from 'module/exporters/Exporter';


export class DefaultExporterFacade{

    private exporter;

    constructor(){

        this.init();
    }

    protected async init(){

        const productType = await Root.instance.events.call(EVENT_GET_PRODUCT_TYPE, null, null) as string;

        if(productType == TYPE_PILLOW){ // TODO - test new pillow exporter and update live website

            this.exporter = new Exporter(); // has its own things in the class

        }else{

            this.exporter = this.prepareExporter();
           
        }
       
    }

    protected prepareExporter(){

        Root.instance.events.register(EVENT_COLLECT_PRODUCT_INFO, this.parse.bind(this));
        
        return new DefaultExporter();
    }

    protected async parse(){

        let data = await this.exporter.parse();
        return new Promise(resolve => resolve(data));
    }

    
}