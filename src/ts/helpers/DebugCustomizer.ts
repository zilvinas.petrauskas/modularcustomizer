import { Root } from 'Root';
import { EVENT_GET_THREE_CAMERA, EVENT_COLLECT_PRODUCT_INFO, EVENT_TAKE_SCREENSHOT, EVENT_OUTRO_START, EVENT_OUTRO_ADD } from './constants';
import { isLocalhost } from './helpers';

export class DebugCustomizer{
    
    constructor(){

        this.events();
    }

    private async events(){

        if( !isLocalhost()) return;

        window.addEventListener('keyup', (e) => {

            console.log(e.keyCode, e.code);
            
            if (e.keyCode === 67) { // [C]
                
               this.getCameraPos();
                
			} else if (e.keyCode === 69) { // [E]

               this.getExportData();

			} else if (e.keyCode === 84 ){ // [T]

                this.takeScreenshotAndViewIt();

            }else if (e.keyCode === 79){ // [O]
                
                // this.triggerOutro();
            
            }else if(e.keyCode === 71){ // [G]

                // this.toggleGui();

            }
		});

    }

    /*
        returns: data in console.log
    */
    private async getExportData(){
        let exported = await Root.instance.events.call(EVENT_COLLECT_PRODUCT_INFO, null, true);
        console.log('getExportData', exported);
    }

    private async getCameraPos(){

        const camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, null) as THREE.Camera;

        console.log('camera position', camera.position);
    }

    private async takeScreenshotAndViewIt(){

       const screenshot = await Root.instance.events.call(EVENT_TAKE_SCREENSHOT, null, true) as any;

       let testParent = document.getElementsByTagName('body')[0];
       let testImg = document.createElement("img"); 
       testImg.classList.add('temp-image');
       testImg.style.width = screenshot.width+'px';
       testImg.style.height = screenshot.height+'px';
       testImg.src = screenshot.image;
       testImg.addEventListener('click', ()=>{
           testParent.removeChild(testImg);
       })
       testParent.appendChild(testImg);

    }

    private async triggerOutro(){

        Root.instance.events.call(EVENT_OUTRO_START, null, null);

        setTimeout(()=>{
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null, 'cart');
        }, 2000);

        setTimeout(()=>{
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null,  'image');
        }, 4000);
    }

    private toggleGui(){

        Root.instance.events.call('toggle_gui', null, null);

    }

}