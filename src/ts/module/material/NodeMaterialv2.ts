import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three';

import { NodeUtils } from './NodeUtils';

import { NodeMaskControl } from './NodeMaskControl';
import { NodeStitchControl } from './NodeStitchControl'

import { Root } from 'Root';
import { EVENT_PRELOADER_LIST } from 'helpers/constants';
import { parseColor, isDefined } from 'helpers/helpers';
import { SwatchTextureNodes } from './SwatchTextureNodes';

export class NodeMaterialv2 { // NodeControl

	private debug = false; // display console.logs if this is set to true

	public nodeSetting = {
		anisotropy : 2 as number,
		pixelSize: 16 as number,
	};

	private key;
	public material;

	private maskControl;
	private stitchControl;
	private swatchNodes;

	private useStitch;
	private textKeys : any = [];

	private width; // width of mask
	private height; // height of mask

	private stitchMasks: any = {};
	private stitchNormals: any = {};

// TIER 1: Original mask and data init
	private originalMask;
	private originalStitchMask;

	protected originalColors :any = {};
	private originalStitches: any = {};

// TIER 2: Declared As Nodes
	private alphaNodes :any = {};
	private alphaStitchNodes: any = {};

	private alphaBlended :any = {};
	private alphaStitchBlended :any = {};
	
	protected activeColors :any = {};
	protected activeStitches :any = {};

	private stitchNodeMasks :any = {};
	private stitchNodeNormals :any = {};
	
	private loadedTextures;

	private initialTextParams: any = []; // array of initial custom text params

    /*
        unlimited arguments supplied to  constructor
        [0] - key
		[1] - options object
		[2] - text params (optional, only if custom text exists on this part)
    */

    constructor(...args){ // unlimited arguments passed thru events

		if(this.debug) console.log('Node material arguments', ...args);

		this.key = args[0];
		const options = args[1];
		const textParams = args[2];

		if(textParams != undefined && textParams !== null){
			this.initialTextParams = textParams; 
		}

		// initialize material
		this.material = this.initNodeMaterial();
		this.applySettings(options);
		
	}

	private applySettings(options){

		if(options == undefined || Object.keys(options).length === 0) return;
		for(let param in options){
			this.nodeSetting[param] = options[param];
		}

	}

    // cant have 'await' in contructors
    public async init(){

        // get loaded textures
        await Root.instance.events.call(EVENT_PRELOADER_LIST, null, (loadedTextures) => {
			this.loadedTextures = loadedTextures;
		});

	// PREPARING material and getting necessary texture swatch

	// CHECK mask and stitch usage
	// either from config or from new image upload

		let textureKey = this.key.concat("-texture");
		textureKey = textureKey.toLowerCase();
		let stitchKey = this.key.concat("-stitch");
		stitchKey = stitchKey.toLowerCase();

		// CHECK IF IT HAS MASKS
		let useMask = this.checkTextureUsage(this.loadedTextures, textureKey);
		if (useMask) this.originalMask = await this.getMasks(this.key, "-texture");

		// CHECK IF IT HAS STITCHES
		let useStitch = this.checkTextureUsage(this.loadedTextures, stitchKey);
		this.useStitch = useStitch;
    //*** TEMP IMAGE UPLOAD
		// this.useStitch = false;
	//*** TEMP IMAGE UPLOAD
		if (useStitch) this.originalStitchMask = await this.getMasks(this.key, "-stitch");

	// 	PREPARING mask and node and color
		
		this.getDimensions();
		
		let controlConfig = {
			useMask: useMask,
			useStitch: useStitch,
			useText: false
		}
		
		await this.checkKey();

		// get all the swatches
		this.swatchNodes = new SwatchTextureNodes(this.width, this.height);
		await this.swatchNodes.init();
		let swatches = await this.swatchNodes.getAll()

		// get all the masks and stitches
		this.maskControl = new NodeMaskControl(controlConfig, this.originalMask, this.originalColors, swatches);
		await this.maskControl.init();
		this.originalColors = await this.maskControl.getReferenceColors();
		this.activeColors = this.originalColors;
		
		this.stitchControl = new NodeStitchControl(controlConfig, this.originalStitchMask, this.originalStitches, swatches);
		await this.stitchControl.init();
		this.originalStitches = await this.stitchControl.getReferenceColors();
		this.activeStitches = this.originalStitches;

		for (var key in this.activeColors) {

			let color = this.activeColors[key];
			await this.maskControl.update(color, key);

		}

		for (var key in this.activeStitches) {

			let stitch = this.activeStitches[key];
			await this.stitchControl.update(stitch, key);

		}


		// if (this.useStitch) await this.prepareStitch();
		// if (this.useStitch) this.initStitchNodes();
		// if (this.useDefaultStitch) this.initDefaultStitchNodes();

		// create custom texts if there are any
		if(this.initialTextParams.length > 0){
			for(let i=0;i<this.initialTextParams.length;i++){
				await this.applyCustomTextIfItExists(this.initialTextParams[i], false);
			}
		}
	
		// APPLYING mask and node and color 

		await this.compileAllNodes();

		return this.material;
	}

	public getMaterial(){
		return this.material;
	}

	private async checkKey() {

		let productConfig = await Root.instance.config.product.get();

		for(let i=0;i<productConfig.parts.length;i++){

			let part = productConfig.parts[i];

			if (part.key == this.key) {

				for(let y=0;y<part.layers.length;y++){

					let layer = part.layers[y];

					if (layer.type == "color" || !isDefined(layer.type)) { // COLOR TYPE MIGHT NOT BE DEFINED BECAUSE ITS DEFAULT ONE

						let value = parseColor(layer.value, 'clean');
						// in case we need nearest color here call things below..
						// let corrected = await Root.instance.events.call(EVENT_GET_NEAREST_COLOR, null, true, value);
						// if(isDefined(corrected) && corrected !== value){
						// 	let oldValue = value;
						// 	value = corrected;
						// }
						this.originalColors[layer.key] = value;

					}else if (layer.type == "stitch") {

						if (layer.key == "default" && layer.size != null) {

							//if(this.debug) console.log("Using default stitch for ", this.key)

							let value = parseColor(layer.value, 'clean');
							this.originalStitches["default"] = value;

						}

					}
				}
			}
		}
	}

	private async prepareTextLayer(layer){

		if (!this.textKeys.includes(layer.key)) this.textKeys.push(layer.key);

		// Define color for layer key
		// if (layer.color) this.updateColorNode(layer.color, layer.key)
		
		// Define stitch for layer key
		if (layer.stitch) {
			let stitchValue = this.getStitchValueBasedOfColorCode( parseColor(layer.stitch, 'clean') );
			let stitchNodes = this.initStitchTextureNodes(stitchValue);
			[this.stitchNodeMasks[layer.key], this.stitchNodeNormals[layer.key]] = stitchNodes;
		}

		return new Promise(resolve => resolve());
	}

	private checkTextureUsage(array, key) {

	// Check if Mask / StitchMask is available
    // ---------------------------------------------------------------

		return array.hasOwnProperty(key) && typeof this.loadedTextures[key] !== 'undefined';
	}

	private async getMasks(key, stringType) {

		let id = key.concat(stringType);
		id = id.toLowerCase(); 

		let mask = await this.getReference(id);

		return mask;

	}

	private async getReference(key) {

	// Get masks references from loaded Texture according to key of node material
    // ---------------------------------------------------------------

		let texture = this.loadedTextures[key];

		if (texture) {
			texture.magFilter = THREE.NearestFilter;
			texture.minFilter = THREE.NearestFilter;
			texture.flipY = false;
		}
		
		return texture;

	}

    private initNodeMaterial() {

		const material = new NODES.StandardNodeMaterial();
		
		//@ts-ignore
    	material.metalness = new NODES.FloatNode( 0 ); // doesnt exist in v109
		//@ts-ignore
		material.roughness = new NODES.FloatNode( 1 ); // doesnt exist in v109
		
		material.side = THREE.DoubleSide;
		material.needsUpdate = true;
		material.name = this.key;
		
    	return material;

	}


	// called from INIT only
	public async applyCustomTextIfItExists(textParams, compileNodes : boolean = true){

		await this.prepareTextLayer(textParams);

		let material = await this.changeCustomText(textParams, compileNodes);		
		return new Promise(resolve => resolve(material));
	}

	/*
		text : string -> custom text from html input
		color : string/color-string -> color to use for custom text
		key : string (optional) - layerKey - probably not needed? because text wil be drawn on texture related to whole model part and not texture layer?
	*/

	// TODO - BUG??? - NOT EVEN CHECKING INCOMING COLOR PARAMETER HERE??
	public async changeCustomText(...args){

		let t = args[0] // TEXT PARAMETER
		let compileNodes = args[1] || true; // shouldnt compile when called from init

		if(this.debug) console.log('READY FOR CUSTOM TEXT', args);

		if (!this.textKeys.includes(t.key)) {
			console.log("!!!! MISSING TEXT LAYER IN PRODUCT CONFIG! \nReverting text to default color (#000000) and stitch (Jacquard)")
			this.textKeys.push(t.key);
		}

	// Converting text data -> canvas -> alphaNodes & alphaStitchNodes

		let tCanvas = document.createElement('canvas');
		tCanvas.width = this.width;
		tCanvas.height = this.height;

		let tCtx = tCanvas.getContext("2d");
		tCtx.scale(1, -1);
		var posX = (t.position.x / 100 *  tCanvas.width);
		var posY = -tCanvas.height + (t.position.y/100 * tCanvas.height);
		
		tCtx.font = t.font;
		tCtx.textAlign = "center";
		tCtx.textBaseline = "middle";
		tCtx.translate(posX, posY);
		tCtx.rotate(t.rotation / 180 * Math.PI);
		tCtx.fillText(t.value, 0, 0);
		tCtx.restore();

		let tTexture = new THREE.CanvasTexture(tCanvas);
		tTexture.flipY = true;
		tTexture.needsUpdate = true;
		let tImageNode = new NODES.TextureNode(tTexture);

	// Adjusting stitch Masks and alphaNodes for colors
	// Will work even if there's no stitch used

		this.maskControl.modifyAlphaNode(t.key, tImageNode, "000000");
		// this.alphaNodes[t.key] = NodeUtils.colorToAlpha(tImageNode, "000000")	
		this.alphaStitchNodes[t.key] = this.alphaNodes[t.key]

		// Defaulting to config stitch if no stitch were defined in layers
		if (!this.stitchNodeMasks[t.key]) {
			let stitchCode = parseColor(t.stitch, 'clean');
			let stitchValue = this.getStitchValueBasedOfColorCode(stitchCode);
			let stitchNodes = this.initStitchTextureNodes(stitchValue);
			[this.stitchNodeMasks[t.key], this.stitchNodeNormals[t.key]] = stitchNodes;
		}

		let empty = new NODES.ColorNode(0x000000);
		var alpha = this.alphaNodes[t.key];
		var sAlpha = this.alphaStitchNodes[t.key];
		var sInput = this.stitchNodeMasks[t.key];

		if (this.useStitch) {
			this.alphaBlended[t.key] = NodeUtils.alphaBlendNodes(empty, sInput, alpha);
			this.alphaStitchBlended[t.key] = NodeUtils.alphaBlendNodes(empty, sInput, sAlpha);
		} else {
			this.alphaBlended[t.key] = alpha;
			this.alphaStitchBlended[t.key] = alpha;
		}		
 
		if(compileNodes) await this.compileAllNodes();

	
		// TODO - modify material and return it back

		return this.material;
	}

    public async changeColors(color, key) {

    // Change individual color/all color from the mask
	// ---------------------------------------------------------------
	
		if (key == undefined) key = "default";
		
		this.activeColors [key] = color;
		
		await this.maskControl.update(color, key);
		await this.compileAllNodes();
		return this.material;

	}
	dd
	public async changeStitch(stitch, key) {

		if (key == undefined) key = "default";

		this.activeStitches[key] = stitch;

		await this.stitchControl.update(stitch, key);
		await this.compileAllNodes();
		return this.material

	}
	
	private initOriginalCanvas(image) {
		
	// Canvas initialization for both mask / stitch original image
	// will be separated using shaders
	// ---------------------------------------------------------------
	

		let canvas = document.createElement('canvas');
		let ctx = canvas.getContext("2d");
		let imageMap = image.image;
		canvas.width = imageMap.width;
		canvas.height = imageMap.height;
		ctx.drawImage(imageMap, 0, 0);

		return [canvas, ctx]

	}

	
	

	// OPTIMIZATION - could we have stitchCodes defined in configs?
	private getStitchValueBasedOfColorCode(colorCode){

		if (colorCode == "FF0000") {
			return 'WDE';
		} else if (colorCode == "0000FF") {
			return 'JCQ';
		} else if (colorCode == "00FF00") {
			return 'CLR';
		}

		console.warn('Unknown stitch code used!', colorCode);

		return 'JCQ'; // which one is default?
	}
	
	// WHY - if its a texture/mask function iT SHOULD NOT CONTAIN STITCHES
	// IF NOT - rename a function to include 'stitch'
    private initStitchTextureNodes(stitchName) {
    
    // initialize mask for stitch nodes
	// ---------------------------------------------------------------
	
		if( ! this.stitchMasks.hasOwnProperty(stitchName)){
			console.warn('Forgot to load DEFAULT_STITCHES on one of parts in product configs??');
		}

    	const stitchTexture = this.stitchMasks[stitchName];
    	const stitchNormal = this.stitchNormals[stitchName];
    	const anisotropy = this.nodeSetting.anisotropy;
    	const pixelSize = this.nodeSetting.pixelSize;
    	var stitchRepeatX = stitchTexture.image.width / pixelSize;
    	var stitchRepeatY = stitchTexture.image.height / pixelSize;
        stitchTexture.wrapS = stitchTexture.wrapT = THREE.RepeatWrapping;
        stitchNormal.wrapS = stitchNormal.wrapT = THREE.RepeatWrapping;
        stitchTexture.anisotropy = stitchNormal.anisotropy = anisotropy;

        var rX = this.width/2/stitchRepeatX; 
        var rY = -this.height/2/stitchRepeatY;

		var nodeMask = new NODES.TextureNode(new THREE.Texture()); 
    	var nodeMask = new NODES.TextureNode(stitchTexture);
		//@ts-ignore
		nodeMask.uv = new NODES.UVTransformNode();
		//@ts-ignore
        nodeMask.uv.setUvTransform(0,0,rX,rY,0);

        var nodeNormal = new NODES.TextureNode(stitchNormal);
		//@ts-ignore
        nodeNormal.uv = new NODES.UVTransformNode();
		//@ts-ignore
		nodeNormal.uv.setUvTransform(0,0,rX,rY,0);
		
		return [nodeMask, nodeNormal]


	}

	private getDimensions() {

	// Get the width and height of the current mask
	// TODO add default parameter to repeat stitches, for e.g. in collar
	// ---------------------------------------------------------------
	
		if (this.originalMask) {
			this.width = this.originalMask.image.width;
			this.height = this.originalMask.image.height;
		} else if (this.originalStitchMask) {
			this.width = this.originalStitchMask.image.width;
			this.height = this.originalStitchMask.image.height;
		}

	}
	
    private async compileAllNodes() {
	
	// Combine all the nodes into one single material
    // ---------------------------------------------------------------
		var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
		await this.compileStitchNodes();
		let mask = await this.stitchControl.getStitchMask();
		await this.maskControl.addStitchMask(mask);

		// if ((this.useStitch || this.useDefaultStitch) && !iOS) await this.compileStitchNodes();
		await this.compileMaskNodes();
		
		// if (this.useDefault) await this.compileDefaultNodes();

		this.material.needsUpdate = true;

		
	}

	private async compileStitchNodes() {
		this.material.normal = await this.stitchControl.compile();
	}


	private async compileMaskNodes() {
		this.material.color = await this.maskControl.compile();
	}

}



