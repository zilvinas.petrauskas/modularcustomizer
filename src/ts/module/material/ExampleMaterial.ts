
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_PRELOADER_LIST } from 'helpers/constants';
import { MeshStandardMaterial } from 'three';

export async function ExampleMaterial2(...args){

    console.log(' ~ ExampleMaterial constructor', ...args);

    const componentName = args[0];
    let loadedTextures: any;

      // get loaded textures object
      await Root.instance.events.call(EVENT_PRELOADER_LIST, null, (_loadedTextures)=>{
        loadedTextures = _loadedTextures;
        console.log('~ 1', loadedTextures);
    });

     // get product config
    const productConfig = Root.instance.config.product.get();
    console.log('~ 2', productConfig);

    const layers = await getComponentLayers();

    const material = await createMaterial(layers);
    console.log('~ 3', material);

    return material;


    // private functions

    async function createMaterial(layers){

        for(let i=0;i<layers.length;i++){
            if(layers[i].type == 'texture'){
                return layers[i].texture;
            }
        }

        return null;
    }

    async function getComponentLayers(){

        const layers = [];

        console.log('~ 3.5 check', loadedTextures);

        for(let i=0;i<productConfig.parts.length;i++){
            
            let part = productConfig.parts[i];

            for(let y=0;y<part.layers.length;y++){

                let layer = part.layers[y];

                if(layer.type == 'texture'){

                    let texture = new MeshStandardMaterial({
                        map: loadedTextures[ layer.key ]
                    });

                    layers.push({
                        type: layer.type,
                        texture: texture,
                    })


                }else if(layer.type == 'normal'){
                    // TODO
                }
            }
        }


        return layers;
    }
}