import * as NODES from 'three/examples/jsm/nodes/Nodes';
import { NodeUtils } from './NodeUtils';
import { initOriginalCanvas } from 'helpers/helpers';

export class NodeControl { 

    protected config : any = {};

    protected mask;
    protected key;
    protected swatches;
    protected empty;
    protected stitchEmpty;

    protected alphaNodes : any = {};
    protected alphaSwatchMasks : any = {};
    protected alphaBlended : any = {};

    protected textKeys : any = [];

    protected referenceColors : any = {};
    protected colorKeys; 

    constructor(...args){ // unlimited arguments passed thru events

        this.config = args[0];
        this.mask = args[1]; // can be canvasTexture, or any Image Texture ?
        this.colorKeys = args[2];
        this.swatches = args[3]; // for debugging

        this.empty = new NODES.ColorNode(0x000000);
        this.stitchEmpty = new NODES.ColorNode(0x8080FF); // nEED FIX

    }

    protected async initWithMask() {

        await this.parseMaskColors();
        await this.initAlphaNodes();

    }

    protected async initAlphaNodes() {

    	for (var index in this.referenceColors) {

            let mask = this.mask;
            let color = this.referenceColors[index];
    		this.alphaNodes[index] = await NodeUtils.colorToAlpha(mask, color);
            
            this.alphaBlended[index] = this.alphaNodes[index];

		}
    }

    // protected async createAlphaNode(mask, color) {

    // // alpha mask stuff
    // // ---------------------------------------------------------------    	

    //     // let color = this.referenceColors[key];
    //     // let imageNode = new NODES.TextureNode(this.mask);
    //     // this.alphaNodes[key] = NodeUtils.colorToAlpha(imageNode, color)
    //     return NodeUtils.colorToAlpha(mask, color);

    // }

    public async getReferenceColors() {
        return this.referenceColors;
    }

    protected async parseMaskColors() { 

        let img = this.mask;
		let canvas, ctx;
		[canvas, ctx] = initOriginalCanvas(img);
        this.matchMaskKeys(canvas, ctx);
        		
    }

    private matchMaskKeys(canvas, ctx) {
		
        let array = this.analyzeMaskColors(canvas, ctx);

		for (var index in array) {

			var near = false;
			
			for (var index2 in this.colorKeys) {

				if (!near) {
					
					var maskHex = array[index];
					var refHex = this.colorKeys[index2];
					near = NodeUtils.verifyColor(maskHex, refHex, 5);
					if (near) this.referenceColors[index2] = array[index];

				}

			}

			if (!near) this.referenceColors[index] = array[index];

		}

        //if(this.debug) console.log ("Original Colors is ", this.originalColors)
        		
    }
    
    private analyzeMaskColors(canvas, ctx) {

    // separating masks into their different colors
    // ---------------------------------------------------------------

        const width = canvas.width;
        const height = canvas.height;
        const imageData = ctx.getImageData(0, 0, width, height).data;
                
        let arrayOfColors = NodeUtils.analyzeImgData(imageData);
        let array = [];

        for (let j = 0; j < arrayOfColors.length; ++j) {

            const maskColor = arrayOfColors[j];
            array[j] = maskColor;

        }

        return array;

    }

    public modifyAlphaNode(key, texture, color) {

		let imageNode = new NODES.TextureNode(texture);
		this.alphaNodes[key] = NodeUtils.colorToAlpha(imageNode, color)	

    }


}