import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three'

import { NodeUtils } from './NodeUtils';
import { NodeControl } from './NodeControl';


export class NodeStitchControl extends NodeControl{ 

    // This module is used to control the stitch nodes of a single stitch mask in NodeMaterial

    private stitchMask;
    private stitchNodes : any = {};

    constructor(...args){ // unlimited arguments passed thru events

        super(...args);

    }

    public async init() {

        this.config.useStitch? await this.initWithMask() : this.initDefault();

    }

    protected initDefault() {
        
        if (this.colorKeys["default"] != null && this.colorKeys["default"] !== null) {
            this.referenceColors["default"] = this.colorKeys["default"];
        } else {
            this.referenceColors["default"] = "0000FF";
        }

    }

    public async update(color, key) {

        this.stitchNodes[key] = await this.updateStitchNode(color, key)
        await this.updateMaskNode(key);
        await this.updateStitchMask();

    }

    public async getStitchMask() {
        return this.stitchMask;
    }

    public async updateStitchNode(newValue, key) {

        let stitchKey = this.getStitchValueBasedOfColorCode(newValue);
        var alpha = this.alphaNodes[key];
        var input = this.swatches[stitchKey]['normal'];

        var color = NodeUtils.alphaBlendNodes(this.empty, input, alpha);

	    return color;

    }

    protected async updateStitchMask() {

        if (!this.config.useStitch) {
            this.stitchMask = new NODES.ColorNode(0xffffff);
            return;
        }

        let blendedMask = this.empty;

        for (var key in this.alphaBlended) {

            let blended = this.alphaBlended[key]
            blendedMask = NodeUtils.addNodes(blendedMask, blended);

        }

        this.stitchMask = blendedMask;
    }


    protected async updateMaskNode(key) {

        if (!this.config.useStitch) {
            this.alphaBlended['default'] = new NODES.ColorNode(0xffffff);
            return;
        }

        var empty = this.empty; 

        let colorCode = this.referenceColors[key]
        let stitchKey = this.getStitchValueBasedOfColorCode(colorCode)

        var alpha = this.alphaNodes[key];
        var input = this.swatches[stitchKey]['mask'];

        this.alphaBlended[key] = NodeUtils.alphaBlendNodes(empty, input, alpha)

        

    }

	private getStitchValueBasedOfColorCode(colorCode){

        // Optimization : make accessible thru config
		if (colorCode == "FF0000") {
			return 'WDE';
		} else if (colorCode == "0000FF") {
			return 'JCQ';
		} else if (colorCode == "00FF00") {
			return 'CLR';
		}

		console.warn('Unknown stitch code used!', colorCode);

        return 'JCQ'; // which one is default?
        
    }
    
	public async compile(text?) {
        
        await this.updateStitchMask();
        let stitch;

        if (!this.config.useStitch) {

            stitch = this.stitchNodes["default"];
            
            return stitch;

        }

		let stitchNormal = this.empty;

		// iterating through all found stitch type in stitch mask
		for (var key in this.stitchNodes) {

            let stitchAdd = this.stitchNodes[key]
			//@ts-ignore
			stitchNormal = NodeUtils.addNodes(stitchNormal, stitchAdd);	
			
        }
        
        let baseStitch = this.stitchNodes["base-value"];
        if(baseStitch = undefined || baseStitch == null){
            baseStitch = this.stitchEmpty;
        }

        // baseStitch = this.swatches['JCQ']['normal']



		var adjustedNormal = await NodeUtils.alphaBlendNodes(baseStitch, stitchNormal, this.stitchMask);
        // var adjustedNormal = stitchNormal;
        //@ts-ignore
		var normal = await new NODES.NormalMapNode(adjustedNormal);
        

        return normal;

    }

}