import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three';

import { NodeUtils } from './NodeUtils';
import { Root } from 'Root';
import { EVENT_PRELOADER_LIST } from 'helpers/constants';
import { hexToRGB, parseColor, isDefined } from 'helpers/helpers';

export class NodeMaterial { // NodeControl

	private debug = false; // display console.logs if this is set to true

	public nodeSetting = {
		anisotropy : 2 as number,
		pixelSize: 16 as number,
	};

	private key;

	public material;

	private useMask;
	private useStitch;
	private useDefault;
	private useDefaultStitch;
	private useText;
	private textKeys : any = [];

	private width; // width of mask
	private height; // height of mask

	private stitchMasks: any = {};
	private stitchNormals: any = {};

	private colorKeys: any = {};

// TIER 1: Original mask and data init
	private originalMask;
	private originalStitchMask;
	protected originalColors :any = {};
	private originalStitches: any = {};

// TIER 2: Declared As Nodes
	private alphaNodes :any = {};
	private alphaStitchNodes: any = {};

	private alphaBlended :any = {};
	private alphaStitchBlended :any = {};

	private stitchAlpha;
	
	protected activeColors :any = {};
	
	private colorNodes :any = {};
	private nodeMasks :any = {};
	private nodeNormals :any = {};
	private stitchNodeMasks :any = {};
	private stitchNodeNormals :any = {};

	private baseStitchNode;
	
	private loadedTextures;

	private initialTextParams: any = []; // array of initial custom text params
	

    /*
        unlimited arguments supplied to  constructor
        [0] - key
		[1] - options object
		[2] - text params (optional, only if custom text exists on this part)
    */

    constructor(...args){ // unlimited arguments passed thru events

		if(this.debug) console.log('Node material arguments', ...args);
		
		this.key = args[0];
		const options = args[1];
		const textParams = args[2];

		// new
		if(textParams != undefined && textParams !== null){ // custom text exists if 3rd argument has stuff defined
			this.initialTextParams = textParams; 
		}
		// end of new

		this.material = this.initNodeMaterial();

		this.applySettings(options);
		
	}

	private applySettings(options){
		if(options == undefined || Object.keys(options).length === 0) return;

		for(let param in options){
			this.nodeSetting[param] = options[param];
		}
	}

    // cant have 'await' in contructors
    public async init(){

        // get laoded textures
        await Root.instance.events.call(EVENT_PRELOADER_LIST, null, (loadedTextures) => {
			this.loadedTextures = loadedTextures;
		});

	// PREPARING material and getting necessary texture swatch

		this.getStitchTextures();

	// CHECK mask and stitch usage

		let key = this.key.concat("-texture");
		key = key.toLowerCase();
		let key2 = this.key.concat("-stitch");
		key2 = key2.toLowerCase();

		// 4 lines below can be streamlined
		this.useMask = this.checkTextureUsage(this.loadedTextures, key);
		this.useStitch = this.checkTextureUsage(this.loadedTextures, key2);
		if (this.useMask) await this.getMasks();
		if (this.useStitch) await this.getStitchMasks();

	// 	PREPARING mask and node and color
		
		this.getDimensions();
		await this.checkKey();

		if(this.useMask){
			await this.prepareMask();
		}else{
			this.useDefault = true;
		}

		if (this.useStitch) await this.prepareStitch();
		if (this.useDefault) await this.prepareDefault();


		if (this.useMask) await this.initMaskNodes();
		if (this.useStitch) await this.initStitchNodes();
		if (this.useDefault) this.initDefaultNodes();
		if (this.useDefaultStitch) this.initDefaultStitchNodes();

		// create custom texts if there are any
		if(this.initialTextParams.length > 0){
			for(let i=0;i<this.initialTextParams.length;i++){
				await this.applyCustomTextIfItExists(this.initialTextParams[i], false);
			}
		}
	
		// APPLYING mask and node and color 

		await this.compileAllNodes();
		console.log(this.material.fragment)

		return this.material;
	}

	public getMaterial(){
		return this.material;
	}

	private async checkKey() {

		let productConfig = await Root.instance.config.product.get();

		for(let i=0;i<productConfig.parts.length;i++){

			let part = productConfig.parts[i];

			if (part.key == this.key) {

				for(let y=0;y<part.layers.length;y++){

					let layer = part.layers[y];

					if (layer.type == "color" || !isDefined(layer.type)) { // COLOR TYPE MIGHT NOT BE DEFINED BECAUSE ITS DEFAULT ONE

						let value = parseColor(layer.value, 'clean');
						// in case we need nearest color here call things below..
						// let corrected = await Root.instance.events.call(EVENT_GET_NEAREST_COLOR, null, true, value);
						// if(isDefined(corrected) && corrected !== value){
						// 	let oldValue = value;
						// 	value = corrected;
						// }
						this.colorKeys[layer.key] = value;

					}else if (layer.type == "stitch") {

						if (layer.key == "default" && layer.size != null) {

							//if(this.debug) console.log("Using default stitch for ", this.key)

							this.useDefaultStitch = true;
							let value = parseColor(layer.value, 'clean');
							this.originalStitches["default"] = value;

						}

					} else if (layer.type == "text") { 
						// THIS PART JUST MAKES THINGS MORE COMPLICATED, moved to prepareTextLayer and called before creating custom text first time
					}

				}
			}
		}

	}

	private async prepareTextLayer(layer){

		if (!this.textKeys.includes(layer.key)) this.textKeys.push(layer.key);

		// Define color for layer key
		if (layer.color) this.updateColorNode(layer.color, layer.key)
		
		// Define stitch for layer key
		if (layer.stitch) {
			let stitchValue = this.getStitchValueBasedOfColorCode( parseColor(layer.stitch, 'clean') );
			let stitchNodes = this.initStitchTextureNodes(stitchValue);
			[this.stitchNodeMasks[layer.key], this.stitchNodeNormals[layer.key]] = stitchNodes;
		}

		return new Promise(resolve => resolve());
	}

	private checkTextureUsage(array, key) {

	// Check if Mask / StitchMask is available
    // ---------------------------------------------------------------

		return array.hasOwnProperty(key) && typeof array[key] !== 'undefined';
	}

	private async getMasks() {

		let key = this.key.concat("-texture");
		key = key.toLowerCase(); 

		this.originalMask = await this.getReference(key);

	}

	private async getStitchMasks() {

		let key2 = this.key.concat("-stitch");
		key2 = key2.toLowerCase(); 

		this.originalStitchMask = await this.getReference(key2);

	}

	private async getReference(key) {

	// Get masks references from loaded Texture according to key of node material
    // ---------------------------------------------------------------

		let texture = this.loadedTextures[key];

		if (texture) {
			texture.magFilter = THREE.NearestFilter;
			texture.minFilter = THREE.NearestFilter;
			texture.flipY = false;
		}
		
		return texture;

	}

	private async prepareMask() {

		let img = this.originalMask;

		let canvas, ctx;
		[canvas, ctx] = this.initOriginalCanvas(img);
		
		this.matchMaskKeys(canvas, ctx);
		
	}

	private matchMaskKeys(canvas, ctx) {
		
		let array = this.analyzeMaskColors(canvas, ctx);

		for (var index in array) {

			var near = false;
			
			for (var index2 in this.colorKeys) {

				if (!near) {
					
					var maskHex = array[index];
					var refHex = this.colorKeys[index2];
					near = NodeUtils.verifyColor(maskHex, refHex, 5);
					if (near) this.activeColors[index2] = this.originalColors[index2] = array[index];

				}

			}

			if (!near) this.activeColors[index] = this.originalColors[index] = array[index];

		}

		//if(this.debug) console.log ("Original Colors is ", this.originalColors)
		
	}

	private async prepareStitch() {

		let img2 = this.originalStitchMask;
		let sCanvas, sCtx;
		[sCanvas, sCtx] = this.initOriginalCanvas(img2);
		this.originalStitches = this.analyzeMaskColors(sCanvas, sCtx);
		
	}

	private async prepareDefault() {

		var color =	this.colorKeys["default"];

		if (color != undefined) {
			this.activeColors["default"] = this.originalColors["default"] = color;
		} else {
			this.activeColors["default"] = this.originalColors["default"] = "FFFFFF"
		}
		
	}

    private initNodeMaterial() {

		const material = new NODES.StandardNodeMaterial();
		
		//@ts-ignore
    	material.metalness = new NODES.FloatNode( 0 ); // doesnt exist in v109
		//@ts-ignore
    	material.roughness = new NODES.FloatNode( 1 ); // doesnt exist in v109

		material.side = THREE.DoubleSide;
		material.needsUpdate = true;
		material.name = this.key;
		
		//if(this.debug) console.log('~~ STANDARD NODE MATERIAL for ' + this.key, material);

    	return material;

	}
	
	private analyzeMaskColors(canvas, ctx) {

	// separating masks into their different colors
    // ---------------------------------------------------------------


		const width = canvas.width;
		const height = canvas.height;
		const imageData = ctx.getImageData(0, 0, width, height).data;
				
		let arrayOfColors = NodeUtils.analyzeImgData(imageData);
		let array = [];

		for (let j = 0; j < arrayOfColors.length; ++j) {

			const maskColor = arrayOfColors[j];
			array[j] = maskColor;

		}

		// if(this.debug) console.log('~~ 3', this.originalColors);

		return array;

	}

	// called from INIT only
	public async applyCustomTextIfItExists(textParams, compileNodes : boolean = true){

		await this.prepareTextLayer(textParams);

		let material = await this.changeCustomText(textParams, compileNodes);		
		return new Promise(resolve => resolve(material));
	}

	/*
		text : string -> custom text from html input
		color : string/color-string -> color to use for custom text
		key : string (optional) - layerKey - probably not needed? because text wil be drawn on texture related to whole model part and not texture layer?
	*/

	// TODO - BUG??? - NOT EVEN CHECKING INCOMING COLOR PARAMETER HERE??
	public async changeCustomText(...args){

		let t = args[0] // TEXT PARAMETER
		let compileNodes = args[1] || true; // shouldnt compile when called from init

		if(this.debug) console.log('READY FOR CUSTOM TEXT', args);

		if (!this.textKeys.includes(t.key)) {
			console.log("!!!! MISSING TEXT LAYER IN PRODUCT CONFIG! \nReverting text to default color (#000000) and stitch (Jacquard)")
			this.textKeys.push(t.key);
		}
		this.useText = true;

	// Converting text data -> canvas -> alphaNodes & alphaStitchNodes

		let tCanvas = document.createElement('canvas');
		tCanvas.width = this.width;
		tCanvas.height = this.height;

		let tCtx = tCanvas.getContext("2d");
		// tCtx.clearRect(0,0, tCanvas.width, tCanvas.height); // dont need to clear fresh canvas?
		tCtx.scale(1, -1);

		var posX = (t.position.x / 100 *  tCanvas.width);
		var posY = -tCanvas.height + (t.position.y/100 * tCanvas.height);
		
		tCtx.font = t.font;
		tCtx.textAlign = "center";
		tCtx.textBaseline = "middle";
		tCtx.translate(posX, posY);
		tCtx.rotate(t.rotation / 180 * Math.PI);
		tCtx.fillText(t.value, 0, 0);

		tCtx.restore();

		let tTexture = new THREE.CanvasTexture(tCanvas);
		tTexture.flipY = true;
		tTexture.needsUpdate = true;
		// let tImageNode = new NODES.TextureNode(tTexture);

		this.alphaNodes[t.key] = await NodeUtils.colorToAlpha(tTexture, "000000")	
		this.alphaStitchNodes[t.key] = this.alphaNodes[t.key]
		
	// Adjusting stitch Masks and alphaNodes for colors
	// Will work even if there's no stitch used

		// Defaulting to config color if no colors were defined in layers
		if (!this.colorNodes[t.key]) {
			this.updateColorNode(t.color, t.key)
		}

		// Defaulting to config stitch if no stitch were defined in layers
		if (!this.stitchNodeMasks[t.key]) {
			let stitchCode = parseColor(t.stitch, 'clean');
			let stitchValue = this.getStitchValueBasedOfColorCode(stitchCode);
			let stitchNodes = this.initStitchTextureNodes(stitchValue);
			[this.stitchNodeMasks[t.key], this.stitchNodeNormals[t.key]] = stitchNodes;
		}

		let empty = new NODES.ColorNode(0x000000);
		var alpha = this.alphaNodes[t.key];
		var sAlpha = this.alphaStitchNodes[t.key];
		var sInput = this.stitchNodeMasks[t.key];

		if (this.useStitch) {
			this.alphaBlended[t.key] = NodeUtils.alphaBlendNodes(empty, sInput, alpha);
			
			console.log(this.alphaBlended[t.key])
			this.alphaStitchBlended[t.key] = NodeUtils.alphaBlendNodes(empty, sInput, sAlpha);
			console.log(this.alphaStitchBlended[t.key])
		} else {
			this.alphaBlended[t.key] = alpha;
			this.alphaStitchBlended[t.key] = alpha;
		}		
 
		if(compileNodes) await this.compileAllNodes();

	
		// TODO - modify material and return it back

		return this.material;
	}

    public async changeColors(color, key) {

    // Change individual color/all color from the mask
	// ---------------------------------------------------------------
	
		if(this.debug) console.log(this.key, key);

		if (key == undefined) key = "default";

		this.updateColorNode(color, key);
		
		await this.compileAllNodes();

		return this.material;

    }

    private updateColorNode(newColor, key) {

	// Update the color info on the color node, used in changeColors
	// ---------------------------------------------------------------

		let c = hexToRGB(newColor);
		 c.r /= 255;
		 c.g /= 255; 
		 c.b /= 255;
		let cv = new THREE.Color(c.r, c.g, c.b);

		this.activeColors[key] = newColor;

		this.colorNodes[key] = new NODES.ColorNode( cv );
    
	}
	
	private initOriginalCanvas(image) {
		
	// Canvas initialization for both mask / stitch original image
	// will be separated using shaders
	// ---------------------------------------------------------------
	

		let canvas = document.createElement('canvas');
		let ctx = canvas.getContext("2d");
		let imageMap = image.image;
		canvas.width = imageMap.width;
		canvas.height = imageMap.height;
		ctx.drawImage(imageMap, 0, 0);

		return [canvas, ctx]

	}

	
	private async initStitchNodes() {

		var empty = new NODES.ColorNode(0x000000); // v109 - requires a default texture to be passed
		this.stitchAlpha = new NODES.ColorNode(0x000000);
		this.createBaseStitch();

		for (var index2 in this.originalStitches) {
			await this.initStitchAlphas(index2);

			let stitchCode = this.originalStitches[index2];
			let stitchNodes;

			let stitchValue = this.getStitchValueBasedOfColorCode(stitchCode);
			stitchNodes = this.initStitchTextureNodes(stitchValue);

			[this.stitchNodeMasks[index2], this.stitchNodeNormals[index2]] = stitchNodes;

			var sAlpha = this.alphaStitchNodes[index2];
			var sInput = this.stitchNodeMasks[index2];

			this.alphaStitchBlended[index2] = NodeUtils.alphaBlendNodes(empty, sInput, sAlpha);
			this.stitchAlpha = NodeUtils.addNodes(this.stitchAlpha, this.alphaStitchBlended[index2]); 
		}

	}

	// OPTIMIZATION - could we have stitchCodes defined in configs?
	private getStitchValueBasedOfColorCode(colorCode){

		if (colorCode == "FF0000") {
			return 'WDE';
		} else if (colorCode == "0000FF") {
			return 'JCQ';
		} else if (colorCode == "00FF00") {
			return 'CLR';
		}

		console.warn('Unknown stitch code used!', colorCode);

		return 'JCQ'; // which one is default?
	}

	private initDefaultStitchNodes() {

		var empty = new NODES.ColorNode(0x000000); 
		this.stitchAlpha = new NODES.ColorNode(0x000000);
		this.createBaseStitch();

		let stitchCode = this.originalStitches["default"];
		let stitchNodes;

		let stitchValue = this.getStitchValueBasedOfColorCode(stitchCode);
		stitchNodes = this.initStitchTextureNodes(stitchValue);

		[this.stitchNodeMasks["default"], this.stitchNodeNormals["default"]] = stitchNodes;

		var sAlpha = new NODES.ColorNode(0xFFFFFF);
		var sInput = this.stitchNodeMasks["default"];

		this.alphaStitchBlended["default"] = NodeUtils.alphaBlendNodes(empty, sInput, sAlpha);
		this.stitchAlpha = NodeUtils.addNodes(this.stitchAlpha, this.alphaStitchBlended["default"]);

	}


	private async initMaskNodes() {

		var empty = new NODES.ColorNode(0x000000); 

    	for (var index in this.originalColors) {

			await this.initAlphas(index);
			
			[this.nodeMasks[index], this.nodeNormals[index]] = this.initStitchTextureNodes('JCQ'); // 

			var alpha = this.alphaNodes[index];
			var input = this.nodeMasks[index];

	        this.alphaBlended[index] = NodeUtils.alphaBlendNodes(empty, input, alpha); 

			let color = this.originalColors[index];
  			this.updateColorNode(color, index);  		

		}
	}

	private initDefaultNodes() {


		let color = this.originalColors["default"];

		this.updateColorNode(color, "default");

	}

    private async initAlphas(key) {

	// alpha mask stuff
    // ---------------------------------------------------------------    	

		let color = this.originalColors[key];

		// let imageNode = new NODES.TextureNode(this.originalMask);
		this.alphaNodes[key] = await NodeUtils.colorToAlpha(this.originalMask, color)

	}
	
	private async initStitchAlphas(key) {

	// alpha mask stuffs
    // ---------------------------------------------------------------    	

		let stitchColor = this.originalStitches[key];

		// let stitchImageNode = new NODES.TextureNode(this.originalStitchMask);
		this.alphaStitchNodes[key] = await NodeUtils.colorToAlpha(this.originalStitchMask, stitchColor)

	}

    private initAlphaNodes(alphaCanvas) {
	
	// initialize alphas as Nodes
    // ---------------------------------------------------------------
    	

	// 	var aTexture = new THREE.CanvasTexture(alphaCanvas);
	// 	var node = new NODES.TextureNode(aTexture);
	// //@ts-ignore
	// 	node.uv = new NODES.UVTransformNode();
	// //@ts-ignore
    //     node.uv.setUvTransform(0,0,1,-1,0);

	// 	return node;

	}
	
	// WHY - if its a texture/mask function iT SHOULD NOT CONTAIN STITCHES
	// IF NOT - rename a function to include 'stitch'
    private initStitchTextureNodes(stitchName) {
    
    // initialize mask for stitch nodes
	// ---------------------------------------------------------------
	
		if( ! this.stitchMasks.hasOwnProperty(stitchName)){
			console.warn('Forgot to load DEFAULT_STITCHES on one of parts in product configs??');
		}

    	const stitchTexture = this.stitchMasks[stitchName];
    	const stitchNormal = this.stitchNormals[stitchName];
    	const anisotropy = this.nodeSetting.anisotropy;
    	const pixelSize = this.nodeSetting.pixelSize;
    	var stitchRepeatX = stitchTexture.image.width / pixelSize;
    	var stitchRepeatY = stitchTexture.image.height / pixelSize;
        stitchTexture.wrapS = stitchTexture.wrapT = THREE.RepeatWrapping;
        stitchNormal.wrapS = stitchNormal.wrapT = THREE.RepeatWrapping;
        stitchTexture.anisotropy = stitchNormal.anisotropy = anisotropy;

        var rX = this.width/2/stitchRepeatX; 
        var rY = -this.height/2/stitchRepeatY;

		var nodeMask = new NODES.TextureNode(new THREE.Texture()); 
    	var nodeMask = new NODES.TextureNode(stitchTexture);
		//@ts-ignore
		nodeMask.uv = new NODES.UVTransformNode();
		//@ts-ignore
        nodeMask.uv.setUvTransform(0,0,rX,rY,0);

        var nodeNormal = new NODES.TextureNode(stitchNormal);
		//@ts-ignore
        nodeNormal.uv = new NODES.UVTransformNode();
		//@ts-ignore
		nodeNormal.uv.setUvTransform(0,0,rX,rY,0);
		
		return [nodeMask, nodeNormal]


	}

	private getDimensions() {

	// Get the width and height of the current mask
	// TODO add default parameter to repeat stitches, for e.g. in collar
	// ---------------------------------------------------------------
	
		if (this.originalMask) {
			this.width = this.originalMask.image.width;
			this.height = this.originalMask.image.height;
		} else if (this.originalStitchMask) {
			this.width = this.originalStitchMask.image.width;
			this.height = this.originalStitchMask.image.height;
		}

	}
	
    private async compileAllNodes() {
	
	// Combine all the nodes into one single material
    // ---------------------------------------------------------------
		var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

		if ((this.useStitch || this.useDefaultStitch) && !iOS) await this.compileStitchNodes();
		if (this.useMask) await this.compileMaskNodes();
		if (this.useDefault) await this.compileDefaultNodes();

		this.material.needsUpdate = true;


		
	}


	private async compileMaskNodes() {

		var empty = new NODES.ColorNode(0x000000);
        let colorMask = new NODES.ColorNode(0x000000);
		let baseColor = new NODES.ColorNode(0x000000); // WHY? was 0x000000
		// let stitchAlpha;

		// iterating through all found colors in color mask
		for (var key in this.alphaNodes) {

			let alpha = this.alphaNodes[key];

		// Adjusting alpha after text is applied
			if (this.useText) {

					if (!this.textKeys.includes(key)) {

						for (var index2 in this.textKeys) {

							let tKey2 = this.textKeys[index2]

							if (index2 == "0") {
								
								alpha = NodeUtils.subNodes(this.alphaNodes[key], this.alphaBlended[tKey2])

							} else {

								alpha = NodeUtils.subNodes(alpha, this.alphaBlended[tKey2])

							}

						}


					} else {

						// stitchAlpha = NodeUtils.subNodes(this.stitchAlpha, this.alphaBlended[tKey])
						// stitchAlpha = NodeUtils.addNodes(stitchAlpha, this.alphaBlended[tKey]);
						
						// for (var key3 in this.alphaNodes) {

						// 	alpha = NodeUtils.subNodes(this.alphaNodes[tKey], this.alphaBlended[key3])

						// }

					}					

			
				
			}

			let stitchBlend = NodeUtils.alphaBlendNodes(empty, this.alphaBlended[key], alpha )


			var blending;
			// check if using stitch

			this.useStitch? blending = stitchBlend : blending = alpha;
			
			var colorAdd = NodeUtils.alphaBlendNodes(empty, this.colorNodes[key], blending);
			//@ts-ignore
			colorMask = NodeUtils.addNodes(colorMask, colorAdd);

			//@ts-ignore
			// stitchAlpha = NodeUtils.addNodes(stitchAlpha, alpha);

		}

		// this.useText ? stitchAlpha = this.stitchAlpha : stitchAlpha = this.stitchAlpha;

		// check if using stitch
 		// blending2 is to check if things have base color
 		var blending2;
		this.useStitch? blending2 = this.stitchAlpha : blending2 = new NODES.ColorNode(0xFFFFFF);
		var color = NodeUtils.alphaBlendNodes(baseColor, colorMask, blending2);

		// this.material.color = this.alphaNodes["bottom-text"];
		// this.material.color = this.alphaBlended["top-text"];
		this.material.color = color;

	}

	private async compileStitchNodes() {
		
		var empty = new NODES.ColorNode(0x000000);
		let alphaMask = new NODES.ColorNode(0x000000);
		let stitchNormal = new NODES.ColorNode(0x000000);

		// iterating through all found stitch type in stitch mask
		for (var key2 in this.alphaStitchBlended) {

			let tAlpha = this.alphaStitchBlended[key2];


		// Adjusting alpha after text is applied
			if (this.useText) {

					if (!this.textKeys.includes(key2)) {

						for (var index2 in this.textKeys) {

							let tKey2 = this.textKeys[index2]

							if (index2 == "0") {
								
								tAlpha = NodeUtils.subNodes(this.alphaStitchBlended[key2], this.alphaStitchBlended[tKey2])

							} else {

								tAlpha = NodeUtils.subNodes(tAlpha, this.alphaStitchBlended[tKey2])

							}

						}
					}
				
				
			}

			// tAlpha = NodeUtils.subNodes(this.alphaStitchBlended[key2], this.alphaStitchBlended[tKey])

			let stitchAdd = NodeUtils.alphaBlendNodes(empty, this.stitchNodeNormals[key2], tAlpha);
			//@ts-ignore
			stitchNormal = NodeUtils.addNodes(stitchNormal, stitchAdd);
			//@ts-ignore
			alphaMask = NodeUtils.addNodes(alphaMask, tAlpha);
			
			
		}

		var adjustedNormal = NodeUtils.normalNodeAdjustment(this.baseStitchNode, stitchNormal, alphaMask);
		//@ts-ignore
		var normal = new NODES.NormalMapNode(adjustedNormal);
		this.material.normal = normal;

	}

	private async compileDefaultNodes() {

		this.material.color = this.colorNodes["default"];

	}


	// TO DO 
	private createBaseStitch() {

		const baseStitch = this.stitchNormals.JCQ;

    	const anisotropy = this.nodeSetting.anisotropy;
    	const pixelSize = this.nodeSetting.pixelSize;

    	var stitchRepeatX = baseStitch.image.width / pixelSize;
    	var stitchRepeatY = baseStitch.image.height / pixelSize;

        baseStitch.wrapS = baseStitch.wrapT = THREE.RepeatWrapping;
        baseStitch.anisotropy = anisotropy;

        var rX = this.width/2/stitchRepeatX;
        var rY = -this.height/2/stitchRepeatY;

		this.baseStitchNode = new NODES.TextureNode(baseStitch);
		this.baseStitchNode.uv = new NODES.UVTransformNode();
		this.baseStitchNode.uv.setUvTransform(0,0,rX,rY,0);
		
	}


// ---------------------------------------------------------------
// TEXTURE/SHADER UTILS
// ---------------------------------------------------------------


// optimize, make accessible thru configs
    private getStitchTextures() {

        const stitchKeys = [ 
            'JCQ_normal',
			'WDE_normal',
			'CLR_normal',
            'JCQ_mask',
			'WDE_mask',
			'CLR_mask'
		];
		
        for(let i=0;i<stitchKeys.length;i++){
            
            let key = stitchKeys[i];

            if(key.includes('_normal')){ // normal

                let cleankey = key.replace('_normal','');
                this.stitchNormals[cleankey] = this.loadedTextures[key];

            }else if(key.includes('_mask')){ // masks

                let cleankey = key.replace('_mask','');
                this.stitchMasks[cleankey] = this.loadedTextures[key]; // still need to do scaling, as in this.initNodeMasks

			}
			
		}
		
    }

}



