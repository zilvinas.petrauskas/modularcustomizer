import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three';

import { NodeUtils } from './NodeUtils';
import { NodeControl } from './NodeControl';
import { hexToRGB } from 'helpers/helpers';


export class NodeMaskControl extends NodeControl{ 

    // This module is used to control the mask nodes of a single mask in NodeMaterial

    protected baseNode;
    protected stitchMask;
    protected colorNodes : any = {};

    constructor(...args){ // unlimited arguments passed thru events

        super(...args);

    }

    public async init() {
        
        this.config.useMask? await this.initWithMask() : this.initDefault();

    }

    protected async addStitchMask(maskNode) {

        this.stitchMask = maskNode;

        for (var key in this.alphaNodes) {

            let input = this.alphaNodes[key];

            this.alphaBlended[key] = NodeUtils.alphaBlendNodes(this.empty, input, maskNode)

        }

    }

    protected initDefault() {

        if (this.colorKeys["default"] != null && this.colorKeys["default"] !== null) {
            this.referenceColors["default"] = this.colorKeys["default"]
        } else {
            this.referenceColors["default"] = "FFFFFF"
        }

    }

    public async update(color, key) {

        this.colorNodes[key] = await this.updateColorNode(color)

    }

    public async updateColorNode(newValue) {

        let c = hexToRGB(newValue);
		c.r /= 255;
		c.g /= 255; 
		c.b /= 255;
	   let cv = new THREE.Color(c.r, c.g, c.b);

	   return new NODES.ColorNode(cv);

    }

    protected async compile(text?) {

        let color;

        if (!this.config.useMask) {
            color = this.colorNodes["default"];
            return color;
        }

        var empty = this.empty;
        let colorMask = this.empty;

		// iterating through all found colors in color mask
		for (var key in this.alphaNodes) {

			let alpha = this.alphaNodes[key];
			let stitchBlend = NodeUtils.alphaBlendNodes(empty, this.alphaBlended[key], alpha )
            
			var blending;
			// check if using stitch
            this.config.useStitch? blending = stitchBlend : blending = alpha;
            
			var colorAdd = NodeUtils.alphaBlendNodes(empty, this.colorNodes[key], blending);
			//@ts-ignore
			colorMask = NodeUtils.addNodes(colorMask, colorAdd);

		}
        
		// check if using stitch
        // blending2 is to check if things have base color
        
        let baseColor = this.colorNodes["base-color"];
		if(baseColor = undefined || baseColor == null){
			baseColor = this.empty;
        }        

        var blending2;
		this.config.useStitch? blending2 = this.stitchMask : blending2 = new NODES.ColorNode(0xFFFFFF);

        this.config.useStitch? color = NodeUtils.alphaBlendNodes(baseColor, colorMask, blending2) : color = colorMask;

        return color;

    }



}