import { Root } from 'Root';
import { EVENT_PRELOADER_LIST, TYPE_TEXTURE } from 'helpers/constants';
import * as THREE from 'three';

export class OldNormalMaterial{
    
    constructor(){

        Root.instance.preloader.add('old', 'oldNormal3.png', TYPE_TEXTURE, true);
        Root.instance.preloader.add('old-stitch', 'stitch_jacquard.png', TYPE_TEXTURE, true);

        Root.instance.events.register('old', this.create.bind(this));

    }

    public async create(...args){

        console.log('!! args?', args);
        const partName = args[0] + '-texture';

        const loaded = await Root.instance.events.call(EVENT_PRELOADER_LIST, null, true) as any;

        const texture = loaded[partName].clone();
        // texture.flipX = true; 
        // texture.wrapS = THREE.RepeatWrapping;
        // texture.repeat.y = -1;
        texture.needsUpdate = true;

        const normalImage = loaded['old'].image;

        const stitchNormal = loaded['old-stitch'];
        const stitchNormalImage = stitchNormal.image;

        const normalCanvas = document.createElement('canvas');
        normalCanvas.width = normalImage.width;
        normalCanvas.height = normalImage.height;
        // normalCanvas.setAttribute('id', 'TEMP_CANVAS');
        // normalCanvas.style.zIndex = "9999";
        // normalCanvas.style.position = "absolute";
        // normalCanvas.style.display = "none";
        // normalCanvas.style.top = "0";
        
        document.body.appendChild(normalCanvas);

        const normalCtx = normalCanvas.getContext('2d');
        normalCtx.drawImage(normalImage, 0, 0);
        
        let stitchNormalPattern = normalCtx.createPattern(stitchNormalImage, "repeat");
        normalCtx.fillStyle = stitchNormalPattern;
        normalCtx.fill();

        var stitchNormalTexture = new THREE.CanvasTexture(normalCanvas);
        stitchNormalTexture.flipY = true;

        const material2 = new THREE.MeshStandardMaterial({
            map: texture,
            normalMap: stitchNormalTexture,
            side: THREE.DoubleSide,
            roughness: 1,
            metalness: 0,
            color: new THREE.Color('#cecece'),
            normalScale: new THREE.Vector2(1, 1),
        });

        // material2.roughness = 1;
        // material2.metalness = 0;
        // material2.normalScale = new THREE.Vector2(1, 1);

        return material2;
    }

    

}