import * as NODES from 'three/examples/jsm/nodes/Nodes';
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_PRELOADER_LIST } from 'helpers/constants';

export class SwatchTextureNodes {
    
    private width;
    private height;

    private swatchSettings;

    private swatchTypes = [
        'JCQ',
        'WDE',
        'CLR'
    ];

    private swatches: any = {};
    

    constructor(...args){ // unlimited arguments passed thru events

        this.width = args[0]
        this.height = args[1]
        this.swatchSettings = args[2];

        if(this.swatchSettings = undefined || this.swatchSettings == null){
            this.swatchSettings = {
                anisotropy : 2 as number,
                pixelSize: 16 as number,
            }
		}

    }

    public async get(key) {
        return this.swatches[key];
    }

    public async getAll() {
        return this.swatches;
    }

    public async init() {
        
        let textures = await Root.instance.events.call(EVENT_PRELOADER_LIST, null, null) 
        
        this.swatchTypes.forEach(key => {

            let maskKey = key.concat("_mask");
            let mask = textures[maskKey];

            let normalKey = key.concat("_normal");
            let normal = textures[normalKey];

            this.swatches[key] = {};
            this.swatches[key]['mask'] = this.createNodes(mask);
            this.swatches[key]['normal'] = this.createNodes(normal);
            

        });

    }

    private createNodes(texture) {

        const a = this.swatchSettings.anisotropy; // anisotropy  from Config
        const p = this.swatchSettings.pixelSize; // pixelSize from Config
        
        var scaleX = texture.image.width / p;
        var scaleY = texture.image.height / p;
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.anisotropy = texture.anisotropy = a;

        var rX = this.width/2/scaleX; 
        var rY = -this.height/2/scaleY;

        var node = new NODES.TextureNode(texture);
        //@ts-ignore
        node.uv = new NODES.UVTransformNode();
        //@ts-ignore
        node.uv.setUvTransform(0,0,rX,rY,0);
        
        return node;

    }

}