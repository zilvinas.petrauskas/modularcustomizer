import { NodeMaterial } from './NodeMaterial';
import { NodeMaterialv2 } from './NodeMaterialv2';

import { Root } from 'Root';
import { EVENT_GET_NEAREST_COLOR } from 'helpers/constants';
import { parseColor } from 'helpers/helpers';

// *** TEMP IMAGE UPLOAD
export class NodeMaterialExtension extends NodeMaterial{

    protected customTextData : any = {};

    constructor(...args){
        super(...args);
    }

    public async getColorData(){
        let activeColors = await this.fixActiveColors( this.activeColors );
        return new Promise(resolve => resolve(activeColors));
    }

    protected async fixActiveColors(activeColors){

        let fixed = {};

        for(let key in activeColors){

            let color = activeColors[key];
            let corrected = await Root.instance.events.call(EVENT_GET_NEAREST_COLOR, null, true, color);
            
            if(typeof corrected !== 'undefined' && corrected !== null && corrected !== color){
                color = corrected;
            }

            fixed[key] = parseColor(color, 'clean');
        }

        return fixed;
    }

    public saveCustomTextValues(key, value){
        this.customTextData[key] = value;
    }

    public async getCustomTextData(){
        let textData = this.customTextData;
        return new Promise(resolve => resolve(textData));
    }
}