
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_CUSTOMIZER_SCENE_READY } from 'helpers/constants';

export class DefaultLights{

    constructor(){

        Root.instance.events.listen(EVENT_CUSTOMIZER_SCENE_READY, this.init.bind(this));
    }

    protected init(){

        this.lightOne();
        this.lightTwo();
        this.lightThree();

    }

    protected lightOne(){

        var light1 = new THREE.PointLight( 0xdfebff, 1 );
        light1.position.set(-250, 500, 300);
        light1.position.multiplyScalar( 1 );
        light1.castShadow = true;
        light1.shadow.bias = 0.00002;
        light1.shadow.radius = 25;
        light1.shadow.mapSize.width = 1024;
        light1.shadow.mapSize.height = 1024;
        light1.shadow.camera.far = 10000;

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, light1);

    }

    protected lightTwo(){

        const light2 = new THREE.PointLight( 0xdfebff, 1.5 );
        light2.position.set(-200, 1000, -1000);
        light2.position.multiplyScalar( 1 );
        light2.castShadow = false;
        light2.shadow.bias = 0.0002;
        light2.shadow.radius = 25;
        light2.shadow.mapSize.width = 1024;
        light2.shadow.mapSize.height = 1024;
        light2.shadow.camera.far = 100;

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, light2);
    }

    protected lightThree(){

        var light3 = new THREE.HemisphereLight(0xffffff, 0x000000, 0.3);
        light3.position.set(0, -0.1, 3);

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, light3);
    }

}
