import { Root } from 'Root';
import { EVENT_CUSTOMIZER_READY, EVENT_DYNAMIC_PROPERTY_CREATION, EVENT_DYNAMIC_PROPERTY_UPDATE, EVENT_CHECK_COLOR_DATA, EVENT_GET_COLOR_FEEDER_INFO, EVENT_GET_UI_GROUP_NAME, EVENT_COLOR_CHANGE_COMPLETE, EVENT_TEXT_CHANGE_COMPLETE, EVENT_GET_PRODUCT_IS_CUSTOMIZED, EVENT_DYNAMIC_PROPERTY_UPDATED, EVENT_GET_CUSTOM_TEXT_DATA, EVENT_GET_CUSTOM_TEXT_PARAMS, EVENT_GET_MODEL_PARTS } from 'helpers/constants';
import { qs, fullFeederString } from 'helpers/helpers';
import { DPF } from './DynamicPropertiesFinder';
 
// FOR SHOPIFY ONLY
export class DynamicPropertiesInjector{
 
    private data: any = {};
    private parentElement : HTMLElement;
 
    constructor(){
   
        this.init();
        this.listen();
        
    }
 

    // add dynamic properties param in UI config, like dynamicPropertiesContainer
    // get UI config
    // get html element
    protected async init(){
 
       
        let uiConfig = Root.instance.config.ui.get();

        if(uiConfig.hasOwnProperty('dynamicPropertiesContainer')){
            this.parentElement = qs(uiConfig.dynamicPropertiesContainer)[0]; 
        }else{
            this.parentElement = qs('#dynamicProductProperties')[0];
        }

        // defaults get created on EVENT_CUSTOMIZER_READY
    }

    // All the events
    protected listen(){
       
        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.createDefaults.bind(this)); // when customizer is ready it should create hidden inputs
        Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.updateOnColorChange.bind(this));
        Root.instance.events.listen(EVENT_TEXT_CHANGE_COMPLETE, this.updateOnTextChange.bind(this));


        Root.instance.events.register(EVENT_DYNAMIC_PROPERTY_CREATION, this.create.bind(this));
        Root.instance.events.register(EVENT_DYNAMIC_PROPERTY_UPDATE, this.update.bind(this)); 
        Root.instance.events.register(EVENT_GET_PRODUCT_IS_CUSTOMIZED, this.getProductIsCustomized.bind(this));

    }
 
    /*
        there should be some default inputs created - like product handle and other stuff that can be received from product config instantly
        otherwise we will use events within other modules to create these dynamic inputs
    */
    protected async createDefaults(){
 
        this.create('handle', {
            hidden: true,
            value: DPF.productHandle(),
            name: 'Handle',
        })

        this.create('originalHandle', {
            hidden: true,
            value: await DPF.getOriginalHandle(),
            name: 'originalHandle',
        })

        // checking createScreenshotInput from old class
        // TODO - call update when colors change
        this.create('colors', {
            hidden: false,
            value: await DPF.productColors(), // TODO parse within and return string value
            name: 'Colors',
        })
        
        this.create('not-customized', {
            hidden: true,
            value: 'yes',
            name: 'Non customized product',
        })

        this.create('product', {
            hidden: true,
            value: '',
            name: 'customProduct',
        })

        // TODO - call after add to cart?
        this.create('variant', {
            hidden: true,
            value: '',
            name: 'customVariant',
        })

        this.create('url', {
            hidden: true,
            value: await DPF.url(),
            name: 'originalUrl',
        })

        this.create('screenshot', {
            hidden: true,
            value: '', //await DPF.screenshot(),
            name: 'screenshot',
        });
         
        this.createInputsForProductPartsAndLayers();
        this.createInputsForCustomText();
 
    }

    protected async updateOnTextChange(){

        const customText = await Root.instance.events.call(EVENT_GET_CUSTOM_TEXT_DATA, null, true) as any;
        let allTexts = [];

        for(let key in customText){
            this.update(key, {
                value: customText[key],
            });
            allTexts.push( customText[key] );
        }


        let allTextString = allTexts.join(', ');
        this.update('Text', {
            value: allTextString,
        });

    }

    // TODO - optimize, resuable parts to fn
    protected async updateOnColorChange(){

        // TODO - run thru ui gropus and update its data
        // TODO - update customized product
        // TODO - finally update all colors

        const colorData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;
        const allColors = [];
        let changesMade = 0;

        for(let groupKey in colorData){

            let colorCode = colorData[groupKey].color;
            let colorInfo = await Root.instance.events.call(EVENT_GET_COLOR_FEEDER_INFO, null, null, colorCode) as any;

            if( ! this.data[groupKey].params.value.includes(colorCode) ){

                let colorFeederNameString = '';

                if(colorInfo){
                    let feederString = fullFeederString(colorInfo.feeder);
                    colorFeederNameString = `${colorCode}|${colorInfo.name}|${feederString}`;
                }

                this.update(groupKey, {
                    value: colorFeederNameString,
                });

                changesMade++;
            }

            if( ! allColors.includes(colorInfo.name) ){
                allColors.push(colorInfo.name);
            }

        }

        this.update('colors', { value: allColors.join(', ') })

        if(changesMade > 0){
            this.update('not-customized', { value: 'no'});
        }

    }
    

    private update(...args){
 
        const key = args[0];
        const params = args[1];

        if(typeof params === 'string' || typeof params === 'number'){
            console.warn('!! Dynamic update 2nd argument should be object!', params);
        }
         
        if( ! this.data.hasOwnProperty(key) ){
            console.warn('!! Dynamic property update failed, coz key not found', key, this.data[key], this.data);
            return;
        }
 
        if(params.hasOwnProperty('value')){
            this.data[key].params.value = params.value;
            this.data[key].element.setAttribute('value', params.value);
            Root.instance.events.broadcast(EVENT_DYNAMIC_PROPERTY_UPDATED, key);
        }else{
            console.log('FAILED TO UPDATE', key, params, args);
        }
   
 
    }

    public async createInputsForCustomText(){

        let allTexts = [];

        const parts = await Root.instance.events.call(EVENT_GET_MODEL_PARTS, null, true) as any;

        for(let i=0;i<parts.length;i++){

            let componentKey = parts[i].component.getKey();
            let customText = await Root.instance.events.call(EVENT_GET_CUSTOM_TEXT_PARAMS, null, true, componentKey) as any;

            if(typeof customText !== 'undefined' && customText !== null && Object.keys(customText).length > 0){

                for(let i=0;i<customText.length;i++){

                    let row = customText[i];
    
                    this.create(row.key, {
                        hidden: true,
                        name: row.key,
                        value: row.value,
                    });
        
                    allTexts.push( row.value );
                }


            }
        }

        if(allTexts.length > 0){
            let allTextString = allTexts.join(', ');
            this.create('Text', {
                hidden: false,
                name: 'Text',
                value: allTextString,
            });
        }
        
    }


    // TODO - send parts and set value to: "colorCode|colorName|Feeder #"
    // TODO - move to DPF
    public async createInputsForProductPartsAndLayers(){
        
        const colorData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any; 

        console.log('!!! colorData', colorData);

        const params = {
            hidden: true,
        }

        for(let groupKey in colorData){
 
            let colorCode = colorData[groupKey].color;
            let colorInfo = await Root.instance.events.call(EVENT_GET_COLOR_FEEDER_INFO, null, null, colorCode) as any;

            let colorFeederNameString = '';

            if(colorInfo){
                let feederString = fullFeederString(colorInfo.feeder);
                colorFeederNameString = `${colorCode}|${colorInfo.name}|${feederString}`;
            }

            let groupName = await Root.instance.events.call(EVENT_GET_UI_GROUP_NAME, null, true, groupKey);

            this.create(groupKey, {
                hidden: params.hidden,
                name: groupName, 
                value: colorFeederNameString,
            })

        }



    }

    // create input with paramaters
    // params should contain things like input name, value
    private create(...args){
 
        const key = args[0];
        const params = args[1]; // object

        if( ! params.hasOwnProperty('name') ){
            console.warn('Dynamic property MISSING NAME', key, params.name, params);
            return;
        }
 
        // .. do stuff
        const inputName = this.generateInputId(params.name, params.hidden);
        const input = this.createInputElement(inputName, params);

        this.append(input); // add to html

        if(this.data.hasOwnProperty(key)){
            console.warn('Dynamic property data already has this key: ', key);
            return;
        }
       
        // same element + params for later usage
        this.data[key] = {
            element: input, // use this for update fn to change input values and skip a need to search for inputs in html
            params: params,
        }
 
       
    }
 
    protected getProductIsCustomized(){
        return this.data['not-customized'];
    }

    private append(element){
       this.parentElement.appendChild(element);
    }
 
 
    /*
        name : string
        isVisibleInCheckout : boolean - whenever some informatino needs to be visible in checkout, make sure its id is created with 'true' here.
                                        things like Colors, Custom text, Gender will be visible in checkout
    */
    private generateInputId(name, isHidden: boolean = true){
        let string = name;
        if( isHidden) string = '_' + string;
        return string;
    }

    private createInputElement(name, params) {

        var input = document.createElement("input");
        input.setAttribute('name', this.propertyName(name));
        input.setAttribute('type','hidden');
      
        if(params.value){
            input.setAttribute('value', params.value);
        }else{
            input.setAttribute('value', '');
        }
       
        return input;
        
    }

    private propertyName(name){
        return "properties["+name+"]"; // shopify standard
    }
 
}
