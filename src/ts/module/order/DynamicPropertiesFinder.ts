import { Root } from 'Root';
import { EVENT_TAKE_SCREENSHOT, EVENT_GET_MODEL_PARTS, EVENT_GET_COLOR_DATA, EVENT_CHECK_COLOR_DATA, EVENT_GET_COLOR_FEEDER_INFO } from 'helpers/constants';
import { getColorFeederData, capitalizeString, getOriginalHandle } from 'helpers/helpers';

export class DPF{ // helper class for dynamic properties injector - finds whatever values we need and keeps things clean

    public static productHandle(){

        return Root.instance.config.product.get('handle');
    }


    // calamigos sweaters might have something like W0001_CC001 as a handle, but we use only CC0001 part in the customizer
    public static async getOriginalHandle(){

        let parsedHandle = Root.instance.config.product.get('handle');
        let originalHandle = getOriginalHandle();

        if(typeof originalHandle !== 'undefined' && originalHandle !== null){
            return originalHandle;
        }

        return parsedHandle;
    }

    public static async screenshot(){

        let screenshot = await Root.instance.events.call(EVENT_TAKE_SCREENSHOT, null, true);

        return screenshot;
    }

    public static async productParts(){

        let parts = await Root.instance.events.call(EVENT_GET_MODEL_PARTS, null, true);

        return parts;
    }

    public static async productColors(){

        const colorData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any; 
        
        if(typeof colorData === 'undefined' || colorData === null){
            console.warn('Failed to get productColors for Dynamic Properties', colorData);
            return '';
        }

        let uniqueColorNames = [];

        for(let groupKey in colorData){

            let colorCode = colorData[groupKey].color;
            let colorInfo = await Root.instance.events.call(EVENT_GET_COLOR_FEEDER_INFO, null, null, colorCode) as any;

            if(colorInfo){
                if(uniqueColorNames.includes(colorInfo.name)) continue;
                uniqueColorNames.push( capitalizeString(colorInfo.name) );
            }

        }

        let colorsString = uniqueColorNames.join(', ');

        return colorsString;
    }

    public static async url(){
        return window.location.href;
    }
}