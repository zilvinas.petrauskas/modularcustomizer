import { Root } from 'Root';
import { EVENT_COLORS_DATA_FOR_EXPORT, EVENT_SEND_PRODUCT_INFO, EVENT_COLLECT_PRODUCT_INFO, EVENT_DYNAMIC_PROPERTY_UPDATE, EVENT_OUTRO_START, EVENT_OUTRO_ADD, EVENT_DYNAMIC_PROPERTY_UPDATED, EVENT_CUSTOMIZER_READY } from 'helpers/constants';
import { getBaseUrl, isLocalhost } from 'helpers/helpers';
import { AJAX } from 'helpers/ajax';

export class OrderManager{
    
    private busy: boolean = false;

    constructor(){

        // disable add to cart buttons while customizer is loading
        this.toggleAddToCartButtons(true); 

        // when customizer is ready - enable add to cart buttons
        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.enableAddToCartButtons.bind(this));


        Root.instance.events.register(EVENT_SEND_PRODUCT_INFO, this.send.bind(this));

    }

    protected async send(){

        if(this.busy) return;

        Root.instance.events.call(EVENT_OUTRO_START, null, null);

        this.busy = true;
        
        let params = await Root.instance.events.call(EVENT_COLLECT_PRODUCT_INFO, null, true);

        let url = await this.getExternalServerUrl('customizer.php') as string;


        try{

            AJAX.load(
                {
                url: url,
                params: params,
                method: 'POST',
                xhrFields: {
                    withCredentials: true
                },
                onComplete: (jsonResponse) =>{
                    const resp = JSON.parse(jsonResponse);
                    console.log('order saved?', resp);
                
                    this.sendSuccess(resp, params);
                }
            });
            
        }catch(e){
            
            // will fail on localhost  until I'll add customizer.php
            console.warn('Error!', e);
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null, 'cart');
            setTimeout(()=>{
                Root.instance.events.call(EVENT_OUTRO_ADD, null, null, 'image');
            }, 1000);

        }


    }

    protected async updateTempProduct(params){

        let url = await this.getExternalServerUrl('shopify.php') as string;

		if(isLocalhost()){
			console.log('not updating product image on localhost');
			return;
		}

		params.post = 'update-image';

		// TODO - queue to shopify to update product image (async request just to make things work faster)
		AJAX.load({
			url: url,
			params: params,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			onComplete: (jsonResponse) =>{

                // TODO - update outro progress
				const resp = JSON.parse(jsonResponse);
                console.log('Image finished updating', resp);		
                
                Root.instance.events.call(EVENT_OUTRO_ADD, null, null,  'image');
			}
		});


    }

    private sendSuccessOnLocalhost(){

        const localDelay = 1500;

        setTimeout(()=>{
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null, 'cart');
        }, 1000);

        setTimeout(()=>{
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null,  'image');
        }, 2000);

        setTimeout(()=>{
            this.toggleAddToCartButtons(false);
        }, localDelay+700);
                    
    }

    protected sendSuccess(resp, params){



        if(isLocalhost()){

            return this.sendSuccessOnLocalhost();
        }


        Root.instance.events.call(EVENT_OUTRO_ADD, null, null, 'cart');

        // TODO - speed up outro

        // replace original product image - use screenshot_public
        // saving at local storage isnt needed anymore

        // replace product image in current cart
        // const customItemImageKey = `customImage_${params.product.id}_${params.product.variant}`;
        // this.waitToReplaceImage(customItemImageKey, resp.screenshot_public); // TEST IF THIS IS NEEDED

        // save screenshot information into custom properties (use screenshot)
        Root.instance.events.call(EVENT_DYNAMIC_PROPERTY_UPDATE, null, null, 'screenshot', {
            value: `${resp.screenshot_public}|${params.screenshot.width}|${params.screenshot.height}`,
        })
        
        // save folder with access to all external data
        Root.instance.events.call(EVENT_DYNAMIC_PROPERTY_UPDATE, null, null, 'product', {value: resp.customProductNumber});

        // trigger shopify add to cart

        this.getReadyToClickActualAddToCart();

        if(resp.temp){ // update with temp product data

            Root.instance.events.call(EVENT_DYNAMIC_PROPERTY_UPDATE, null, null, 'variant', {value: resp.temp.variant_id});

            this.updateTempProduct(resp.temp);
        
        }else{
            Root.instance.events.call(EVENT_OUTRO_ADD, null, null,  'image'); // finish outro without temp product
        }

        this.enableButtonsAfterDelay(5000);
     
    }

    private enableButtonsAfterDelay(delay){
   // reset add to cart buttons
        setTimeout(()=>{

            this.toggleAddToCartButtons(false);
            this.busy = false;

        }, delay || 2000); // protect from double clicks and spam with this delay
    }

    private getReadyToClickActualAddToCart(){

        Root.instance.events.listen(EVENT_DYNAMIC_PROPERTY_UPDATED, (key)=>{

            console.log('- updated', key);

            if(key == 'variant'){

                setTimeout(()=>{
                    // click shopify add to cart button
                    const btnCart = document.querySelectorAll('.cart-functions button[type="submit"]')[0] as HTMLElement;
                    btnCart.click();

                }, 100);
                

            }

        });

    }

    private waitToReplaceImage(_id, _src){

        let maxWait = 5000;
        let currentWait = 0;
        
        function wait(){
            let elem = document.getElementById(_id);
            if(elem){
                elem.setAttribute('src',_src);
            }else{
                let t = 50;
                currentWait += t;
                if(currentWait < maxWait){
                    setTimeout(()=>{
                        wait();
                    }, t);
                }else{
                    console.log('FAILED to replace img',_id);
                }
            }
        }

        wait();


    }

    private enableAddToCartButtons(){
        this.toggleAddToCartButtons(false);
    }

    // TODO
    private toggleAddToCartButtons(disabled: boolean = false){

		let buttons = document.querySelectorAll('.customizer-add-cart'); // TODO - check ui config value for key 'addToCart'
		if(buttons && buttons.length > 0){
			for(let i=0;i<buttons.length;i++){
				if(disabled === true){
					buttons[i].setAttribute('disabled','disabled');
					buttons[i].classList.add('busy');
				}else{
					buttons[i].removeAttribute('disabled');
					buttons[i].classList.remove('busy');
				}
				
			}
		}
	}

    private async getExternalServerUrl(file?){

		let url = '';

		if (isLocalhost()){

			// trigger local script on localhost
			url = getBaseUrl() + './api/' + file;

		}else{

            let websiteConfig = Root.instance.config.website.get();
            
            if(websiteConfig.hasOwnProperty('api')){

                let apiList = websiteConfig.api;

                for(let i=0;i<apiList.length;i++){
                    if(apiList[i].hostname == window.location.hostname){
                        url = apiList[i].api + file;
                        break;
                    }
                }

            }

            if(url == ''){
                console.warn('API URL NOT FOUND!', url, websiteConfig.api, window.location.hostname);
            }
		}

		return new Promise(resolve => resolve(url));
    }
    

}