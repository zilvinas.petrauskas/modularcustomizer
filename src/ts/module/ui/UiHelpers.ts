import { isDefined } from 'helpers/helpers';

    /*
    rules - ui.group.rules
*/
export function defaultTextParams(key, group){

    return {
        key : key, // key relation between part->layer key and UI key
        name: isDefined(group.name) ? group.name : key,
        value : isDefined(group.value) ? group.value : '', // text value
        position: isDefined(group.rules.position) ? group.rules.position : {x:0, y: 0},
        font : isDefined(group.rules.font) ? group.rules.font : '100px Arial',
        color: isDefined(group.rules.color) ? group.rules.color : '000000',
        stitch: isDefined(group.rules.stitch) ? group.rules.stitch : '0000FF',
        rotation: isDefined(group.rules.rotation) ? group.rules.rotation: 0,
    };
}

/*
    colorData - active color data on parts/layers received from event
    setActiveColor - function to be callde for setting active color
    disableColorsFn - function to be called for disabling colors

    return
    {
        active: active, // {key -> color:string, ... }
        disable: disable, // object{ array[groupKey:string, color:string] }
    };
*/
export function getAppliedRulesData(colorData){

        const active = {};
        const disable = {};
        const different = {};
        const groups = [];
        
        // prepare stuff
        for(let groupKey in colorData){
            
            // have a groups array
            groups.push(groupKey);

            // prep disablers
            disable[groupKey] = [];

            // prep diff
            different[groupKey] = [];

            // gather relations between disabled elements
            let row = colorData[groupKey];
            
            if(row.hasOwnProperty('different')){
                for(let d in row.different){
                    if( ! different.hasOwnProperty(d)) different[d] = [];
                    if( ! different[d].includes( row.different[d] )){
                        different[d].push( row.different[d] );
                    }
                }
            }
        }

        for(let groupKey in colorData){

            let row = colorData[groupKey];

            // if row is unique or is different form other groups
            if(row.isUnique || different[groupKey].length > 0){
                for(let g in groups){
                    let key = groups[g];
                    if(key == groupKey) continue;
                    let disableThisKey = false;
                    if(row.isUnique){ // disable row.color in all other groups
                        if( colorData[key].color != row.color ) disableThisKey = true; // unless about to be disabled row has THAT color as active
                    }else if(different[groupKey].includes(key)){ // different
                        disableThisKey = true;
                    }
                    if(disableThisKey){
                        disable[groupKey].push({
                            groupKey: key,
                            color: row.color,
                        });
                    }
                }
            }

            active[groupKey] = row.color;
        }

        return {
            active: active, // key -> color
            disable: disable, // object{ array[groupKey:string, color:string] }
        };
}