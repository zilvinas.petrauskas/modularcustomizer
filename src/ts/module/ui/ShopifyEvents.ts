import { Root } from 'Root';
import { EVENT_PRELOADER_FINISHED, EVENT_SHOW_CUSTOMIZER, EVENT_PRELOADER_START } from 'helpers/constants';

export class ShopifyEvents{

    private websiteConfig: any;
    private defaultButtonQuery :string = '#variant-customizer-show';

    constructor(){

        this.prepare();

        Root.instance.events.listen(EVENT_PRELOADER_START, this.init.bind(this)); // can be shown once preloader has started

    }

    protected prepare(){

        this.websiteConfig = Root.instance.config.website.get();
    }

    protected async init(){

        const buttonQuery = this.websiteConfig.customizeProductButton || this.defaultButtonQuery;
        const button = document.querySelectorAll(buttonQuery)[0];

        if( ! button ){
            console.warn('Missing button for showing customizer - assign it via configuration or use default id of '+this.defaultButtonQuery)
            return;
        }

        button.addEventListener('click', ()=>{

            console.log('START CUSTOMIZER');

            // const oldEvent = new Event( "customizer:start");

            var event = new CustomEvent("customizer:start", {
                detail: {
                  silent: false
                }
              });

            window.dispatchEvent(event);

        });

    }

}