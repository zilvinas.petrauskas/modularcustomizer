import { Root } from 'Root';
import { getColorsListIdName, qs, parseColor, isDefined } from 'helpers/helpers';
import { EVENT_UI_COLOR_CHANGED, EVENT_COLOR_CHANGE, EVENT_PROGRESS_ADD, EVENT_CUSTOMIZER_READY, EVENT_TEXTURES_APPLIED, EVENT_COLOR_CHANGE_COMPLETE, EVENT_CHECK_COLOR_DATA, EVENT_CHECK_COLOR_ALLOWED, EVENT_GET_COLOR_DATA, EVENT_GET_UI_GROUP_NAME, EVENT_COLORS_DATA_FOR_EXPORT, EVENT_GET_COLOR_FEEDER_INFO, EVENT_UI_ADD_TO_CART, EVENT_SEND_PRODUCT_INFO, EVENT_UI_INPUT_CHANGED, EVENT_CUSTOM_TEXT_CHANGE, EVENT_CUSTOMIZER_VISIBLE } from 'helpers/constants';
import { ColorRules } from 'component/colors/ColorRules';
import { DefaultUI } from './DefaultUi';
import { defaultTextParams, getAppliedRulesData } from './UiHelpers';

export interface UiGroupInterface{
	key: string,
	name: string,
	type: string,
	value: string[],
	unique?: boolean,
	different?: string[],
}

// calamigos & default UI
export class UI extends DefaultUI {

	protected groups;
	protected rules: ColorRules | any;

	constructor(){
		super();

		this.prepare();
		this.listen();
		this.init();
		
	}

	protected prepare(){

	}

	/* 
		NOTES:
		- parent div should come from configuration (or use default if nothing provided)
		- ui doesnt include progress bar HTML - might only trigger events related to?
		- [TODO] color/color-group/input as separate components? Probably.
		- [TODO] input text comes from configuration (or default text used)
		- 
	*/

	//x 
	protected async init(){

		this.applyUiVersionClass();
		await this.getConfigs();
		await this.getContainers();
		this.groups = this.parseProductConfig(this.productConfig);		
		await this.createUiHtml(this.groups);
		this.bindEvents();
		this.mobileUiInit();
	}

	protected applyUiVersionClass(){
		document.getElementsByTagName('html')[0].classList.add('customizer-ui-v1');
	}

	// x
	protected async getContainers(){

		this.parentContainer = this.getParentContainer();
		this.optionsContainer = this.getUiOptionsWrapper();
		this.closeCustomizerButton = this.getCloseCustomizerButton(); 
		this.addToCartButtons = this.getAddToCartButtons(); 

		return new Promise(resolve => resolve());
	}

	

	protected mobileUiInit(){
		this.mobileToggler = this.getMobileTogglerElement();
		this.bindMobileEvents();
	}

	protected getMobileTogglerElement(){
		return qs(this.uiConfig.mobileToggler || '.customizer-mobile-options')[0];
	}

	protected bindMobileEvents(){

		if(this.mobileToggler){

			const arrow = this.mobileToggler.querySelectorAll('.customizer-arrow')[0];
			let visible = false;
			const scope = this;

			this.mobileToggler.addEventListener('click', (e)=>{
				
				e.preventDefault();
				
				scope.parentContainer.classList.toggle('mobile-options-visible');
				arrow.classList.toggle('arrow-up');
				arrow.classList.toggle('arrow-down');
				visible = !visible;

			});

		}


	}

	
	protected additionalEvents(){

		Root.instance.events.listen(EVENT_TEXTURES_APPLIED, this.applyRules.bind(this));
		Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.applyRules.bind(this));

	}	

	public async applyRules(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;
        this.clearColorsElementClasses(this.colorElementGroups);

        let check = getAppliedRulesData(checkedData);

        for(let key in check.active){
            let color = check.active[key];
            this.setActiveColor(key, color, this.colorElementGroups);
        }

        this.disableInList(check.disable, this.colorElementGroups);

    }

	
	protected getCloseCustomizerButton(){
		return qs(this.uiConfig.closeCustomizer)[0];
	}

	protected getUiOptionsWrapper(){
		return qs(this.uiConfig.optionsWrapper)[0];
	}
	protected getParentContainer(){
		return qs(this.uiConfig.parentElement)[0];
	}

	
	/*
		parsed - parsed config with UI group and Model Parts inside
		THIS function can be changed freely in v2, etc classes
	*/
	protected async createUiHtml(parsed){

		for(let groupKey in parsed){
			
			const groupConfig: UiGroupInterface | any = this.getGroupConfig(groupKey);
			let parts = parsed[groupKey];

			if(parts.length === 0){
				console.warn('Parts not found for ', groupKey, parts);
			}

			if(groupConfig.type == 'color' || groupConfig.type == undefined){ // type = 'color' dont need to be defined, becaues thast basically a default value
				
				let div = await this.createColorPicker(groupConfig, parts, EVENT_UI_COLOR_CHANGED, groupKey);
				this.addToOptionsContainer(div);

			}else if(groupConfig.type == 'input'){

				let div = await this.createCustomTextInput(groupConfig, parts, EVENT_UI_INPUT_CHANGED, groupKey);
				this.addToOptionsContainer(div);

			}else{
				console.warn('Unknown UI part type', groupConfig.type);
			}
		
			
		}

		this.triggerUiCreatedEvent();
	}

	
	protected async createCustomTextInput(group, parts, eventName, groupKey){

		const div = document.createElement('div');
		div.classList.add('customizer-line');
		div.classList.add('customizer-ui-group');

		const heading = document.createElement('h3');
		heading.classList.add('color-group-title'); // damn this css class..
		heading.innerHTML = group.name || group.key;

		let isRequired = false;

		if(isDefined(group.rules) && isDefined(group.rules.required)){
			if(group.rules.required === true){
				heading.innerHTML += " (Required)";
				isRequired = true;
			}else{
				heading.innerHTML += " (Optional)";
			}
		}

		const input = document.createElement('input');
		input.classList.add('customizer-input');

		let maxTextLength = 1;

		if(isDefined(group.rules) && isDefined(group.rules.limit)){
			maxTextLength = group.rules.limit;
			input.placeholder = `Enter up to ${group.rules.limit} symbols`;
			input.maxLength = group.rules.limit;
		} else {
			input.placeholder = `Enter 1 symbol`;
			input.maxLength = maxTextLength;
		}

		let defaultText = '';
		if (group.value && String(group.value).length > 0) {
			defaultText = group.value;
		}

		input.value = defaultText;

		// TODO - save current custom text somewhere... (so we could compare when things change)
		let prevValue = defaultText;
		input.value = defaultText;

		const textParams = defaultTextParams(groupKey, group);

		// TODO - have a global fn for this.. because thats required in all UI versions
		input.addEventListener('change', (event: any) => {

			textParams.value = event.target.value;

			let key = group.key;
			console.log(key, 'changed to:"', textParams.value, '". String length?', textParams.value.length);

			if(prevValue == textParams.value) return;
			if(isRequired && textParams.value.length === 0) return;

			prevValue = textParams.value;

			// let target = event.target as HTMLElement;
			// EVENT_UI_INPUT_CHANGED
			// this.triggerInputChange
			Root.instance.events.call(eventName, null, null, event, parts, textParams);

		});

		// TODO - what is this??
		// TODO - move this part out of here
		// await Root.instance.events.listen(EVENT_TEXTURES_APPLIED, () => {
		// 	if (input.value) {
		// 		Root.instance.events.call(eventName, null, null, event, parts, textParams);
		// 	}
		// });


		div.appendChild(heading);
		div.appendChild(input);


		return new Promise(resolve => resolve(div));
	}



	protected addToOptionsContainer(div){
		this.optionsContainer.appendChild(div);
	}

	/*
		create html elements for color groups

		group - ui group config
		parts - { list: [related part keys], part: boolean }
		eventName - which event to trigger and send parts to

	*/
	protected async createColorPicker(group, parts, eventName, groupKey, returnElement?){

		// let scope = this;
		const div = document.createElement('div');
		div.classList.add('customizer-line');
		div.classList.add('customizer-ui-group');

		const heading = document.createElement('h3');
		heading.classList.add('color-group-title');
		heading.innerHTML = group.name || group.key;

		const ul = document.createElement('ul');
		ul.classList.add('color-group-wrapper');
		ul.setAttribute('id', getColorsListIdName(groupKey));

		this.colorElementGroups[groupKey] = {
			ul: ul,
			li: [], // color, element
		}

		let colors = group.value;

		for (var i = 0; i < colors.length; i++) {

			const color = colors[i];

			if( ! await this.colorAllowed(groupKey, color) ) continue;

			const colorElement = document.createElement('li');
			colorElement.style.backgroundColor = parseColor(color);
			colorElement.setAttribute('data-color', parseColor(color, 'clean'));
			colorElement.setAttribute('data-index', String(i + 1)); // TODO - why I need index?

			// console.log('!!! prep event', groupKey, parts);

			colorElement.addEventListener('click', (event) => {
				let target = event.target as HTMLElement;
				// EVENT_UI_COLOR_CHANGED
				// this.triggerColorChange
				Root.instance.events.call(eventName, null, null, event, parts, target.dataset.color, groupKey );
			});
			ul.appendChild(colorElement);
			this.colorElementGroups[groupKey].li.push({
				color: parseColor(color, 'clean'),
				element: colorElement,
			});
		}

		div.appendChild(heading);
		div.appendChild(ul);

		

		return new Promise(resolve => resolve(div));
	}


	// dont touch it, needed for v2
	public changeStep(...args) : void{};


}