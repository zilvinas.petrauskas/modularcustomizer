import { UiController } from './UiController';
import { EVENT_UI_INPUT_CHANGED, EVENT_UI_COLOR_CHANGED, EVENT_UI_ADD_TO_CART, EVENT_SHOW_CUSTOMIZER, EVENT_HIDE_CUSTOMIZER, EVENT_RESUME_CUSTOMIZER, EVENT_UI_CHANGE_STEP } from 'helpers/constants';
import { Root } from 'Root';
import { UI_v2 } from './UI_v2';
import { UI } from './UI';


// TODO - this is for V2 only
export class XilliamUiController extends UiController{

    protected ui : UI | UI_v2;

    constructor(){
        super();
    }

    protected additionalEvents(){

        // ui steps
        Root.instance.events.register(EVENT_UI_CHANGE_STEP, this.ui.changeStep.bind(this.ui));
    }

}