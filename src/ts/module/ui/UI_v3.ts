import { UI_v3_desktop } from 'component/ui/UI_v3_desktop';
import { DefaultUI } from './DefaultUi';
import { UI_v3_mobile } from 'component/ui/UI_v3_mobile';
import { Root } from 'Root';
import { EVENT_TEXTURES_APPLIED, EVENT_COLOR_CHANGE_COMPLETE, EVENT_UI_SELECT_GROUP, EVENT_PIN_SELECT } from 'helpers/constants';
import { qs } from 'helpers/helpers';

export class UI_v3 extends DefaultUI{

    protected groups: any;
    private container: any = {};
    private desktop: UI_v3_desktop;
    private mobile: UI_v3_mobile;

    constructor() {
        super();

        this.prepare();
      
    }

    protected async prepare(){

        this.listen()
        this.init();

    }

    protected additionalEvents(){

        Root.instance.events.listen(EVENT_PIN_SELECT, this.selectGroup.bind(this));
        Root.instance.events.listen(EVENT_UI_SELECT_GROUP, this.selectGroup.bind(this));

    }

    protected async init(){       

        this.applyUiVersionClass();

        await this.getConfigs();
		await this.getContainers();

        this.desktop = new UI_v3_desktop(this);
        this.mobile = new UI_v3_mobile(this);
        
        this.groups = this.parseProductConfig(this.productConfig);		
        this.bindEvents();

        // create html for desktop
        this.desktop.create(this.groups);

        // create html for mobile
        this.mobile.create(this.groups);

    }

    protected applyUiVersionClass(){
		document.getElementsByTagName('html')[0].classList.add('customizer-ui-v3');
	}

     // move to UI_v3 and directly access fucntion in this class
     public bindEvents(){

        // on load - get colors after textures have been applied
        Root.instance.events.listen(EVENT_TEXTURES_APPLIED, this.updateColorElement.bind(this));
        // on change - get colors once the color change is complete
        Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.updateColorElement.bind(this));

        Root.instance.events.listen(EVENT_TEXTURES_APPLIED, this.applyRules.bind(this));
		Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.applyRules.bind(this));
    }


    protected selectGroup(groupKey){

        this.desktop.expand(groupKey);
        this.mobile.select(groupKey);

    }

    protected updateColorElement(){

        this.desktop.update();
        this.mobile.update();

    }

    // TODO - need this on desktop AND mobile components
    protected async applyRules(){

        this.desktop.applyRules(); 
        this.mobile.applyRules();

    } 

    protected async getContainers(){

        // TODO - use ui config to get container

        this.addToCartButtons = this.getAddToCartButtons();  // same
        this.parentContainer = this.getParentContainer();

        this.container = {
            desktop : {
                options: document.querySelectorAll('.customizer-options')[0],
            },
            mobile : {
                options: 'TODO',
            }
            
        }

        return new Promise(resolve => resolve());
    }

     // TODO - check ui config for parent container too
     protected getParentContainer(){
        return qs('.customizer-ui-wrapper')[0];
    }


    /* defined below as empty functions and should NOT be used in UI_v3 (some stuff moved to components) */
    protected bindMobileEvents(){}
  

     // not used
     protected addToGroupContainer(groupDiv){
        
    }

}