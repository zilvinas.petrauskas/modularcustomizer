import { UI, UiGroupInterface } from './UI';
import { EVENT_UI_COLOR_CHANGED, EVENT_UI_INPUT_CHANGED, EVENT_UI_CHANGE_STEP, EVENT_COLOR_CHANGE, EVENT_COLOR_CHANGE_COMPLETE, EVENT_CHECK_COLOR_DATA, EVENT_TEXTURES_APPLIED } from 'helpers/constants';
import { Root } from 'Root';
import { parseColor, isDefined } from 'helpers/helpers';


// xilliam OLD UI
// TODO - if this ever gets used: extend from DefaultUI and copy missing functions from UI.ts to keep it separated from UI_v1
export class UI_v2 extends UI {

    private currentStep: number = 1;

    private container: any;

    constructor() {
        super();
    }

    protected prepare(){

        this.prepareStepContainers();
        this.bindBackButtons();

        // change ui step - show either first or second block in UI
        Root.instance.events.register(EVENT_UI_CHANGE_STEP, this.changeStep.bind(this));

        // on load - get colors after textures have been applied
        Root.instance.events.listen(EVENT_TEXTURES_APPLIED, this.updateColorElement.bind(this));
        // on change - get colors once the color change is complete
        Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.updateColorElement.bind(this));
       
    }

    protected prepareStepContainers(){
        this.container = {
            one : document.querySelectorAll('#customizerStep1 > .step-options')[0],
            two : document.querySelectorAll('#customizerStep2 > .step-options')[0]
        }
    }

    protected bindBackButtons(){
        let backButtons = document.querySelectorAll('.customizer-ui-back');
        if(backButtons){
            for(let i=0;i<backButtons.length;i++){
                backButtons[i].addEventListener('click', ()=>{
                    Root.instance.events.call(EVENT_UI_CHANGE_STEP, null, null, 1)
                })
            }
        }
    }

    protected applyUiVersionClass() {
        document.getElementsByTagName('html')[0].classList.add('customizer-ui-v2');
    }

    // thru event
    public changeStep(...args): void {

        // console.log('## CHANGE STEP', args)
        
        const step = args[0];
        const groupKey = args[1]; // undefined if step == STEP_ONE

        if(this.currentStep === step) return;
        this.currentStep = step;

        if(step == 2){

        
            let group = this.colorElementGroups[groupKey];
            // console.log('## TODO', group);
            // console.log('## all groups', this.colorElementGroups);

            for(let key in this.colorElementGroups){
                let current = this.colorElementGroups[key];
                if(isDefined(current.line)){
                    if(key == groupKey){
                        current.line.classList.add('visible');
                    }else{
                        current.line.classList.remove('visible');
                    }
                }
                
            }
           
            this.parentContainer.classList.add('ui-step2');

        }else{

            this.parentContainer.classList.remove('ui-step2');

        }

    }

    protected async updateColorElement(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;
        // console.log('### TODO COLOR CHECK', checkedData);

        for(let groupKey in checkedData){
            let color = checkedData[groupKey].color;
            this.updateGroupCssColor(groupKey, color);
        }
          
    }

    private updateGroupCssColor(groupKey, color){

        let groupActive = this.colorElementGroups[groupKey].active;
        for(let i=0;i<groupActive.length;i++){
            let element = groupActive[i].element;
            element.style.backgroundColor = parseColor(color, 'css');
        }

    }

    protected async createUiHtml(parsed) {

        // TODO - create v2 UI
        // console.log('## parsed', parsed);

        for (let groupKey in parsed) {

            const groupConfig: UiGroupInterface | any = this.getGroupConfig(groupKey);
            let parts = parsed[groupKey];

            if (parts.length === 0) {
                console.warn('Parts not found for ', groupKey, parts);
            }

            this.colorElementGroups[groupKey] = {

                // visible in step 1
                group: undefined,
                active: [],

                // visible in step 2
                line: undefined,
                ul: undefined,
                li: [], // color, element
               
            } as any;

            let groupDiv = this.createUiGroup(groupKey, groupConfig);
            this.addToGroupContainer(groupDiv);

            let groupItems;
             if (groupConfig.type == 'color' || groupConfig.type == undefined) { // type = 'color' dont need to be defined, becaues thast basically a default value

                groupItems = await this.createColorPicker(groupConfig, parts, EVENT_UI_COLOR_CHANGED, groupKey);

            } else if (groupConfig.type == 'input') {

                groupItems = await this.createCustomTextInput(groupConfig, parts, EVENT_UI_INPUT_CHANGED, groupKey);
                this.colorElementGroups[groupKey].line = groupItems;

            } else {
                console.warn('Unknown UI part type', groupConfig.type);
                continue;
            }

            this.addToOptionsContainer(groupItems);
        
        }


        this.triggerUiCreatedEvent();
    }

    protected addToGroupContainer(div){
        this.container.one.appendChild(div);
    }

    protected addToOptionsContainer(div){
        this.container.two.appendChild(div);
	}

    protected createUiGroup(groupKey, groupConfig) {


        let div = document.createElement('div');
        div.setAttribute('class', 'group-row');
        div.addEventListener('click', () => {
           Root.instance.events.call(EVENT_UI_CHANGE_STEP, null, null, 2, groupKey);
        });

        // colors
        let colorElementsWrapper = document.createElement('div');
        for (let i = 0; i < groupConfig.group.length; i++) {
            let colorElement = document.createElement('div');
            colorElement.setAttribute('class', 'color-group-item');
            // colorElement.setAttribute('id', this.groupItemId(groupConfig.group[i]));
            colorElementsWrapper.appendChild(colorElement);
            this.colorElementGroups[groupKey].active.push({
                element: colorElement,
                key: groupConfig.group[i],
            });
        }
        let countGroupItemsClass = (groupConfig.group.length > 1) ? 'color-group-count-' + groupConfig.group.length : '';
        colorElementsWrapper.setAttribute('class', 'color-group ' + countGroupItemsClass);
        div.appendChild(colorElementsWrapper);

        // text
        let text = document.createElement('div');
        text.setAttribute('class', 'group-line-name');
        text.innerHTML = groupConfig.name;
        div.appendChild(text);

        
        this.colorElementGroups[groupKey].group = div;

        return div;

    }


    // FOR UI V2
    // FOR UI V2
    // FOR UI V2
    protected async createColorPicker(group, parts, eventName, groupKey){

		// let scope = this;
		const div = document.createElement('div');
		div.classList.add('customizer-line');
		div.classList.add('customizer-ui-group');

		const ul = document.createElement('ul');
		ul.classList.add('color-group-wrapper');
        // ul.setAttribute('id', getColorsListIdName(groupKey));

        this.colorElementGroups[groupKey].line = div;
        this.colorElementGroups[groupKey].ul = ul;
        
        const title = document.createElement('div');
        title.classList.add('color-group-title');
        title.innerHTML = group.name;

		let colors = group.value;

		for (var i = 0; i < colors.length; i++) {

			const color = colors[i];

			if( ! await this.colorAllowed(groupKey, color) ) continue;

			const colorElement = document.createElement('li');
            colorElement.style.backgroundColor = parseColor(color);
			colorElement.setAttribute('data-color', parseColor(color, 'clean'));
			colorElement.setAttribute('data-index', String(i + 1)); // TODO - why I need index?

			// console.log('!!! prep event', groupKey, parts);

			colorElement.addEventListener('click', (event) => {
				let target = event.target as HTMLElement;
				// EVENT_UI_COLOR_CHANGED
				// this.triggerColorChange
				Root.instance.events.call(eventName, null, null, event, parts, target.dataset.color, groupKey );
			});
            ul.appendChild(colorElement);
            
			this.colorElementGroups[groupKey].li.push({
				color: parseColor(color, 'clean'),
				element: colorElement,
			});
		}

		div.appendChild(ul);
		div.appendChild(title);

        
        return new Promise(resolve => resolve(div));
	}

}