import { Root } from 'Root';
import { EVENT_SEND_PRODUCT_INFO, EVENT_PROGRESS_ADD, EVENT_CHANGE_MASK, EVENT_CHECK_COLOR_DATA, EVENT_GET_COLOR_FEEDER_INFO, EVENT_GET_UI_GROUP_NAME, EVENT_COLOR_CHANGE, EVENT_CUSTOM_TEXT_CHANGE, EVENT_CHECK_COLOR_ALLOWED, EVENT_CUSTOMIZER_VISIBLE, EVENT_COLORS_DATA_FOR_EXPORT, EVENT_GET_UI_SIZE, EVENT_GET_CUSTOM_TEXT_PARAMS } from 'helpers/constants';
import { qs } from 'helpers/helpers';
import { defaultTextParams } from './UiHelpers';

/* just some helper functions */
export class DefaultUI{


    protected uiConfig;
    protected productConfig;
    
    protected parentContainer: HTMLElement;
	protected optionsContainer: HTMLElement;
	protected mobileToggler: HTMLElement;
	protected closeCustomizerButton: HTMLElement;
    protected addToCartButtons: any;

    protected colorElementGroups: any = {}; // holds ul, li for colors
    
    constructor(){}

	protected listen(){
		
		Root.instance.events.register(EVENT_CUSTOMIZER_VISIBLE, this.show.bind(this) );
		Root.instance.events.register(EVENT_GET_UI_GROUP_NAME, this.getGroupName.bind(this));
		Root.instance.events.register(EVENT_GET_UI_SIZE, this.getUiSize.bind(this));
		Root.instance.events.register(EVENT_COLORS_DATA_FOR_EXPORT, this.getColorsInfoForExport.bind(this));
		Root.instance.events.register(EVENT_GET_CUSTOM_TEXT_PARAMS, this.getInitialTextParams.bind(this));

		this.additionalEvents();
	}

	protected additionalEvents(){

	}

    public triggerAddToCart(...args){

		// call order manager?
		Root.instance.events.call(EVENT_SEND_PRODUCT_INFO, null, null);

    }

    // do things related to UI, then  trigger another event for color change
	// triggered by event EVENT_UI_COLOR_CHANGED
	// onUiColorChange
	public triggerColorChange(...args){

		const event = args[0];
		const parts = args[1];
		const newColor = args[2];
				
		Root.instance.events.call(EVENT_COLOR_CHANGE, null, null, parts, newColor);
	}

	protected async getInitialTextParams(partKey){

		let modelConfig = Root.instance.config.product.get();
		let part = modelConfig.parts.find(item => item.key == partKey);

		const textParams = []; 
		let textConfigs = {};
		let textLayers = [];

		// find layer with type == 'text' on this part
		for(let i=0;i<part.layers.length;i++){
			if(part.layers[i].type == 'text')
				textLayers.push( part.layers[i] );
		}

		for(let i=0;i<modelConfig.ui.length;i++){
			for(let y=0;y<textLayers.length;y++){
				if( modelConfig.ui[i].key == textLayers[y].key ){
					// let key = textLayers[y].key;
					let key = modelConfig.ui[i].key;
					textConfigs[ key ] = {
						layer: textLayers[y],
						ui: modelConfig.ui[i],
					}
					
				}
			}
		}
		
		for(let key in textConfigs){
			let layerKey = textConfigs[key].layer.key;
			if(typeof layerKey === 'undefined' || layerKey == null) console.warn('!!! MISSING KEY IN LAYERS -> check layers associated with UI KEY', key);

			// let config = {... textConfigs[key].ui };
			// config.value = config.color;

			let params = defaultTextParams(layerKey, textConfigs[key].ui);
			textParams.push(params);
		}

		return new Promise(resolve => resolve(textParams));
	}

	public applyRulesToChangedInput(inputValue: string, rules: any = {}){

		if(rules && rules.uppercase === true){
			inputValue = inputValue.toUpperCase();
		}

		return inputValue;
	}

	public triggerInputChange(...args){

		console.log('triggerInputChange', args[1], args[2]);

		const event = args[0];
		const parts = args[1];
		const textParams = args[2];

		Root.instance.events.call(EVENT_CUSTOM_TEXT_CHANGE, null, null, parts, textParams);

	}
	
	public triggerUploadImage(...args) {

		console.log('triggerUploadImage', args);

		const event = args[0];
		const parts = args[1];
		const image = args[2];
		const groupKey = args[3];

		Root.instance.events.call(EVENT_CHANGE_MASK, null, null, parts, image, groupKey);


	}
    
    protected async colorAllowed(...args){
		return await Root.instance.events.call(EVENT_CHECK_COLOR_ALLOWED, null, true, ...args);
		// return this.rules.colorAllowed(...args);
	}
    
    public resume(){

		this.show();

    }
    
    // TODO - trigger event to start rendering scene
	public show(){

		document.getElementsByTagName('html')[0].classList.add('customizer-loaded');

	}

	// TODO - add event to stop threejs rendering if its not visible
	public hide(){
		
		document.getElementsByTagName('html')[0].classList.remove('customizer-loaded');
		
    }
    
    protected triggerUiCreatedEvent(){
		Root.instance.events.call(EVENT_PROGRESS_ADD, null, null, 'ui');
    }
    
    protected getGroupName(groupKey){

		let groupConfig = this.getGroupConfig(groupKey) as any;

		return groupConfig.name;

	}
	

	protected getUiSize(){
		
		let w = 0;
		let h = 0;

		if(typeof this.parentContainer !== 'undefined'){
			w = this.parentContainer.scrollWidth || this.parentContainer.offsetWidth || 0;
			h = this.parentContainer.scrollHeight || this.parentContainer.offsetHeight || 0;
		}else{
			console.warn('Parent container for UI not found!', this.uiConfig.parentContainer, this.parentContainer);
		}

		return {
			width: w,
			height: h,
		}
	}

    protected async checkColorRule(){
		return await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true);
    }
    
    /*
		Return obj: {colorKey => Color Code (Color Name)}
	*/
	protected async getColorsInfoForExport(){

		const colorData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;

		const data: any = {};

        for(let groupKey in colorData){

            let colorCode = colorData[groupKey].color;
			let colorInfo = await Root.instance.events.call(EVENT_GET_COLOR_FEEDER_INFO, null, null, colorCode) as any;
			
			let groupName = await Root.instance.events.call(EVENT_GET_UI_GROUP_NAME, null, true, groupKey) as string;

			data[groupName] = `${colorCode} (${colorInfo.name})`;

        }

		return data;

    }
    
    protected clearColorsElementClasses(colorElementGroups){

		for(let groupKey in colorElementGroups){
			let li = colorElementGroups[groupKey].li;
			for(let i=0;i<li.length;i++){
				li[i].element.setAttribute('class','');
			}
		}

	}

	protected async setActiveColor(groupKey, color, colorElementGroups){

		let li = colorElementGroups[groupKey].li;
		for(let i=0;i<li.length;i++){
			if( li[i].color == color ){
				li[i].element.classList.add('active');
			}
		}

	}

	protected disableInList(disable, colorElementGroups){


		for(let groupKey in disable){

			if(disable[groupKey].length > 0){

				for(let g in disable[groupKey]){
					let group = disable[groupKey][g];
					// group.groupKey
					// group.color

					let li = colorElementGroups[ group.groupKey ].li;
					for(let i=0;i<li.length;i++){
						if(li[i].color == group.color){
							li[i].element.classList.add('disabled');
						}
					}
				}

			}

		}

	}

	protected disableAllBut(notGroupKey, color, colorElementGroups){

		for(let groupKey in colorElementGroups){
			if(groupKey == notGroupKey) continue;

			let li = colorElementGroups[groupKey].li;
			for(let i=0;i<li.length;i++){
				if(li[i].color == color){
					li[i].element.classList.add('disabled');
				}
			}
		}


    }
    
    protected async getConfigs(){
	
		this.uiConfig = Root.instance.config.ui.get();
		this.productConfig = Root.instance.config.product.get();

		
		// console.log('~~ uiConfig', this.uiConfig);
		// console.log('~~ productConfig', this.productConfig);

		return new Promise(resolve => resolve());
    }
    
    protected getAddToCartButtons(){

        let query = '.customizer-add-cart';
        if(this.uiConfig.hasOwnProperty('addToCart')) query = this.uiConfig.addToCart;
        
		return qs(query);
	}
    
    protected getGroupConfig(groupKey){

		let foundGroup = {};
		for(let i=0;i<this.productConfig.ui.length;i++){
			let group = this.productConfig.ui[i];
			if(group.key == groupKey){
				foundGroup = group;
				break;
			}
		}

		return foundGroup;
	}
    
    protected bindEvents(){

		const scope = this;

		if(this.closeCustomizerButton){
			this.closeCustomizerButton.addEventListener('click', (e) => {
				e.preventDefault();
				scope.hide();
			});
		}


		// addToCart
		if(this.addToCartButtons){
			// console.log('CART BUTTONS?', this.addToCartButtons);
			for(let i=0;i<this.addToCartButtons.length; i++){
				this.addToCartButtons[i].addEventListener('click', (e)=>{
					e.preventDefault();
					scope.triggerAddToCart();
				})
			}
		}

    }

    protected parseProductConfig(config){

		if( ! config.hasOwnProperty('ui')){
			console.warn('ui configuration doesnt exist in product config', config);
			return;
		}

		let parsed = {};

		for(let i=0;i<config.ui.length;i++){

			let uiKey = config.ui[i].key;

			if( ! parsed.hasOwnProperty(uiKey) ) parsed[uiKey] = [];

			// go thru all the groups for this UI item
			if( ! config.ui[i].hasOwnProperty('group')){
				console.warn('Missing group parameter for UI ', config.ui[i] );
				continue;
			}

			for(let g=0;g<config.ui[i].group.length;g++){

				let uiGroupKey = config.ui[i].group[g];

				// check all the parts first
				for(let y=0;y<config.parts.length;y++){

					let part = config.parts[y];

					// compare part's group string to uiGroupKey
					if(uiGroupKey == part.group){

						parsed[uiKey].push({
							key: part.key,
							layerKey: undefined,
							group: uiKey,
						});

					}
					

					// and check all the layers after to match group 
					if(part.hasOwnProperty('layers') && part.layers.length > 0){

						for(let u=0;u<part.layers.length;u++){

							let layer = part.layers[u];
		
							if(layer.hasOwnProperty('group') && layer.group != undefined && layer.group == uiGroupKey){
	
								parsed[uiKey].push({
									key: part.key,
									layerKey: layer.key,
									group: uiKey,
								});
	
							}
						}

					}else{
						console.warn('!!! this part doenst have layers', part.key , part.layers, part);
					}
					

				}

			}

		}

		// console.log('!!! parsed', parsed);

		return parsed;
	}
	
	


}