import { UI } from './UI'
import { UI_v2 } from './UI_v2'
import { Root } from 'Root';
import { EVENT_HIDE_CUSTOMIZER, EVENT_SHOW_CUSTOMIZER, EVENT_UI_ADD_TO_CART, EVENT_UI_COLOR_CHANGED, EVENT_UI_INPUT_CHANGED, EVENT_RESUME_CUSTOMIZER, EVENT_UI_UPLOAD_IMAGE } from 'helpers/constants';
import { isDefined } from 'helpers/helpers';
import { UI_v3 } from './UI_v3';

// TODO - should be named UIFacade and move to Facades folder
export class UiController{

    protected ui : UI | UI_v2 | UI_v3;

    constructor(){

        this.detectVersionToLoad();
        this.bindEvents();
    }

    protected async detectVersionToLoad(){

        const uiConfig = Root.instance.config.ui.get();
        let version = 'v1'; // default version used if nothing found in UI config

        if(isDefined(uiConfig.version)){
            version = uiConfig.version;
        }

        console.log('------ UI version', version);

        switch(version){
            case 'v3':          this.ui = new UI_v3();    break; // xilliam updated UI
            case 'v2':          this.ui = new UI_v2();    break; // xilliam old UI
            default: /* v1 */   this.ui = new UI();       break; // calamigos default - in case unsupported ui version is provided
        }

    }

    protected bindEvents(){

        // on color/input change
        Root.instance.events.register(EVENT_UI_COLOR_CHANGED, this.ui.triggerColorChange.bind(this.ui));
        Root.instance.events.register(EVENT_UI_INPUT_CHANGED, this.ui.triggerInputChange.bind(this.ui));
        Root.instance.events.register(EVENT_UI_UPLOAD_IMAGE, this.ui.triggerUploadImage.bind(this.ui));

        // add to cart clicked
        Root.instance.events.register(EVENT_UI_ADD_TO_CART, this.ui.triggerAddToCart.bind(this.ui));

        // customizer visibility
        Root.instance.events.register(EVENT_SHOW_CUSTOMIZER, this.ui.show.bind(this.ui), { unique: false });
        Root.instance.events.register(EVENT_HIDE_CUSTOMIZER, this.ui.hide.bind(this.ui), { unique: false });

        if( ! Root.instance.events.exists(EVENT_RESUME_CUSTOMIZER) ){ // check if state manager exists (state manager will bind this event first)
            Root.instance.events.register(EVENT_RESUME_CUSTOMIZER, this.ui.resume.bind(this.ui), { unique: false });
        }


        this.additionalEvents();
    }

    // can be used to extend this controller and bind additional events if needed
    protected additionalEvents(){}

}