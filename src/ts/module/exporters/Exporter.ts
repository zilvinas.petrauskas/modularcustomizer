import { Root } from 'Root';
import { EVENT_COLLECT_PRODUCT_INFO, EVENT_GET_PRODUCT_IS_CUSTOMIZED, EVENT_TAKE_SCREENSHOT, EVENT_COLORS_DATA_FOR_EXPORT } from 'helpers/constants';
import { DPF } from 'module/order/DynamicPropertiesFinder';

export class Exporter{
    
    constructor(){

        Root.instance.events.register(EVENT_COLLECT_PRODUCT_INFO, this.collect.bind(this), null);

    }

    // ONLY FOR PILLOW
    // TODO - test new defaultExpoter and create config for pillows too
    public async collect(){

        let params: any = {
            post: 'save-product',
            product: {
                handle: Root.instance.config.product.get('handle'),
                originalHandle: await DPF.getOriginalHandle(),
                id : this.getRealId(), // id used in shopify
                variant: this.getVariant(),
                quantity: 1,
                colors: await Root.instance.events.call(EVENT_COLORS_DATA_FOR_EXPORT, null, true),
            },
            isCustomized: await this.isCustomized(),
        }

        params.product.type = 'pillow';
        params.product.size = 'Default';

        // params.masks = await Root.instance.events.call('EVENT_GET_MASKS_TODO', null, true); // dont need
        params.screenshot = await Root.instance.events.call(EVENT_TAKE_SCREENSHOT, null, true);

        params.variantText = "Default"; // pillows dont have variants or sizes

        params.makeTemp = true; // save stuff to backend for checkout

       

        return params;
    }

    private getRealId(){
        let elem = document.getElementById('section-product');
        if(elem){
            return elem.dataset.id;
        }
        console.warn('Real product id not found! Looking for element #section-product', elem)
        return '';
    }

    private getVariant(){
        var url = new URL(window.location.href);
        var variant = url.searchParams.get("variant") || '';
        return variant;
    }
    
    private async isCustomized(){
        return await Root.instance.events.call(EVENT_GET_PRODUCT_IS_CUSTOMIZED, null, true) == 'yes';
    }

}