import { Root } from 'Root';
import { EVENT_GET_MODEL_NAME, EVENT_COLORS_DATA_FOR_EXPORT, EVENT_GET_PRODUCT_IS_CUSTOMIZED, EVENT_TAKE_SCREENSHOT, EVENT_GET_PRODUCT_TYPE, EVENT_CUSTOMIZER_READY, EVENT_GET_CUSTOM_TEXT_DATA } from 'helpers/constants';
import { isDefined, getOriginalHandle } from 'helpers/helpers';

export class DefaultExporter{

    // TODO - based of product type get MODEL_CONFIG from model.default.js
    // parse its config array
    // get values
    // prepare data for export

    // TODO - brainstorm if MODEL_CONFIG should have model-name OR.. should have an array of product types (sweater-male, sweater-female)
    // TODO - brainstorm if its possible to have different exported params usign same 3d model??

    private name: string;
    private config: any;

    constructor(){

        this.prepare();
    }

    protected async prepare(){

        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.init.bind(this));

    }

    protected async init(){

        this.name = await this.getProductName() as string;

        if(!this.name){
            console.log('!!! Exporter init failed, because product name not found', this.name);   
            return;
        }

        this.config = this.getModelExporterConfig(this.name);
        
        if(!this.config) return;

    }

    
    protected getModelExporterConfig(name){

        let exporterConfig = Root.instance.config.exporter.get();

        if(typeof exporterConfig === 'undefined' || exporterConfig === null){
            console.warn('Model config not found! Make sure model config has MODEL_CONFIG constant defined.', exporterConfig);
            alert('Model config not found in Exporter');
            return undefined;
        }

        let row = undefined;

        try{
            row = exporterConfig.find(item => item.product == name);
        }catch(e){
            console.warn(e);
            console.log('Probably {name}.exporter.js config is not included!');
        }
        

        if( row == undefined ){
            console.warn('Model config not found by name', name, exporterConfig);
            alert('Model config not found by name!');
            return undefined;
        }

        return row.config;      
    }
    
    protected blankData(){

        let data  = {} as any;

        data.post = 'save-product'; // required by backend to verify actions
        data.makeTemp = true;

        return data;
    }

    protected async parse(){

        let data  = this.blankData();

        console.log('parsing config', this.config);

        for(let i=0;i<this.config.length;i++){

            let row = this.config[i];
            let rowData = await this.parseRowData(row.id, row);

            if(row.id == 'product_custom_text') console.log('!! 3', row.id, rowData);

            if( row.hasOwnProperty('optional') && row.optional === true && !isDefined(rowData) ){
                continue;
            }else if( ( !row.hasOwnProperty('optional') || row.optional === false ) && !isDefined(rowData) ){
                console.warn('Failed to get exporter data for ', row.key, rowData, row);
                continue;
            }

            // console.log('CHECK #'+row.id, rowData);

            if(typeof row.group !== 'undefined' && row.group !== null){

                if(!data.hasOwnProperty(row.group)) data[row.group] = {};
            
                data[row.group][row.key] = rowData;

            }else{

                data[row.key] = rowData;

            }
            
        }

        return new Promise(resolve => resolve(data));
    }

    protected async parseRowData(id, rowConfig){

        let rowData = undefined;

        switch(id){

            case 'custom_value':        rowData = this.customValueFromConfig(rowConfig); 
                break;

            case 'product_type':            rowData = await this.getProductType();
                break;
                
            case 'product_size':            rowData = await this.getProductSize();
                break;

            case 'product_colors':          rowData = await  this.getProductColors();
                break;

            case 'product_custom_text':      rowData = await this.getProductCustomText();
                break;

            case 'product_masks':               rowData = await this.getProductMasks();
                break;
                
            case 'product_screenshot':          rowData = await this.getProductScreenshot(rowConfig);
                break;
                
            case 'product_gender':              rowData = await this.getProductGender();
                break;

            case 'product_customized':          rowData = await this.getProductIsCustomized();
                break;

            // shopify

            case 'shopify_product_handle':      rowData = this.getShopifyProductHandle();
                break;

            case 'shopify_original_handle':     rowData = await this.getShopifyOriginalHandle();
                break;

            case 'shopify_product_id':          rowData = await this.getShopifyProductId();
                break;

            case 'shopify_product_size':        rowData = await this.getShopifyProductSize();
                break;

            case 'shopify_product_variant':         rowData = await this.getShopifyProductVariantId();
                break;

            case 'shopify_product_variant_text':  rowData = await this.getShopifyVariantName();
                break;

            case 'shopify_quantity':            rowData = await this.getShopifyProductQuantity();
                break;
           
            default:        console.warn('Exporter - parse by key - not defined fn for key: ', id);
            break;

        }


        return new Promise(resolve => resolve(rowData));
    }

    protected customValueFromConfig(config){
        if(config.hasOwnProperty('value')){
            return config.value;
        }
        console.warn('!! Missing DEFAULT VALUE for custom_value key', config);
        return '';
    }
 
    protected getShopifyProductHandle(){
        let handle = Root.instance.config.product.get('handle');
        return handle;
    }

    /*
        returns: string (handle used to recognize product e.g. CC001, CCB004, etc)
    */
    protected async getShopifyOriginalHandle(){

        let originalHandle = getOriginalHandle();
        if(typeof originalHandle !== 'undefined' && originalHandle !== null){
            return originalHandle;
        }
        let parsedHandle = Root.instance.config.product.get('handle');
        return parsedHandle;
    }

    /*
        returns: string (product id from shopify's html)
    */
    protected async getShopifyProductId(){
        let elem = document.getElementById('section-product');
        if(elem){
            return elem.dataset.id;
        }
        console.warn('Real product id not found! Looking for element #section-product', elem)
        return '';
    }

    /*
        returns: string
    */
    protected async getProductType(){
        return await Root.instance.events.call(EVENT_GET_PRODUCT_TYPE, null, true);
    }

    /*
        only for sweaters atm
         returns: string
    */
    protected async getShopifyProductSize(){
        const options = document.querySelectorAll('ul.color-false[data-option="option1"] > li.active');
        if(options.length > 0){
            return options[0].getAttribute('data-text'); // data-text -> XS, S, M, L, XL
        }
        console.warn('!! getShopifyProductSize NOT FOUND', options);
        return '';
    }

    /*
        for pillows, blanket, small blankets..
        returns: string
    */
    protected async getProductSize(){
        return 'Default'; 
    }

    // TODO - same as size.. but backend required variant name
    protected getShopifyVariantName(){
       return this.getShopifyProductSize();
    }

    /*
        get shopify variant id-string from url
        returns: number
    */
    protected async getShopifyProductVariantId(){
        var url = new URL(window.location.href);
        var variant = url.searchParams.get("variant") || '';
        return variant;
    }

    /*
        shopify quantity is always 1
        returns: number
    */
    protected async getShopifyProductQuantity(){
        return 1;
    }

    /*
        returns: string with color names separated by comma (color1, color2, etc)
    */
    protected async getProductColors(){
        return await Root.instance.events.call(EVENT_COLORS_DATA_FOR_EXPORT, null, true);
    }


    protected async getProductCustomText(){

        const customText = await Root.instance.events.call(EVENT_GET_CUSTOM_TEXT_DATA, null, true);
        return new Promise(resolve => resolve(customText));
    }


    /*
        array of all colors with color name and color hex code e.g. ffffff (White)
    */
    protected async getProductCustomColors(){

    }

    protected async getProductMasks(){
        return []; // not needed anymore?
    }

    /*
        returns: {
            image: image-as-stringy-data
            width: number - image width
            height: number - image height
        }
    */
    protected async getProductScreenshot(rowConfig){
        return await Root.instance.events.call(EVENT_TAKE_SCREENSHOT, null, true, rowConfig);
    }

    /*
        returns: string
    */
    protected async getProductGender(){

    }

    /*
        product name
        returns: string
    */
    protected async getProductName(){

        let name = await Root.instance.events.call(EVENT_GET_MODEL_NAME, null, true);

        if(typeof name === 'undefined' || name === null){
            console.warn('Model name unknonw in Exporter!', name);
            alert('Unknown model name in Exporter');
        }

        return new Promise(resolve => resolve(name));
    }

    /*
        if product was customized
        returns: boolean
    */
    protected async getProductIsCustomized(){
        return await Root.instance.events.call(EVENT_GET_PRODUCT_IS_CUSTOMIZED, null, true) == 'yes';
    }


    


}