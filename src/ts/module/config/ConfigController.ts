import { AJAX } from 'helpers/ajax';

export class ConfigController{

    protected config;
    private _tempConfig;

    constructor(config){

        this._tempConfig = config; 
    }

    public async init(){

        await this.load(this._tempConfig);
    }

    protected async load(config){
        
        if(config.url){ // load config from external file

            await this.getConfigFile(config.url).then((_config)=>{

                this.config = _config;

            }).catch((err)=>{
                console.warn('Config failed to load: ',err);
            });

        }else{
                        
            await this.setConfig(config);

        }

    }

    private async setConfig(_config){

        return new Promise(resolve => {
                // setTimeout(()=>{

                    this.config = _config;
                    resolve();

                // }, 0);
        })

    }

    private getConfigFile(url){

        return new Promise((resolve, reject)=>{

            AJAX.load({
                url: url,
                method: 'GET',
                xhrFields: {
                    withCredentials: true
                },
                onComplete: (jsonResponse) =>{
                
                    let config = JSON.parse(jsonResponse);
                    resolve(config);

                },
                onFail: (err) => {
                    reject(err);
                }
            });           

        });

    }

    protected get(param?){
        
        if(typeof param !== 'undefined' && param !== null){
                
            if(this.config.hasOwnProperty(param)){
                return this.config[param];
            }else{
                console.warn('Paramater not found in config', param, this.config);
                return undefined;
            }
        }
        return this.config;


        // return new Promise( (resolve, reject) => {

        //     if(typeof param !== 'undefined' && param !== null){
                
        //         if(this.config.hasOwnProperty(param)){
        //             resolve(this.config[param]);
        //         }else{
        //             resolve(undefined);
        //         }
        //     }
        //     resolve(this.config);

        // });

        
    }
    
}