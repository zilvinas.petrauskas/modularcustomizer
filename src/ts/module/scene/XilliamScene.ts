import { Scene } from './Scene';
import * as THREE from 'three';
import * as THREEGL from 'three/examples/jsm/WebGL';

export class XilliamScene extends Scene{

    constructor(){
        super();
    }

    protected getCameraPosition(){
        return  new THREE.Vector3(0,47,661);
    }

    
    protected createThreeCanvas(){

        const canvas = document.createElement('canvas');

         // transparent BG - fix black bg on ios + ipad + safari
        const gl = canvas.getContext("webgl", { premultipliedAlpha: false });
        gl.clearColor(1, 1, 0, 0);
        gl.clear(gl.COLOR_BUFFER_BIT);

        this.container.appendChild(canvas);

        return canvas;
    }

    // transparent bg
    protected createSky(){
        this.renderer.setClearColor( 0xffffff, 0 ); // NEW
    }

    protected createScene(){
        const scene = new THREE.Scene();
        // scene.background = new THREE.Color( 0xFFFFFF );
        return scene;
    }

    protected async createRenderer(){
        
        const rendererParams = <any>{
            antialias: true,
            canvas: this.getCanvas(),
        }

        if ( THREEGL.WEBGL.isWebGL2Available() ){
            rendererParams.context = this.getCanvas().getContext( 'webgl2', { alpha: true } ); // NEW - alpha= true
        }else{
            rendererParams.context = this.getCanvas().getContext('experimental-webgl2');
        }

        const size = await this.getCanvasSizes();
        rendererParams.canvas.width = size.width;
        rendererParams.canvas.height = size.height;

        const renderer = new THREE.WebGLRenderer(rendererParams);
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( size.width, size.height );
        renderer.gammaOutput = true;
        renderer.gammaFactor = 1.2;

        renderer.context.canvas.addEventListener('webglcontextlost', this.webglContextError);

        return renderer;
    }

}