
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Root } from 'Root';
import { EVENT_GET_THREE_CAMERA, EVENT_GET_THREE_CANVAS, EVENT_ADD_TO_RENDERER, EVENT_GET_THREE_RENDERER, EVENT_TRIGGER_RENDER, EVENT_AFTER_CONTROLS_UPDATE, TRIGGER_MANUAL_CONTROLS_UPDATE, EVENT_CUSTOMIZER_READY, EVENT_CUSTOMIZER_SCENE_READY, EVENT_GET_CONTROLS, EVENT_MOUSE_PRESS, EVENT_MOUSE_RELEASE } from 'helpers/constants';

export class SceneControls{
    

    public controls : OrbitControls;
    public controlsConfig;
    private onControlsUpdateFnList : any = [];

    constructor(){

        this.controlsConfig = this.getControlsConfig();

        Root.instance.events.listen(EVENT_CUSTOMIZER_SCENE_READY, this.init.bind(this) );
        Root.instance.events.listen(EVENT_TRIGGER_RENDER, this.triggerControlsUpdate.bind(this) );

        Root.instance.events.register(EVENT_AFTER_CONTROLS_UPDATE, this.addFunctionAfterControlsUpdate.bind(this));
        Root.instance.events.register(TRIGGER_MANUAL_CONTROLS_UPDATE, this.afterControlsUpdate.bind(this));
        Root.instance.events.register(EVENT_GET_CONTROLS, this.getControls.bind(this));
    
    }

    public async init(){
        this.controls = await this.createControls() as OrbitControls;
    }

    public getControls(){
        return this.controls;
    }

    protected getControlsConfig(){

        return {
			minDistance: 50,
			maxDistance: 1800,
			minPolarAngle: Math.PI * 0,
			maxPolarAngle: Math.PI * 1,
            enableZoom: true,
            enablePan: false,
            enableDamping: true,
            dampingFactor: 0.15,
            rotateSpeed: 0.25,
            // autoRotate: false,
            // autoRotateSpeed: 0.15,
		}

    }

    protected triggerControlsUpdate(){
        this.controls.update();
    }

    protected async createControls(){

        let camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.Camera;
        let renderer = await Root.instance.events.call(EVENT_GET_THREE_RENDERER, null, true) as THREE.Renderer;
        let canvas = await Root.instance.events.call(EVENT_GET_THREE_CANVAS, null, true) as HTMLElement;

        const controls = new OrbitControls( camera, renderer.domElement );

        if(this.controlsConfig)
            for(let key in this.controlsConfig) controls[key] = this.controlsConfig[key];
        
        
        const toggleControlLock = (_toggle)=>{
            controls.autoRotate = _toggle;
        }

        controls.addEventListener('change', ()=>{
            this.afterControlsUpdate();
        })

        // TODO - where's mouseup and touchend located???
        canvas.addEventListener('mousedown', ()=>{
            Root.instance.events.broadcast(EVENT_MOUSE_PRESS, null);
            return toggleControlLock(false);
        });
        window.addEventListener('mouseup', ()=>{
            Root.instance.events.broadcast(EVENT_MOUSE_RELEASE, null);
        })
        canvas.addEventListener('touchstart', ()=>{
            Root.instance.events.broadcast(EVENT_MOUSE_PRESS, null);
            return toggleControlLock(false);
        });
        window.addEventListener('touchend', ()=>{
            Root.instance.events.broadcast(EVENT_MOUSE_RELEASE, null);
        })


        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'update-orbit', this.triggerControlsUpdate.bind(this));

        
        return new Promise(resolve => resolve(controls));
    }

    protected addFunctionAfterControlsUpdate(...args){
        let fn = args[0];
        this.onControlsUpdateFnList.push( fn );
    }

    private afterControlsUpdate(){
        
        if(this.onControlsUpdateFnList.length > 0){
            for(let i=0;i<this.onControlsUpdateFnList.length;i++){
                this.onControlsUpdateFnList[i](this.controls);
            }
        }
    }

}