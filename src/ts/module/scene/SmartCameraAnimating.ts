
import * as THREE from 'three';
import { getPointInBetweenByPerc } from 'helpers/helpers';
import { EVENT_ADD_TO_SCENE, EVENT_CAMERA_ANIMATION, EVENT_CAMERA_RESET_ANIMATION, EVENT_MODEL_READY, EVENT_GET_MODEL_DIMENSIONS, EVENT_CUSTOMIZER_READY, EVENT_GET_THREE_CAMERA, EVENT_GET_CONTROLS, EVENT_ADD_TO_RENDERER, TRIGGER_MANUAL_CONTROLS_UPDATE } from 'helpers/constants';
import { Root } from 'Root';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { isArray } from 'util';
const TWEEN = require('tween.js');

export class SmartCameraAnimating{

    private debug : boolean = false;

    private camera : THREE.PerspectiveCamera;
    private controls: OrbitControls;
    private modelDimensions : any;
    private current : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }
    private previous : any = {
        position: new THREE.Vector3(),
        target: new THREE.Vector3()
    }
    private defaults : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }
    private animations : any = {};
    private busy : boolean = false;
    private forceStop : boolean = false;

    private animationId : number = 0;

    constructor(){

        this.listen();
    }

    protected listen(){

        Root.instance.events.listen(EVENT_MODEL_READY, this.getModelSize.bind(this));
        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.prepare.bind(this));

        Root.instance.events.register(EVENT_CAMERA_ANIMATION, this.cameraAnimation.bind(this));
        Root.instance.events.register(EVENT_CAMERA_RESET_ANIMATION, this.resetCameraAnimation.bind(this));

    }

    protected async prepare(){

        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.PerspectiveCamera;
        this.controls = await Root.instance.events.call(EVENT_GET_CONTROLS, null, true) as OrbitControls;

        this.defaults.position.copy(this.camera.position);
        this.defaults.target.copy(this.controls.target);

        // triggers tween udpate together with renderer update
        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'tween', ()=>{
            TWEEN.update();
        });

    }

    // TODO - reset camera position to default/initial one
    protected resetCameraAnimation(){

        let endCameraPos = new THREE.Vector3().copy( this.defaults.position );
        let targetEndPosition = new THREE.Vector3().copy( this.defaults.target );

        let route = [];
        route.push(endCameraPos);

        this.animateRoute(endCameraPos, targetEndPosition);

    }

    protected cameraAnimation(...args){

        const cameraEndPosition = args[0];
        const targetEndPosition = args[1];
        const params = args[2] || {};        

        const a = new THREE.Vector3().copy(this.camera.position);
        const b = new THREE.Vector3().copy(cameraEndPosition);
        
        let transition = 'default';
        if(params.transition) transition = params.transition;

        let middlePointAt = 0.25;
        let defaultAngle = 45; // degrees
        let clockwise, route = [];
        if(params.angle) defaultAngle = params.angle;

        // console.log('ANGLE', defaultAngle);

        if(transition == 'direct'){

            route.push( cameraEndPosition );

            if(this.debug){
                let visual = [...route];
                visual.push(this.previous.position);
                this.previewRoute(visual);
                this.previewVectorPosition(visual[0], new THREE.Color('blue'));
                this.previewVectorPosition(visual[1], new THREE.Color('blue'));
            }

        }else{

            clockwise = b.x > 0;
            if(a.z < 0) clockwise = !clockwise; // when viewing from back, sides change too

            const angle = clockwise ? THREE.Math.degToRad(defaultAngle) : THREE.Math.degToRad(-defaultAngle); // works on front, but not from back
            const up = new THREE.Vector3( 0, 1, 0 );
            const c0 = getPointInBetweenByPerc(a,b,middlePointAt);
            const c = new THREE.Vector3().copy(c0).applyAxisAngle(up, angle); // triangle top

          

            if(this.debug){
                
                this.previewVectorPosition(a, new THREE.Color('blue'));
                this.previewVectorPosition(b, new THREE.Color('blue'));
                this.previewVectorPosition(c, new THREE.Color('red'));
        
                route = this.previewRoute(this.createRoute(a,b,c));
            }else{
                route = this.createRoute(a,b,c);
            }

        }

        this.previous.position.copy( route[ route.length - 1 ]);
        this.previous.target.copy( targetEndPosition );

        this.animateRoute(route, targetEndPosition);
    }

    /*
        route - either vector3 OR array of vector3
        targetEndPosition - camera TARGET position at the end of animation
        params - optional
    */
    protected async animateRoute(route, targetEndPosition, params: any = {}){

        if(this.busy){
            await this.stopAnimations();
        }

        let id = ++this.animationId; // animation ID

        this.busy = true;

        if( ! isArray(route) ){
            let copy = route;
            route = [copy];
        }

        let scope = this;
        let duration = params.duration || 1000;
        let durationPerStep = Math.floor( duration / route.length );
        let index = 0;

        let enableControlsOnCompletion = true;
        if(params.hasOwnProperty('enabled') && typeof params.enabled == 'boolean'){
            enableControlsOnCompletion = params.enabled;
        }

        this.current.position.copy( this.camera.position );
        this.current.target.copy( this.controls.target );

        animate();
        animateTarget(targetEndPosition, duration);
        
        function animate(){

            let _cameraPos = route[index];

            scope.animations.position = new TWEEN.Tween(scope.current.position)
                .to(_cameraPos, durationPerStep)
                .easing(TWEEN.Easing.Linear.None)
                .onStart( ()=>{

                })
                .onUpdate(() => {
                    scope.camera.position.copy(scope.current.position);
                    scope.controls.update();
                })
                .onComplete(() => {

                    if(scope.forceStop) return;
                    if(id != scope.animationId) return;

                    if(index < route.length){
                        index += 1;
                        animate();
                    }else{
                        if(params.callback) params.callback();
                    }
                   
                });

            if( ! scope.forceStop ){
                scope.animations.position.start()
            }

        }

        function animateTarget(_targetEndPosition, _duration){

            scope.animations.target = new TWEEN.Tween(scope.current.target)
                .to(_targetEndPosition, _duration)
                .easing(TWEEN.Easing.Cubic.Out)
                .onStart(()=>{
                    scope.controls.enableZoom = false;
                })
                .onUpdate(() => {
                    scope.controls.target.copy(scope.current.target);
                    scope.controls.update();			
                })
                .onComplete(()=>{

                    scope.controls.target.copy( scope.current.target );
                    scope.controls.saveState();
                    scope.controls.update();

                    if(enableControlsOnCompletion){
                        scope.controls.enableZoom = true;
                        scope.controls.enabled = true;
                    }

                    scope.busy = false;

                });

            if( ! scope.forceStop ){
                scope.animations.target.start()
            }

        }
    }

    protected createRoute(a,b,c){

        const curve = new THREE.QuadraticBezierCurve3(a,c,b); // a - start, b - end, c - triangle top / middle point in theory

        const points = curve.getPoints( 39 ); // +1 = 40

        return points;
    }


    // protected previewRoute(a,b,c){
    protected previewRoute(points){

        // const points = this.createRoute(a,b,c);
        const geometry = new THREE.BufferGeometry().setFromPoints( points );
        const material = new THREE.LineBasicMaterial( { color : new THREE.Color('green') } );
        
        // Create the final object to add to the scene
        const curveObject = new THREE.Line( geometry, material );

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, curveObject);

        return points;
    }

    protected previewVectorPosition(position, color){

        var geometry2 = new THREE.SphereGeometry( 1, 10, 10 );
        var material2 = new THREE.MeshBasicMaterial( {color: color} );
        var sphere = new THREE.Mesh( geometry2, material2 );
        sphere.position.copy(position);
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, sphere);


        return sphere;
    }

    protected async getModelSize(){

        this.modelDimensions = await Root.instance.events.call(EVENT_GET_MODEL_DIMENSIONS, null, true);

    }

    protected async stopAnimations(){

        this.forceStop = true;

        for(let name in this.animations){
            try{
                this.animations[name].pause();
                this.animations[name] = undefined;
            }catch(e){

            }
        }

        this.forceStop = false;

        return new Promise(resolve => resolve());
    }

}