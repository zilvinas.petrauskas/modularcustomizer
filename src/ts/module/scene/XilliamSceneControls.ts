import { SceneControls } from './SceneControls';

export class XilliamSceneControls extends SceneControls{


    constructor(){
        super();
    }

    protected getControlsConfig() : any{
        return {
            minDistance: 50,
			maxDistance: 2000,
			minPolarAngle: Math.PI * 0,
			maxPolarAngle: Math.PI * 1,
            enableZoom: true,
            enablePan: false,
            enableDamping: true,
            dampingFactor: 0.1,
            rotateSpeed: 0.45,
            autoRotate: false,
            autoRotateSpeed: 0.15,
        }
    }
}