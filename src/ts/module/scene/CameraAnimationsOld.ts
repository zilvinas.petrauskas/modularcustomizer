import { Root } from 'Root';
import * as THREE from 'three';
import { EVENT_GET_PIN_POSITION, EVENT_CUSTOMIZER_READY, EVENT_GET_THREE_CAMERA, EVENT_ADD_TO_SCENE, EVENT_REMOVE_FROM_SCENE, EVENT_MODEL_READY, EVENT_GET_MODEL_DIMENSIONS, EVENT_GET_CAMERA_INITIAL_POSITION, EVENT_GET_CONTROLS, EVENT_GET_THREE_RENDERER, EVENT_ADD_TO_RENDERER, EVENT_CAMERA_ANIMATION, EVENT_CAMERA_RESET_ANIMATION, TRIGGER_MANUAL_CONTROLS_UPDATE } from 'helpers/constants';
import { getPointInBetweenByPerc, getPointInBetweenByValue } from 'helpers/helpers';

const TWEEN = require('tween.js');
declare const TweenMax;
declare const TimelineMax;

export class CameraAnimations{

    protected config : any = {
        zoomDistance : 40,
    }

    protected test: any = {};
    protected camera : any;
    protected controls: any;

    protected boundingHorizontalCircle: any;
    protected verticalboundingHorizontalCircle: any;

    private modelDimensions : any;

    private current : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }

    private defaults : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }
    
    private  animations : any = {};

    private busy : boolean = false;

    constructor(){

        this.listen();
    }

    protected listen(){

        Root.instance.events.listen(EVENT_MODEL_READY, this.getModelSize.bind(this));
        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.prepTesting.bind(this));

        Root.instance.events.register(EVENT_CAMERA_ANIMATION, this.runCameraAnimation.bind(this));
        Root.instance.events.register(EVENT_CAMERA_RESET_ANIMATION, this.resetCameraPosition.bind(this));
    }


 
    protected async getModelSize(){

        this.modelDimensions = await Root.instance.events.call(EVENT_GET_MODEL_DIMENSIONS, null, true);

    }

    // v3
    protected async prepTesting_v3(){

        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true);
        this.controls = await Root.instance.events.call(EVENT_GET_CONTROLS, null, true);

        this.defaults.position.copy(this.camera.position);
        this.defaults.target.copy(this.controls.target);

        let elements = document.querySelectorAll('.customizer-pins--color-item');

        for(let i=0;i<elements.length;i++){
            elements[i].addEventListener('click', async (event: any)=>{

                let key = event.target.dataset.key;

                let endPosition = await Root.instance.events.call(EVENT_GET_PIN_POSITION, null, true, key) as any;
                if(endPosition == undefined){
                    console.log('Pin position not found', key, endPosition);
                    return;
                }

                let endTarget = new THREE.Vector3();
                this.debugMovement(endPosition, endTarget);
            })
        }

    }

    // v2
    protected async prepTesting(){

        
        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true);
        this.controls = await Root.instance.events.call(EVENT_GET_CONTROLS, null, true);

        this.defaults.position.copy(this.camera.position);
        this.defaults.target.copy(this.controls.target);

        // triggers tween udpate together with renderer update
        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'tween', ()=>{
            TWEEN.update();
        });

        let elements = document.querySelectorAll('.customizer-pins--color-item');

        for(let i=0;i<elements.length;i++){
            elements[i].addEventListener('click', async (event: any)=>{

                let key = event.target.dataset.key;

                let endPosition = await Root.instance.events.call(EVENT_GET_PIN_POSITION, null, true, key) as any;
                if(endPosition == undefined){
                    console.log('Pin position not found', key, endPosition);
                    return;
                }

                let endTarget = new THREE.Vector3();
                this.debugMovement(endPosition, endTarget);
            })
        }

    }

    protected async prepTesting_v1(){

        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true);
        this.controls = await Root.instance.events.call(EVENT_GET_CONTROLS, null, true);

        let elements = document.querySelectorAll('.customizer-pins--color-item');

        for(let i=0;i<elements.length;i++){
            elements[i].addEventListener('click', async (event: any)=>{

                let key = event.target.dataset.key;

                let endPosition = await Root.instance.events.call(EVENT_GET_PIN_POSITION, null, true, key) as any;
                if(endPosition == undefined){
                    console.log('Pin position not found', key, endPosition);
                    return;
                }

                let endTarget = new THREE.Vector3();
                this.debugMovement(endPosition, endTarget);
            })
        }

        this.boundingHorizontalCircle = this.createHorizontalDebugCircle();
        this.verticalboundingHorizontalCircle = this.createVerticalDebugCircle();

        // for debug only
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, this.boundingHorizontalCircle);
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, this.verticalboundingHorizontalCircle);

    }

    protected getMaximumDimensionOnModel(){

        let vars = ['x','y','z'];
        let max = 0;
        for(let i in vars){
            let pos = vars[i];
            if(this.modelDimensions.size[pos] > max)
                max = this.modelDimensions[pos];
            
        }

        return max;
    }

    protected createHorizontalDebugCircle(){

        console.log('modelDimensions', this.modelDimensions);

        let size = this.modelDimensions.size.x * 1.1; // needs to be bigger than model itself
        let circleSize = Math.floor(size / 2); // for circle its from side to center, so 50% of total size 
        

        var geometry = new THREE.CircleGeometry( circleSize, 8 );
        var material = new THREE.MeshBasicMaterial( { color: new THREE.Color('yellow'), wireframe: true } );
        var circle = new THREE.Mesh( geometry, material );
        circle.rotateX(THREE.Math.degToRad(90));
        circle.position.copy( this.modelDimensions.center );

        // apply rotation to vertices
        circle.updateMatrix();
        circle.geometry.applyMatrix(circle.matrix);
        circle.matrix.identity();

        circle.rotateX(THREE.Math.degToRad(90));
        
        // exclude side vertices - index of 1 and 6
        // @ts-ignore
        // this.debugChosenPoints(circle.geometry.vertices[1], 0x00ff00);
        // @ts-ignore
        // this.debugChosenPoints(circle.geometry.vertices[6], 0x0000ff);

        return circle;
    }

    protected createVerticalDebugCircle(){

        let size = this.modelDimensions.size.y * 1.1; // needs to be bigger than model itself
        let circleSize = Math.floor(size / 2); // for circle its from side to center, so 50% of total size 

        var geometry = new THREE.CircleGeometry( circleSize, 8 );
        var material = new THREE.MeshBasicMaterial( { color: new THREE.Color('green'), wireframe: true } );
        var circle = new THREE.Mesh( geometry, material );
        circle.position.copy( this.modelDimensions.center );
        circle.updateMatrix();
        circle.geometry.applyMatrix(circle.matrix);

        return circle;
    }

    protected findClosestPositionToVertices(position, vertices){

        let minDist = Infinity;
        let zero = new THREE.Vector3(0,0,0);
        let middlePoint = new THREE.Vector3();

        let ignore = [];
        ignore.push(0);
        ignore.push(1);
        ignore.push( Math.floor(vertices.length / 2) + 1);

        for(let i=0;i<vertices.length;i++){

            if(ignore.includes(i)) continue;
            // if(vertices[i].equals(zero)) continue;
        
            let dist = position.distanceTo(vertices[i]);
            if(dist < minDist){
                minDist = dist;
                middlePoint.copy(vertices[i]);
            }
        }

        return middlePoint;
    }

    protected debugChosenPoints(position, color){

        var geometry2 = new THREE.SphereGeometry( 5, 10, 10 );
        var material2 = new THREE.MeshBasicMaterial( {color: color} );
        var sphere = new THREE.Mesh( geometry2, material2 );
        sphere.position.copy(position);
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, sphere);


        return sphere;
    }

    // TODO - fix busy bug, this gets triggered multiple times thru UI
    protected runCameraAnimation(...args){

        if( this.busy ){
            console.log('BUSY!!!', ...args);
            return;
        }

        this.busy = true;

        this.cameraMovement(...args);
    }

    protected async resetCameraPosition(){

        let endCameraPos = new THREE.Vector3().copy( this.defaults.position );
        let endTarget = new THREE.Vector3().copy( this.defaults.target );

        this.runCameraAnimation(endCameraPos, endTarget);

    }

    /*
        1. calc distance between camera and pin (or between 2 pins)
        2. calc angle 
        3. draw square and use its 2 corners for drawing cubic bezier curve
        4. get "amount of points" from bezier curve and use them for camera path
        5. trigger animation itself
    */
    protected async debugMovement(endPosition, endTarget){

        // debug
        if(this.test){
            for(let key in this.test){
                Root.instance.events.call(EVENT_REMOVE_FROM_SCENE, null, null, this.test[key]);
                this.test[key] = undefined;
            }
            
        }

        // 1.
        
        // let position0 = await Root.instance.events.call(EVENT_GET_CAMERA_INITIAL_POSITION, null, true) as THREE.Vector3;
        // let position1 = new THREE.Vector3().copy(endPosition);

        // console.log('~ pos0', position0);
        // console.log('~ pos1', position1);

        // const distance = position0.distanceTo(position1);
        // const r = distance / 2; // circle radius
        // console.log('~ DIST', distance, r);

        // let middlePoint = getPointInBetweenByPerc(position0, position1, 0.5);

        // // 2
        // const angle = position0.angleTo(position1);

        // console.log('~ ANGLE', angle);

        // const distVector = new THREE.Vector3(0, 0, -r);

        // var squareCorners = [];
        // squareCorners.push(position0);
        // squareCorners.push(position1);
        // squareCorners.push(position1.add(distVector));
        // squareCorners.push(position0.add(distVector));



        // let endCameraPos = getPointInBetweenByPerc(position0, position1, 0.5);
        // let endCameraPos = getPointInBetweenByValue(position0, position1, this.config.zoomDistance);

        // this.cameraMovement(endCameraPos, endTarget);
        this.cameraMovement(endPosition, endTarget);
        


        

    }

    protected async debugMovement_v1(endPosition, endTarget){

        // TODO - create curved line from camera current position to end position
        // TODO - transition from current target to endTarget
        if(this.test){
            for(let key in this.test){
                Root.instance.events.call(EVENT_REMOVE_FROM_SCENE, null, null, this.test[key]);
                this.test[key] = undefined;
            }
            
        }

       
        let a = this.findClosestPositionToVertices(endPosition, this.boundingHorizontalCircle.geometry.vertices);
        let b = this.findClosestPositionToVertices(endPosition, this.verticalboundingHorizontalCircle.geometry.vertices);
        let c = a.add(b).multiplyScalar(0.5); // middle point

        console.log('a', a);
        console.log('b', b);
        console.log('c', c);

        // debug position
        this.test.a = this.debugChosenPoints(a, new THREE.Color('yellow'));
        this.test.b = this.debugChosenPoints(b, new THREE.Color('green'));
        this.test.c = this.debugChosenPoints(c, new THREE.Color('blue'));

        var curve = new THREE.QuadraticBezierCurve3(
            this.camera.position,
            c,
            endPosition,
        );

       
        
        // draw line
        var points = curve.getPoints( 50 );
        var geometry = new THREE.BufferGeometry().setFromPoints( points );
        
        var material = new THREE.LineBasicMaterial( { color : 0xff0000 } );
        
        // Create the final object to add to the scene
        var curveObject = new THREE.Line( geometry, material );

        this.test.curve = curveObject;

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, curveObject);


        let cameraInitialPos = await Root.instance.events.call(EVENT_GET_CAMERA_INITIAL_POSITION, null, true);
        let cameraEndPosition = getPointInBetweenByPerc(cameraInitialPos, endPosition, 0.8); // 80% in of initial camera position

        // TODO - end position should stop before the pin (in distance)
        this.cameraMovement(cameraEndPosition, endTarget, points);
    }
    

    // TODO - if cameraMovement gets overwritten (event gets called before end) - cancel previous animation and continue to another point from currentPosition + currentTarget
    protected cameraMovement(...args){
        
        console.log('~ cameraMovement', ...args);

        const scope = this;
        const endPosition = args[0]; // camera end position
        const endTarget = args[1]; // target position
        const params = args[2] || {};
        // const path = args[2]; // if path is defined, follow it, if not, just go straight line

        // this.debugChosenPoints(endPosition, new THREE.Color('red'));
        // this.debugChosenPoints(endTarget, new THREE.Color('green'));
        
        // console.log('camera movement - cam pos', endPosition);
        // console.log('camera movement - target', endTarget);

        let enableControlsOnCompletion = true;
        if(params.hasOwnProperty('enabled') && typeof params.enabled == 'boolean'){
            enableControlsOnCompletion = params.enabled;
        }

        this.current.position.copy( this.camera.position );
        this.current.target.copy( this.controls.target );

        this.controls.saveState();
        // prep animation

        let duration = 1000; // ms

        this.controls.enabled = false;

        // TODO - proper bullet proof animation...

        this.animations.target = new TWEEN.Tween(this.current.target)
			.to(endTarget, duration)
            .easing(TWEEN.Easing.Cubic.InOut)
            .onStart(()=>{
                scope.controls.enableZoom = false;
            })
			.onUpdate(() => {
                scope.controls.target.copy(scope.current.target);
                scope.controls.update();			
            })
            .onComplete(()=>{

                scope.controls.target.copy( scope.current.target );
                scope.controls.saveState();
                scope.controls.update();

                if(enableControlsOnCompletion){
                    scope.controls.enableZoom = true;
                    scope.controls.enabled = true;
                }

                scope.busy = false;

            })
            .start();	
            
        this.animations.position = new TWEEN.Tween(this.current.position)
            .to(endPosition, duration)
            .easing(TWEEN.Easing.Cubic.InOut)
            .onStart( ()=>{
                
            })
            .onUpdate(() => {
                scope.camera.position.copy(scope.current.position);
            })
            .start()
            .onComplete(() => {
                Root.instance.events.call(TRIGGER_MANUAL_CONTROLS_UPDATE, null, null);
            });
        
    }

    private TODO_TWEENMAX_ANIMATION(){

        /*
         // camera position animation
         const tl = new TimelineMax({
            paused: true,
            onStart: ()=>{

            },
            onComplete: ()=>{

            }
        });

        // camera position anim
        tl.add(TweenMax.fromTo(
            this.camera,
            duration,
            {
                position: this.camera.position,
            },
            {
                position: endPosition,
            }
        ));

        // target anim
        tl.add(TweenMax.fromTo(
            this.controls,
            duration,
            {
                target: this.controls.target,
            },
            {
                target: endTarget
            }
        ));


         this.animations.timeline = tl;
         
         tl.play();

         */
    }


    



}


