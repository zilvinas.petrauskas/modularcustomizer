import { Root } from 'Root';
import * as THREE from 'three';
import { EVENT_CUSTOMIZER_READY, EVENT_GET_THREE_CAMERA, EVENT_ADD_TO_SCENE, EVENT_MODEL_READY, EVENT_GET_MODEL_DIMENSIONS, EVENT_GET_CONTROLS, EVENT_ADD_TO_RENDERER, EVENT_CAMERA_ANIMATION, EVENT_CAMERA_RESET_ANIMATION, TRIGGER_MANUAL_CONTROLS_UPDATE } from 'helpers/constants';
const TWEEN = require('tween.js');

export class CameraAnimations{

    

    protected test: any = {};
    protected camera : any;
    protected controls: any;

    protected boundingHorizontalCircle: any;
    protected verticalboundingHorizontalCircle: any;

    private modelDimensions : any;

    private current : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }

    private defaults : any = {
        position: new THREE.Vector3(), // camera pos
        target: new THREE.Vector3(), // controls/camera target
    }
    
    private  animations : any = {};

    private busy : boolean = false;

    constructor(){

        this.listen();
    }

    protected listen(){

        Root.instance.events.listen(EVENT_MODEL_READY, this.getModelSize.bind(this));
        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.prepare.bind(this));

        Root.instance.events.register(EVENT_CAMERA_ANIMATION, this.runCameraAnimation.bind(this));
        Root.instance.events.register(EVENT_CAMERA_RESET_ANIMATION, this.resetCameraPosition.bind(this));
    }


 
    protected async getModelSize(){

        this.modelDimensions = await Root.instance.events.call(EVENT_GET_MODEL_DIMENSIONS, null, true);

    }

    protected async prepare(){

        
        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true);
        this.controls = await Root.instance.events.call(EVENT_GET_CONTROLS, null, true);

        this.defaults.position.copy(this.camera.position);
        this.defaults.target.copy(this.controls.target);

        // triggers tween udpate together with renderer update
        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'tween', ()=>{
            TWEEN.update();
        });

    }

    protected getMaximumDimensionOnModel(){

        let vars = ['x','y','z'];
        let max = 0;
        for(let i in vars){
            let pos = vars[i];
            if(this.modelDimensions.size[pos] > max)
                max = this.modelDimensions[pos];
            
        }

        return max;
    }

    protected debugChosenPoints(position, color){

        var geometry2 = new THREE.SphereGeometry( 5, 10, 10 );
        var material2 = new THREE.MeshBasicMaterial( {color: color} );
        var sphere = new THREE.Mesh( geometry2, material2 );
        sphere.position.copy(position);
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, sphere);


        return sphere;
    }

    protected async stopAnimations(){

        for(let name in this.animations){
            try{
                this.animations[name].pause();
                this.animations[name] = undefined;
            }catch(e){

            }
        }

        return new Promise(resolve => resolve());
    }

    // TODO - fix busy bug, this gets triggered multiple times thru UI
    protected async runCameraAnimation(...args){

        if( this.busy ){
            console.log('BUSY!!!', ...args);
            await this.stopAnimations();
        }

        this.busy = true;
        this.cameraMovement(...args);
    }
    

    protected async resetCameraPosition(){

        let endCameraPos = new THREE.Vector3().copy( this.defaults.position );
        let endTarget = new THREE.Vector3().copy( this.defaults.target );

        this.runCameraAnimation(endCameraPos, endTarget);

    }
    
    

    // TODO - if cameraMovement gets overwritten (event gets called before end) - cancel previous animation and continue to another point from currentPosition + currentTarget
    protected cameraMovement(...args){
        
        console.log('~ cameraMovement', ...args);

        const scope = this;
        const endPosition = args[0]; // camera end position
        const endTarget = args[1]; // target position
        const params = args[2] || {};

        let enableControlsOnCompletion = true;
        if(params.hasOwnProperty('enabled') && typeof params.enabled == 'boolean'){
            enableControlsOnCompletion = params.enabled;
        }
        this.current.position.copy( this.camera.position );
        this.current.target.copy( this.controls.target );

        this.controls.saveState();
        // prep animation

        let duration = 1000; // ms

        this.controls.enabled = false;

        // TODO - proper bullet proof animation...

        this.animations.target = new TWEEN.Tween(this.current.target)
			.to(endTarget, duration)
            .easing(TWEEN.Easing.Cubic.InOut)
            .onStart(()=>{
                scope.controls.enableZoom = false;
            })
			.onUpdate(() => {
                scope.controls.target.copy(scope.current.target);
                scope.controls.update();			
            })
            .onComplete(()=>{

                scope.controls.target.copy( scope.current.target );
                scope.controls.saveState();
                scope.controls.update();

                if(enableControlsOnCompletion){
                    scope.controls.enableZoom = true;
                    scope.controls.enabled = true;
                }

                scope.busy = false;

            })
            .start();	
            
        this.animations.position = new TWEEN.Tween(this.current.position)
            .to(endPosition, duration)
            .easing(TWEEN.Easing.Cubic.InOut)
            .onStart( ()=>{
                
            })
            .onUpdate(() => {
                scope.camera.position.copy(scope.current.position);
            })
            .start()
            .onComplete(() => {
                Root.instance.events.call(TRIGGER_MANUAL_CONTROLS_UPDATE, null, null);
            });
        
    }



}


