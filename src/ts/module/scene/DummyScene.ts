import { DefaultScene } from './DefaultScene';
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_ADD_TO_RENDERER } from 'helpers/constants';

export class DummyScene extends DefaultScene{

    public container;

    constructor(){
        super();


        this.init();
    }

    protected init(){
        // add some extra stuff

        /// create a dummy block
        this.createDummyBox();
       
        this.animate();
    }

    private createDummyBox(){

        var geometry = new THREE.BoxBufferGeometry( 200, 200, 200 );
        var material = new THREE.MeshBasicMaterial({
            color: new THREE.Color('white'),
            wireframe: true,
        });
        var mesh = new THREE.Mesh( geometry, material );

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, mesh); // adds mesh to scene

        let clock = new THREE.Clock();

        let rotateDummyBox = () => {
            var delta = clock.getDelta() * 0.5;
				mesh.rotation.x += delta;
				mesh.rotation.y += delta;
        }

        Root.instance.events.call(EVENT_ADD_TO_RENDERER, null, null, 'rotate-box', rotateDummyBox); // triggers rotation fn on very tick in renderer
    }

    protected createCanvasContainer(){

        var container = document.getElementById('customizerCanvasWrapper'); // TODO - should be adjustable thru config if needed
        return container;
    }

}