
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_REMOVE_FROM_SCENE, EVENT_GET_THREE_SCENE, EVENT_GET_THREE_RENDERER, EVENT_GET_THREE_CAMERA, EVENT_ADD_TO_RENDERER, EVENT_GET_THREE_CANVAS, EVENT_GET_GROUND_CONFIG, EVENT_WINDOW_RESIZE, EVENT_TRIGGER_RENDER, EVENT_GET_CANVAS_SIZE, EVENT_GET_UI_SIZE, EVENT_CUSTOMIZER_SCENE_READY } from 'helpers/constants';
import * as THREEGL from 'three/examples/jsm/WebGL';
import { isNumber } from 'util';

export class DefaultScene{

    protected scene;
    protected camera;
    protected renderer;
    protected container;
    protected canvas;
    protected renderQueue = {};

    protected rendererRunning = false;
    protected loop;

    protected parentContainer : HTMLElement;

    private websiteConfig: any;

    protected groundConfig = {
        y: -220,
    }


    constructor(){

       this.prepare();
       this.afterPrepare();
    }

    protected async prepare(){

        this.websiteConfig = Root.instance.config.website.get();

        this.parentContainer = this.assignParentContainer();
        this.container = this.createCanvasContainer();
        this.canvas = this.createThreeCanvas();
        this.scene = this.createScene();
        this.renderer = await this.createRenderer();
        this.camera = await this.createCamera();
       
        this.listeners();

        this.eventListeners();

        Root.instance.events.listen(EVENT_WINDOW_RESIZE, this.onWindowResize.bind(this) );

        // DO NOT TRIGGER this.init HERE, should be triggered manually or in extended classes' constructor

        this.render();


        Root.instance.events.broadcast(EVENT_CUSTOMIZER_SCENE_READY, null);
    }

    // some extra functions to include
    protected afterPrepare(){}


    protected eventListeners(){

        window.addEventListener( 'resize', this.onWindowResize.bind(this), false );
    }

    protected assignParentContainer(){

        const elemQuery = this.websiteConfig.hasOwnProperty('customizerCanvasParent') ? this.websiteConfig.customizerCanvasParent : '.customizerCanvasParent';
        return document.querySelectorAll(elemQuery)[0] as HTMLElement;

    }

    protected init(){

         this.startRenderer();
    }

    protected animate(){

        this.loop = this.renderer.setAnimationLoop(this.render.bind(this));

    }

    // thru event
    protected triggerRender(){
        this.render();
    }

    private render(){

        this.renderQueued();
        this.renderer.render( this.scene, this.camera );
    }

    protected listeners(){

        Root.instance.events.register(EVENT_ADD_TO_SCENE, this.addToScene.bind(this) );
        Root.instance.events.register(EVENT_REMOVE_FROM_SCENE, this.removeFromScene.bind(this) );
        Root.instance.events.register(EVENT_GET_THREE_SCENE, this.getScene.bind(this) );
        Root.instance.events.register(EVENT_GET_THREE_CAMERA, this.getCamera.bind(this) );
        Root.instance.events.register(EVENT_GET_THREE_RENDERER, this.getRenderer.bind(this) );
        Root.instance.events.register(EVENT_GET_THREE_CANVAS, this.getCanvas.bind(this) );
        Root.instance.events.register(EVENT_ADD_TO_RENDERER, this.addToRenderQueue.bind(this) );
        Root.instance.events.register(EVENT_GET_GROUND_CONFIG, this.getGroundConfig.bind(this) );
        Root.instance.events.register(EVENT_GET_CANVAS_SIZE, this.getCanvasSizes.bind(this));

        Root.instance.events.listen(EVENT_TRIGGER_RENDER, this.triggerRender.bind(this) );

    }   

    private getGroundConfig(){
        return this.groundConfig;
    }

    private renderQueued(){
        for(let name in this.renderQueue){
            this.renderQueue[name].fn();
        }
    }

    // add additional functions to renderer using this + events
    protected addToRenderQueue(...args){

        let name = args[0];
        let fn = args[1];

        if( ! this.renderQueue.hasOwnProperty(name) ){
            this.renderQueue[name] = {
                fn : fn
            }
        }

    }

    protected addToScene(...args){

        for(let i=0;i<args.length;i++){
            this.scene.add(args[i]);
        }
    }

    protected removeFromScene(...args){
        let mesh = args[0];
        if(mesh == undefined) return;

        mesh.geometry.dispose();
        mesh.material.dispose();
        this.scene.remove(mesh);
    }

     

    protected getScene(){
        return this.scene;
    }

    protected getCamera(){
        return this.camera;
    }

    protected getRenderer(){
        return this.renderer;
    }

    protected getCanvas(){
        return this.canvas;
    }


    protected createCanvasContainer(){

        const container = document.getElementById('customizerCanvasWrapper'); // TODO - thru config
        return container;
        
    }

    protected createThreeCanvas(){

        const canvas = document.createElement('canvas');
       
        // transparent BG - fix black bg on ios + ipad + safari
        const gl = canvas.getContext("webgl", { premultipliedAlpha: false });
        gl.clearColor(1, 1, 0, 0);
        gl.clear(gl.COLOR_BUFFER_BIT);

        this.container.appendChild(canvas);

        return canvas;
    }

    // TODO - websiteConfig.lowersCanvasHeight
    protected async getCanvasSizes(){

        let width = window.innerWidth;
        let height = window.innerHeight;
        let original = {
            width:  window.innerWidth,
            height: window.innerHeight
        }

        let desktopSize = this.parentContainer.dataset.desktopSize;
        let mobileSize = this.parentContainer.dataset.mobileSize;
        let uiSize = {
            width: 0,
            height: 0,
        }

        if(window.innerWidth > 768){

            // for UI_v3 we need UI to stay fixed size with canvas size being adaptive
            if(desktopSize == 'calc'){ // calculate by subtracting UI width from 100vw

                if(Root.instance.events.exists(EVENT_GET_UI_SIZE)){
                    uiSize = await Root.instance.events.call(EVENT_GET_UI_SIZE, null, true) as any;
                }

                width = window.innerWidth - uiSize.width;
                if(width < 1) width = original.width;

            }else if(isNumber(parseFloat(desktopSize))){ // vw * number

                width = window.innerWidth * parseFloat(desktopSize);
                

            }else{ // calc using parent width?
                
            }

        }else{

            // for now mobile size is always 100vw * ratio
            width = window.innerWidth * parseFloat(mobileSize);

        }

        // if(window.innerWidth > 768 && this.parentContainer.dataset.desktopSize){
        //     width = window.innerWidth * parseFloat(this.parentContainer.dataset.desktopSize);
        // }else if(this.parentContainer.dataset.mobileSize){
        //     width = window.innerWidth * parseFloat(this.parentContainer.dataset.mobileSize);
        // }

        return {
            width: width,
            height: height
        }
    }

    protected lowersCanvasHeight(){



    }

    protected async createRenderer(){
        
        const rendererParams = <any>{
            antialias: true,
            canvas: this.getCanvas(),
        }

        if ( THREEGL.WEBGL.isWebGL2Available() ){
            rendererParams.context = this.getCanvas().getContext( 'webgl2', { alpha: false } );
        }else{
            rendererParams.context = this.getCanvas().getContext('experimental-webgl2');
        }

        const size = await this.getCanvasSizes();
        rendererParams.canvas.width = size.width;
        rendererParams.canvas.height = size.height;

        const renderer = new THREE.WebGLRenderer(rendererParams);
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( size.width, size.height );
        renderer.gammaOutput = true;
        renderer.gammaFactor = 1.2;

        renderer.context.canvas.addEventListener('webglcontextlost', this.webglContextError);

        return renderer;
    }

    protected startRenderer(){

		if(this.rendererRunning){
			return; // already running
		}

		this.rendererRunning = true;
		this.animate();
    }

    protected stopRenderer(){
        
        if( ! this.rendererRunning) return; // already paused

		cancelAnimationFrame(this.loop);
		this.rendererRunning = false;
    }

    protected webglContextError(event: Event){
        event.preventDefault();
        console.warn('CRITICAL ERROR', event);
        alert('Unfortunately WebGL has crashed. Please reload the page to continue!');
    }

    protected createScene(){
        return new THREE.Scene();
    }

    protected async createCamera(){
        
        const cameraInitialPosition = new THREE.Vector3(0,75,700);

        const size = await this.getCanvasSizes();

        const camera = new THREE.PerspectiveCamera( 30, size.width / size.height, 1, 10000 );
        camera.position.copy(cameraInitialPosition);

        return camera; //new Promise(resolve => resolve());
    }


    private async onWindowResize(...args) {

        const forcedWidth = args[0], 
              forcedHeight = args[1];
        
        let size = await this.getCanvasSizes();

        if(typeof forcedWidth !== 'undefined' && forcedWidth > 0) size.width = forcedWidth;
		if(typeof forcedHeight !== 'undefined' && forcedHeight > 0) size.height = forcedHeight;

        this.canvas.width = size.width;
        this.canvas.height = size.height;
        this.renderer.setSize( size.width, size.height );
        this.camera.aspect = size.width / size.height;
        this.camera.updateProjectionMatrix();
       
    }


}