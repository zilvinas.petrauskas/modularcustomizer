import { DefaultScene } from './DefaultScene';
import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_ADD_TO_RENDERER, EVENT_PRELOADER_FINISHED, EVENT_GET_CAMERA_INITIAL_POSITION } from 'helpers/constants';
import { parseColor } from 'helpers/helpers';

export class Scene extends DefaultScene{

    public container;
    

    constructor(){
        super();
    
        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.init.bind(this));
        Root.instance.events.register(EVENT_GET_CAMERA_INITIAL_POSITION, this.getCameraPosition.bind(this));
    }

    protected init(){

        this.startRenderer();
    }

    protected async createCamera(){
        
        const size = await this.getCanvasSizes();

        const camera = new THREE.PerspectiveCamera( this.getCameraPerspective(), size.width / size.height, this.getCameraNear(), this.getCameraFar() );
        camera.position.copy(this.getCameraPosition());

        return camera;
    }

    protected getCameraPerspective(){
        return 30;
    }

    protected getCameraNear(){
        return 1;
    }

    protected getCameraFar(){
        return 10000;
    }

    protected getCameraPosition(){
        return  new THREE.Vector3(-129,17,339);
    }

    protected createCanvasContainer(){

        var container = document.getElementById('customizerCanvasWrapper'); // TODO - should be adjustable thru config if needed
        return container;
    }

}