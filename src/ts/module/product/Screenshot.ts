import { Root } from 'Root';
import {EVENT_TAKE_SCREENSHOT, EVENT_GET_THREE_CAMERA, EVENT_GET_THREE_RENDERER, EVENT_GET_THREE_SCENE, EVENT_WINDOW_RESIZE} from 'helpers/constants';
import * as THREE from 'three';

export class Screenshot {

    
    constructor() {

        Root.instance.events.register(EVENT_TAKE_SCREENSHOT, this.screenshot.bind(this));

    }

    protected getScreenshotPosition(){
        // const screenshotPosition = new THREE.Vector3(-185, 18, 333); // farer
        const screenshotPosition = new THREE.Vector3(-112, -8, 257); // closer

        return screenshotPosition;
    }


    protected async screenshot(options: any = {}){

        const screenshotPosition = this.getScreenshotPosition();

        let camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.PerspectiveCamera;
        let renderer = await Root.instance.events.call(EVENT_GET_THREE_RENDERER, null, true) as THREE.Renderer;
        let scene = await Root.instance.events.call(EVENT_GET_THREE_SCENE, null, true) as THREE.Scene;

        const desired = {
            width: 400,
            height: 400,
        }

        if(options && options.width > 0) desired.width = options.width;
        if(options && options.height > 0) desired.height = options.height;
        
        const pixelRatio = window.devicePixelRatio || 1;
        const width = Math.floor( desired.width / pixelRatio );
        const height = Math.floor( desired.height / pixelRatio );

        // // save current camera position
        const oldPosition = camera.position.clone();

        // // change camera position for 1 frame
        camera.position.copy(screenshotPosition);
        camera.lookAt(0,0,0);

        // set camera and renderer to desired screenshot dimension
        camera.aspect = width / height;
        camera.updateProjectionMatrix();
        renderer.setSize( width, height );

        // // trigger render on the scene
        renderer.render(scene, camera)

         // save image data
        const imageData = renderer.domElement.toDataURL( 'image/png' );
       
         // once screenshot is captured, restore camera position
         camera.position.copy(oldPosition);
         camera.lookAt(0,0,0);

          // reset to old dimensions by invoking the on window resize function
         Root.instance.events.broadcast(EVENT_WINDOW_RESIZE, null);

         return {
             image: imageData,
             width: width,
             height: height,
         }

    }

}