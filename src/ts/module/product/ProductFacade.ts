import { Product } from './Product';
import { Root } from 'Root';
import { EVENT_COLOR_CHANGE, EVENT_PREPARE_PRODUCT_FACADE } from 'helpers/constants';

/*
    communicates with lower tier modules and acts like a controller for Product.ts
*/


// THIS CLASS A TON OF WORK
export class ProductFacade{

    constructor(){

        Root.instance.events.register(EVENT_PREPARE_PRODUCT_FACADE, this.init.bind(this));
    }

    /*
        - color changes
            -- ui triggers event
            -- facade should catch the event with parameters
            -- facade should call product texture updater for components/product
            -- 

        - custom text changes
            -- ui triggers event
            -- facade catches event with custom text parameters + on which part
            -- facade triggers product evenet to apply custom text
    */

    protected init(){
        
        this.listen();
        

    }

    protected listen(){
        

    }

}