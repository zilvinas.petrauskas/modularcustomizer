import { CalamigosBlanket } from 'component/products/calamigos/blanket/CalamigosBlanket';
import { CalamigosSweater } from 'component/products/calamigos/sweater/CalamigosSweater';
import { CalamigosPillow } from 'component/products/calamigos/pillow/CalamigosPillow';
import { DemoSweater } from 'component/products/calamigos/sweater/DemoSweater';
import { CalamigosScarf } from 'component/products/calamigos/scarf/CalamigosScarf';
import { DemoLevis } from 'component/products/calamigos/sweater/DemoLevis';

/*
    Because dynamic import doesnt work, so need a class like this to hold all possible products for imported models
*/

export async function ModelPicker(_name){

    return new Promise((resolve, reject) => {

        let name = _name.toLowerCase().trim();

        // =========== CALAMIGOS PRODUCTS ===========
        if(name == 'calamigos-sweater') resolve(new CalamigosSweater());
        else if(name == 'demo-sweater') resolve(new DemoSweater());
        else if(name == 'calamigos-pillow') resolve(new CalamigosPillow());
        else if(name == 'calamigos-blanket') resolve(new CalamigosBlanket());
        else if(name == 'calamigos-scarf') resolve(new CalamigosScarf());

        else if(name == 'demo-levis') resolve(new DemoLevis());

        // =========== XILLIAM PRODUCTS ===========
        // else if(name == 'xilliam-sweater') resolve(new CalamigosBlanket());
        // else if(name == 'xilliam-hoodie') resolve(new XilliamHoodie());

        else{
            console.warn('Unknown model with name: ',name, name == 'calamigos-sweater');
            reject();
        }
       

      
    });

}