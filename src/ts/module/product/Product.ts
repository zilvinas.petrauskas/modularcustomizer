import { Root } from 'Root';
import { ProductFacade } from 'module/product/ProductFacade';
import { ModelPicker } from './ModelPicker';
import { detectType } from 'helpers/helpers';
import { EVENT_DYNAMIC_PROPERTY_UPDATE } from 'helpers/constants';


export class Product{

    private productConfig : any;
    protected facade : ProductFacade | any; 
    protected model;

    constructor(){
        
         /*
            1. get product config
            2. preload textures (based of config)
            3. preload product module
        */
      
        this.prepare();
    }

   

    protected async prepare(){

        this.productConfig = Root.instance.config.product.get()

        await this.prepareTextures()
        await this.prepareModel()

    }

    protected async prepareTextures(){

        // types not defined in here, will not be loaded from product's layers
        let preloadTypes = [ // TODO: move out?
            'texture', 
            'normal',
            'stitch',
            'stitch_normal',
            'stitch_mask',
            'stitch_text',
        ];

        /*   PRODUCT CONFIG STRUCTURE
            {
                model: { name, }
                parts: [{ name, group, layers[ {type, value, key?}] }],
                pins: [ {ref[], position{} }],
                ui: [ {key, name, type, value[], unique, different[] } ],
            }
        */
        for(let i=0;i<this.productConfig.parts.length;i++){
            
            let part = this.productConfig.parts[i];

            for(let y=0;y<part.layers.length;y++){

                let layer = part.layers[y];

                if(preloadTypes.includes(layer.type)){
                    Root.instance.preloader.add(layer.key, layer.value, detectType(layer.type), true);
                    // Root.instance.preloader.add('unique-textre-key', 'textures-of-front.ng', )
                }else if(layer.type == 'color' || layer.type == undefined){
                    // doesnt need to be preloaded, but also shouldnt throw a warning
                }else if(layer.type == 'text'){
                    // fine also, nothing to preload
                }else{
                    console.warn('Unknown layer type not allowed! --->', layer.type);
                }

            }
        }

    }


    protected async prepareModel(){

        if( ! this.productConfig.hasOwnProperty('model')){
            console.warn('Product config doesnt contain model information - "model" ', this.productConfig);
            return;
        }

        if( ! this.productConfig.hasOwnProperty('model')) console.warn('Define parameter "model" within product configuration');
        if( ! this.productConfig.model.hasOwnProperty('name')) console.warn('Define parameter "name" for model object within product configuration');

        // load 3d model class
        await ModelPicker(this.productConfig.model.name).catch(onError.bind(this)).then(onSuccess.bind(this));
     
        function onSuccess(_model){
            // this.model = _model; // we wont access this directly, just wanted for it to be created, so we can triggers events

            // console.log('~EXECUTED?', _model);
        }
        function onError(err){
            console.warn('Model by name '+this.productConfig.model.name+' NOT FOUND in ModelPicker');
        }

    }


    

}