import { TYPE_SWEATER } from 'helpers/constants';
import { Root } from 'Root';
import { ProductTypeDetector } from './ProductTypeDetector';

export class XilliamProductTypeDetector extends ProductTypeDetector{

    constructor(){
        super();
    }

    protected detect() : string | null {

        // actual sweaters
        if(this.handle.includes('ccx'))         
            return TYPE_SWEATER;

        // for the demo only
        else if(this.handle.includes('x1') || this.handle.includes('x2') || this.handle.includes('x3') || this.handle.includes('x4'))
            return TYPE_SWEATER;

        else{
            console.warn('Product type not detected!', this.handle);
            return null;
        }                   

    }

}