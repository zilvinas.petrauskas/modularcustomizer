import { Root } from 'Root';
import { parseColor, getColorFeederData, rgbToHex } from 'helpers/helpers';
import { EVENT_REGISTER_DEFAULT_COLOR, EVENT_GET_NEAREST_COLOR, EVENT_GET_COLOR_FEEDER_INFO } from 'helpers/constants';

export class NearestColor {

    private colors: any = {};
    private cache: any = {};

	constructor() {

        this.init();
        this.listen();
    }
    
    protected async init(){

        this.parseColors(getColorFeederData());

    }

    protected listen(){

        Root.instance.events.register(EVENT_REGISTER_DEFAULT_COLOR, this.register.bind(this));
        Root.instance.events.register(EVENT_GET_NEAREST_COLOR, this.find.bind(this));

        Root.instance.events.register(EVENT_GET_COLOR_FEEDER_INFO, this.getColorInfo.bind(this));

    }

    /*
        arguments
        [0] - color name
        [1] - color code
        [2] - color feeder
    */
    protected register(...args){

        const colorName = args[0];
        const colorCode = parseColor(args[1], 'clean');
        const feederNumber = args[2];

        this.colors[colorCode] = {
            name: colorName,
            color: colorCode,
            feeder: feederNumber,
            rgb: this.getRGBvalue(colorCode),
        }

    }

    protected getColorInfo(colorCode){

        if( ! this.colors.hasOwnProperty(colorCode) ){
            let nearest = this.nearest(colorCode, this.colors);
            if(typeof nearest !== 'undefined' && nearest !== null) colorCode = nearest;
        }

        return this.colors[colorCode];
    }

     /*
        arguments
        [0] - color code

    */
    protected find(...args){

        const colorCode = args[0];
        if(this.cacheContains(colorCode)) return this.getFromCache(colorCode);
        let nearest = this.nearest(colorCode, this.colors);

        return nearest;
    }

 

    /*
        needle = color code
        colors = this.colors
    */
	private nearest(needle, colors) {

		needle = this.getRGBvalue(needle);

		if (!needle) {
			return null;
		}

		var distanceSq,
			minDistanceSq = Infinity,
			rgb,
			found;

        for(let code in colors){
            
            rgb = colors[code].rgb;

			distanceSq = (
				Math.pow(needle.r - rgb.r, 2) +
				Math.pow(needle.g - rgb.g, 2) +
				Math.pow(needle.b - rgb.b, 2)
            );
            
			if (distanceSq < minDistanceSq) {
				minDistanceSq = distanceSq;
				found = colors[code];
			}
		}

		return found.color;
    }

    protected getFromCache(code){
        return this.cache[code];
    }

    protected cacheContains(code){
        return this.cache.hasOwnProperty(code);
    }
    
    protected saveIntoCache(code, color){
        this.cache[code] = color; // cache colors that were found once
    }

	private getRGBvalue(source) {

		var red, green, blue;

		if (typeof source === 'object') {
			return source;
		}

		var hexMatch = source.match(/^#?((?:[0-9a-f]{3}){1,2})$/i);
		if (hexMatch) {
			hexMatch = hexMatch[1];

			if (hexMatch.length === 3) {
				hexMatch = [
					hexMatch.charAt(0) + hexMatch.charAt(0),
					hexMatch.charAt(1) + hexMatch.charAt(1),
					hexMatch.charAt(2) + hexMatch.charAt(2)
				];

			} else {
				hexMatch = [
					hexMatch.substring(0, 2),
					hexMatch.substring(2, 4),
					hexMatch.substring(4, 6)
				];
			}

			red = parseInt(hexMatch[0], 16);
			green = parseInt(hexMatch[1], 16);
			blue = parseInt(hexMatch[2], 16);

			return { r: red, g: green, b: blue };
		}
	}

    protected parseColors(feederData){

        for(let key in feederData){
            let row = feederData[key];

            this.register(row.name, row.value, row.feeder);

        }

    }


}