import { TYPE_SWEATER, TYPE_PILLOW, TYPE_BLANKET, EVENT_GET_PRODUCT_TYPE, TYPE_SCARF } from 'helpers/constants';
import { Root } from 'Root';

export class ProductTypeDetector{

    protected handle : string;
    protected type: string | null;

    constructor(){

        this.init();
    }

    protected async init(){
        const handle = Root.instance.config.product.get('handle'); 
        this.handle = handle.toLowerCase();
        this.type = this.detect();
        console.log('PRODUCT TYPE?', this.type);
        this.listen();
    }

    protected detect() : string | null { // allows a return of string OR null

        if(this.handle.includes('cc0'))         
            return TYPE_SWEATER;

        else if(this.handle.includes('ccb'))    
            return TYPE_BLANKET

        else if(this.handle.includes('ccp') || this.handle.includes('pillow'))    
            return TYPE_PILLOW

        else if(this.handle.includes('ccs'))
            return TYPE_SCARF;

        else{
            console.warn('Product type not detected!', this.handle);
            return null;
        }                   

    }

    protected getType(){
        return this.type;
    }

    protected listen(){
        Root.instance.events.register(EVENT_GET_PRODUCT_TYPE, this.getType.bind(this));
    }
}