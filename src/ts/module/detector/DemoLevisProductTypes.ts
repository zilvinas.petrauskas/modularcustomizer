import { TYPE_SWEATER } from 'helpers/constants';
import { Root } from 'Root';
import { ProductTypeDetector } from './ProductTypeDetector';

export class DemoLevisProductTypes extends ProductTypeDetector{

    constructor(){
        super();
    }

    protected detect() : string | null {

        return TYPE_SWEATER;                  
    }

}