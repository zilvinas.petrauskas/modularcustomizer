import * as THREE from 'three';
import { Root } from 'Root';
import { EVENT_ADD_TO_SCENE, EVENT_CUSTOMIZER_READY, EVENT_GET_MODEL, EVENT_GET_THREE_CAMERA, EVENT_GET_CANVAS_SIZE, EVENT_CUSTOMIZER_SCENE_READY, EVENT_GET_CONTROLS } from 'helpers/constants';
import { isDefined, isLocalhost } from 'helpers/helpers';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export class PinsAdmin{
     
    private camera : THREE.Camera;
    private controls : OrbitControls;
    private clock : THREE.Clock;
    private raycaster : THREE.Raycaster;
    private mouse : THREE.Vector2 = new THREE.Vector2();
    private objects : any = [];
    private ready : boolean = false;
    private ticks : number = 0;
    private dot: any;
    private size : any = {
        width: 0,
        height: 0,
    }
    private locked : boolean = false;
    private visible : boolean = false;
    private elements: any = {}; // html elements for gui updater holder
   
    constructor(){

        console.log('PINS admin');

        this.init();
    }

    protected init(){

        this.raycaster = new THREE.Raycaster();
        this.clock = new THREE.Clock();

        this.createDot();
        this.actions();

        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.onReady.bind(this));
    }

    // visual dot on top of product for debugging - ENABLE WITH LETTER " P "
    protected createDot(){

        var geometry = new THREE.SphereGeometry( 1, 16, 16 );
        var material = new THREE.MeshBasicMaterial( {
            color: new THREE.Color('red')
        } );
        let dot = new THREE.Mesh( geometry, material );
        if( ! this.visible) dot.visible = false;

        this.dot = dot;
    }

    // register mouse movement and raycasting
    protected actions(){

      
        document.addEventListener( 'mousemove', this.onDocumentMouseMove.bind(this), false );
        document.addEventListener( 'keyup', this.onKeyDown.bind(this), false );

        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.updateEverything.bind(this));

    }

    protected async getUpdateCanvasSize(){

        this.size = await Root.instance.events.call(EVENT_GET_CANVAS_SIZE, null, true);

        return new Promise(resolve => resolve());
    }

    protected async updateEverything(){

        await this.getUpdateCanvasSize();

    }

    protected async onReady(){

        // if( ! this.enabled){
            // this.visible = false;
            // this.updateVisibility();
        // }

        console.log('create PINS ADMIN');

        await this.getUpdateCanvasSize();

        this.createGui();
        this.addToScene(this.dot);

        await this.getCamera();
        await this.getControls();
        await this.getObjects();
        

    }

    private  async addToScene(mesh) {
        await Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, mesh);
    }

    private async getControls() {

        this.controls =  await Root.instance.events.call(EVENT_GET_CONTROLS, null, true) as OrbitControls;
        this.controls.addEventListener('change', this.onControlsChange.bind(this));

        return new Promise(resolve => resolve());
    }

    private async getCamera() {
        this.camera =  await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.Camera;
        return new Promise(resolve => resolve());
    }

    protected async getObjects(){

        // TODO - get object3d from components so that all meshes are validated

        let productModel = await Root.instance.events.call(EVENT_GET_MODEL, null, true) as THREE.Object3D;
        if( ! isDefined(productModel) ){
            console.warn('Pins admin failed to get product model!', productModel);
            return;
        }

        for(let i=0;i<productModel.children.length;i++){
            this.objects.push( productModel.children[i] );
        }

        this.ready = true;

    }

    private toggleLock(){
        this.locked = ! this.locked;
    }

    private  toggleVisibility() {
        
        this.visible = ! this.visible;
        this.updateVisibility();

    }

    protected onKeyDown(event){

        switch(event.code){
            case 'KeyL': this.toggleLock(); break;
            case 'KeyP': this.toggleVisibility(); break;
        }

    }
    

    protected onDocumentMouseMove(event){

        if( ! this.ready ) return;

        event.preventDefault();

        this.mouse.x = ( event.clientX / this.size.width ) * 2 - 1;
        this.mouse.y = - ( event.clientY / this.size.height ) * 2 + 1;

        this.detect();
    }

    // detect intersection
    protected detect(){

        if(this.locked || !this.visible) return;

        this.ticks += this.clock.getDelta();
        if( this.ticks < 0.02 ) return;

        this.raycaster.setFromCamera( this.mouse, this.camera );

        var intersections = this.raycaster.intersectObjects( this.objects );
        var intersection = ( intersections.length ) > 0 ? intersections[ 0 ] : null;

        if ( intersection !== null ) {

            this.dot.position.copy( intersection.point );
            this.updatePinPos( intersection.point );

            // this.test();

            // console.log('intersection?', intersections.length);

            // this.ticks = 0;
        }

        this.ticks = 0; 
        
    }

  

    protected updateVisibility(){

        if(this.visible){

            this.elements.wrapper.style.display = 'block';
            this.dot.visible = true;

        }else{

            this.elements.wrapper.style.display = 'none';
            this.dot.visible = false;
        }


    }

    protected createGui(){

        let div = document.createElement('div');
        div.setAttribute('class','pins-admin-gui');

        // pin position
        let pinPos = document.createElement('div');
        let pinPosTitle = document.createElement('span');
        pinPosTitle.innerHTML = 'PIN: ';
        let pinPosValue = document.createElement('span');
        pinPos.appendChild(pinPosTitle);
        pinPos.appendChild(pinPosValue);
        div.appendChild(pinPos);

        // camera position
        let cameraPos = document.createElement('div');
        let cameraPosTitle = document.createElement('span');
        cameraPosTitle.innerHTML = 'CAM: ';
        let cameraPosValue = document.createElement('span');
        cameraPos.appendChild(cameraPosTitle);
        cameraPos.appendChild(cameraPosValue);
        div.appendChild(cameraPos);

        // info
        let info = document.createElement('div');
        let infoSmall = document.createElement('small');
        infoSmall.innerHTML = 'Press L to lock pin position';
        infoSmall.style.color = 'gray';
        info.appendChild(infoSmall);
        div.appendChild(info);

        document.getElementsByTagName('body')[0].appendChild(div);

        if( ! this.visible ) div.style.display = 'none';

        this.elements.pin = pinPosValue;
        this.elements.camera = cameraPosValue;
        this.elements.wrapper = div;

    }

    protected onControlsChange(){

        this.updateCameraPosition(this.camera.position);

    }

    protected updateCameraPosition(pos){

        let x = parseInt(pos.x);
        let y = parseInt(pos.y);
        let z = parseInt(pos.z);

        let string = `${x}, ${y}, ${z}`;

        this.elements.camera.innerHTML = string;

    }

    protected updatePinPos(pos){

        // console.log('- update gui', pos);

        let x = parseInt(pos.x);
        let y = parseInt(pos.y);
        let z = parseInt(pos.z);

        let string = `${x}, ${y}, ${z}`;

        this.elements.pin.innerHTML = string;
    }


}