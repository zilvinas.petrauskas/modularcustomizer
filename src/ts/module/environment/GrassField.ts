import { Root } from 'Root';
import { EVENT_PRELOADER_FINISHED, EVENT_ADD_TO_SCENE, TYPE_TEXTURE, EVENT_GET_THREE_RENDERER } from 'helpers/constants';
import * as THREE from 'three';

export class GrassField{
    
    protected textures;
    protected loadedTextures;

    constructor(){

        console.log('- init grass field');
        this.prepare();
        
    }


    public async prepare(){

        this.textures = {};
        this.textures.grass = `grasslight-big_v2.jpg`;

        // queue a textures to load
        Root.instance.preloader.add('grass', this.textures.grass, TYPE_TEXTURE, true);

        // once things loaded - trigger a function -> this.start
        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.start.bind(this));

        this.createSky();
    }

     // TODO - move out to envrionments
     protected async createSky(){

        const renderer = await Root.instance.events.call(EVENT_GET_THREE_RENDERER, null, null) as THREE.WebGLRenderer;
        renderer.setClearColor(0xF6F3EF, 1);

    }

    /*
        args - array
        [0] - object of loaded textures
    */
    protected start(...args){

        console.log('.. grass field args', args);

        this.loadedTextures = args[0];
        /*
            {
                'name' => THREE.Texture(),
                'front_texture' => ...
            }
        */
        /*

        */

        // TODO - if used by other stuff, move it out
        const groundConfig = {
            // y : 220, 
            y: -220,
        }

        
       const groundTexture = this.loadedTextures.grass;
       groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
       groundTexture.repeat.set( 25, 25 );
       groundTexture.anisotropy = 16;

       const groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture } );

       const groundMesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
       groundMesh.position.y = groundConfig.y;
       groundMesh.rotation.x = - Math.PI / 2;
       groundMesh.receiveShadow = true;
       
       Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, groundMesh);
    }

}