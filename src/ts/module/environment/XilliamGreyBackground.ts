import { Root } from 'Root';
import { EVENT_PRELOADER_FINISHED, EVENT_ADD_TO_SCENE, EVENT_TRIGGER_RENDER } from 'helpers/constants';
import * as THREE from 'three';
import { GUI } from 'three/examples/jsm/libs/dat.gui.module';
import { componentToHex, parseColor } from 'helpers/helpers';

const GRADIENT_VERTEXT = require('component/glsl/GradientVertextShader.vert').default;
const GRADIENT_FRAGMENT = require('component/glsl/GradientFragmentShader.frag').default;

export class XilliamGreyBackground{

    protected background;
    protected loadedTextures;

    private mesh;
    private gui;
    private timeout;

    protected params = {
        color1: 0xffffff,
        color2: 0xcccccc, //0xa1a1a1, //0xcccccc, //0xe0e0e0,
    }

    constructor(){

        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.start.bind(this));

        Root.instance.events.register('change_bg_colors', this.change.bind(this));
        Root.instance.events.register('toggle_gui', this.toggle.bind(this));


    }

    protected start(){

        this.gradient();
        this.initGui();

    }

    protected initGui(){
        
        this.toggle(); // hide at start

        let scope = this;
        this.gui = new GUI();

        this.gui.addColor( scope.params, 'color1' ).name('color #1').onChange( function ( val ) {
            scope.change(val, scope.params.color2)
        } );
        this.gui.addColor( scope.params, 'color2' ).name('color #2').onChange( function ( val ) {
            scope.change(scope.params.color1, val)
        } );

        this.gui.open();
    }

    protected createUniforms(color1, color2){

        const uniforms = {
            "color1" : {
              type : "c",
              value : new THREE.Color(color1), // fffdfa, 0xfffbf2
            },
            "color2" : {
              type : "c",
              value : new THREE.Color(color2), // 0xCCCCCC 0xe0e0e0 0xe8e8e8
            },
          };

          return uniforms;
    }

    private createShaderMaterial(uniforms){
        return new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: GRADIENT_VERTEXT,
            fragmentShader: GRADIENT_FRAGMENT,
            side: THREE.DoubleSide,
        });
    }

    protected gradient(){

        let uniforms = this.createUniforms(this.params.color1, this.params.color2);

        const material = this.createShaderMaterial(uniforms);

        const geometry = new THREE.SphereGeometry( 3000, 32, 32 ); 
        this.mesh = new THREE.Mesh( geometry, material );

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, this.mesh);

        console.log('got mesh #1' , this.mesh);
    }

    protected change(...args){

        clearTimeout(this.timeout);

        this.timeout = setTimeout(()=>{
            this.changeMaterial(...args);
        }, 100);

    }

    private changeMaterial(...args){

        const color1 = parseColor(componentToHex(args[0]), 'threejs');
        const color2 = parseColor(componentToHex(args[1]), 'threejs');

        // console.log('change', args, color1, color2);

        let uniforms = this.createUniforms(color1, color2);
        const material = this.createShaderMaterial(uniforms);

        this.mesh.material = material;
        this.mesh.needsUpdate = true;

        console.log('got mesh #2' , this.mesh);

        Root.instance.events.call(EVENT_TRIGGER_RENDER, null, null);
    }

    protected toggle(){

        document.getElementsByTagName('body')[0].classList.toggle('hide-gui');

    }

}
