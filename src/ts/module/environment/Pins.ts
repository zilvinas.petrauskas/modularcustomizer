import { PinsAdmin } from './PinsAdmin';
import { Root } from 'Root';
import { EVENT_CUSTOMIZER_READY, EVENT_GET_THREE_CAMERA, EVENT_GET_CANVAS_SIZE, EVENT_ADD_TO_SCENE, EVENT_AFTER_CONTROLS_UPDATE, EVENT_CHECK_COLOR_DATA,  EVENT_COLOR_CHANGE_COMPLETE, EVENT_GET_PIN_POSITION,  EVENT_MOUSE_PRESS, EVENT_MOUSE_RELEASE, EVENT_CAMERA_ANIMATION,  EVENT_CAMERA_RESET_ANIMATION, EVENT_TRIGGER_ZOOM_IN, EVENT_TRIGGER_ZOOM_OUT,  EVENT_PIN_SELECT } from 'helpers/constants';
import { isDefined, isLocalhost, getPointInBetweenByValue, getPointInBetweenByPerc } from 'helpers/helpers';
import * as THREE from 'three';


const STATE_DEFAULT = 'STATE_DEFAULT';
const STATE_ZOOMING_IN = 'STATE_ZOOMING_IN';
const STATE_ZOOMING_OUT = 'STATE_ZOOMING_OUT';

export class Pins{
    
    protected config : any = {
        zoomDistance : 250,
    }

    private pinsData : any = [];
    private container: HTMLElement;
    private elements: any = {};
    private clock : THREE.Clock;
    private ticks: any = {}

    private camera : THREE.Camera;
    private size : any = {
        width: 0,
        height: 0,
    }

    private mouseTimeout: any = null;
    private isVisible : boolean = true;
    private locked : boolean = false;

    private state : string = STATE_DEFAULT;

    constructor(){

        this.prepare();
    
    }

    protected listen(){

        Root.instance.events.listen(EVENT_MOUSE_PRESS, this.onMousePress.bind(this));
        Root.instance.events.listen(EVENT_MOUSE_RELEASE, this.onMouseRelease.bind(this));

        Root.instance.events.listen(EVENT_TRIGGER_ZOOM_IN, this.triggerZoomIn.bind(this));
        Root.instance.events.listen(EVENT_TRIGGER_ZOOM_OUT, this.triggerZoomOut.bind(this));

        // TODO - add pin hide/show with lock/unlock

    }

    protected onMousePress(){

        if( this.locked ) return;

        if(this.container){

            this.hide();
            clearTimeout(this.mouseTimeout);
           
        }

    }

    protected onMouseRelease(){
        
        if( this.locked ) return;

        this.mouseTimeout = setTimeout(()=>{
            this.show();
        }, 500);

    }


    protected show(){

        if( this.isVisible ) return;

        this.isVisible = true;
        this.container.classList.remove('hide-pins');

    }

    protected hide(){

        if( ! this.isVisible ) return;

        this.isVisible = false;
        this.container.classList.add('hide-pins');

    }

  

    protected getContainers(productConcifg){
        this.container = this.getContainer(productConcifg);
    }
   
    protected async prepare(){

       this.admin();


       const productConcifg = Root.instance.config.product.get();

        this.getContainers(productConcifg);

       // TODO - read config, create pins at config-position, assign to groups, listen for color change events, zoom onto pin

       // TODO - get container from UI config
       
       this.clock = new THREE.Clock();

        this.ticks = { // for performance, dont need to update things every ms
            position : 0,
            color: 0,
        }

       

        this.camera = await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, true) as THREE.Camera;
        
        this.pinsData = this.parseConfig(productConcifg);
        // console.log('PINS DATA', this.pinsData);
        await this.createHtml();

        Root.instance.events.listen(EVENT_CUSTOMIZER_READY, this.initialUpdate.bind(this));
        Root.instance.events.listen(EVENT_COLOR_CHANGE_COMPLETE, this.updateAllPinsColor.bind(this));

        Root.instance.events.register(EVENT_GET_PIN_POSITION, this.getPinPosition.bind(this));

        window.addEventListener('resize', this.resize.bind(this));
    }

    protected initialUpdate(){

        this.listen();

        // update pins' colors, show pins, zoom in

        // update colors
        this.updateAllPinsColor();

        // update pins position
        this.updatePinsPositions(true);
    }

    protected async resize(){

        // console.log('resizing');
       
        this.updatePinsPositions();

    }

    protected async getCanvasSize(){
        let size = await Root.instance.events.call(EVENT_GET_CANVAS_SIZE, null, true);
        return new Promise(resolve =>resolve(size));
    }

    protected parseConfig(config){

        if( ! isDefined(config.ui)) return [];

        const pinsData = [];

        for(let i=0;i<config.ui.length;i++){
            let row = config.ui[i];
            if(row.hasOwnProperty('pins')){
                
                pinsData.push({
                    name: row.name,
                    key: row.key,
                    group: row.group,
                    config: row.pins,
                });

                // pinsData.push({
                //     position: row.pins.position,
                //     name: row.name,
                //     key: row.key,
                //     group: row.group, // in case key is not enough to find a color (or multiple colors)
                // });
            }
        }


        return pinsData;
    }

    protected async toScreenPosition(object){

        let pos = new THREE.Vector3();
        pos = pos.setFromMatrixPosition(object.matrixWorld);
        pos.project(this.camera);

        let size = await this.getCanvasSize() as any;

        let widthHalf = size.width / 2;
        let heightHalf = size.height / 2;

        pos.x = (pos.x * widthHalf) + widthHalf;
        pos.y = - (pos.y * heightHalf) + heightHalf;
        pos.z = 0;

        return new Promise(resolve => resolve(pos));
    }

    protected getContainer(config){

        let query = '.customizer-canvas-wrapper';
        if(isDefined(config.pinsContainer)){
            query = config.pinsContainer;
        }

        return document.querySelectorAll(query)[0] as HTMLElement;
    }

    protected async createHtml(){

        if(this.pinsData.length === 0) return;

        for(let i=0;i<this.pinsData.length;i++){
            let pin = await this.createPin(this.pinsData[i]);
            this.addToContainer(pin);
        }

        this.updatePinsOnControlsChange();
    }

    // TODO - on camera/controls update - update pins position too

    protected async updatePinsPositions(force=false){

        if(force === false){
            this.ticks.position += this.clock.getDelta();
            if( this.ticks.position < 0.05 ) return; // if not performant, increase numeric value
        }

        for(let p in this.elements){

            let pin = this.elements[p];
            let pos = await this.toScreenPosition(pin.object) as any;

            pin.outer.style.left = pos.x + 'px';
            pin.outer.style.top = pos.y + 'px';
        }

        this.ticks.position = 0;

    }

    protected async updateAllPinsColor(){

        const checkedData = await Root.instance.events.call(EVENT_CHECK_COLOR_DATA, null, true) as any;

        for(let groupKey in checkedData){
            let color = checkedData[groupKey].color;
            this.updatePinColor(groupKey, color);
        }

    }

    protected updatePinColor(groupKey, color){

        if( ! isDefined(this.elements[groupKey]) ){
            console.warn('~~~ updatePinColor - NOT DEFIEND', groupKey, this.elements);    
            return;
        }

        let pin = this.elements[groupKey];

        for(let i=0;i<pin.colors.length;i++){
            if(pin.colors[i].itemGroup == groupKey){
                pin.colors[i].element.style.backgroundColor = color;
            }
        }


    }

    protected updatePinsOnControlsChange(){

        Root.instance.events.call(EVENT_AFTER_CONTROLS_UPDATE, null, null, this.updatePinsPositions.bind(this));        

    }

    protected addToContainer(pin){
        this.container.appendChild(pin);
    }

    // TODO - assign to global variable for later access
    protected async createPin(pin){

        let outer = document.createElement('div');
        outer.setAttribute('class','customizer-pins--wrapper');
        outer.dataset.pin = pin.key; // data-pin
        outer.setAttribute('data-key', pin.key);
        outer.addEventListener('click', ()=>{
            console.log('PIN CLICK', pin);
            Root.instance.events.broadcast(EVENT_PIN_SELECT, pin.key);
        });
        if(pin.config.invisible === true){
            outer.setAttribute('class','customizer-pins--invisible');
        }

        let inner = document.createElement('div');
        inner.setAttribute('class', 'customizer-pins--color-group');

        let textWrapper = document.createElement('div');
        textWrapper.setAttribute('class','customizer-pins--text-wrapper');
        textWrapper.innerHTML = pin.name;

        this.elements[pin.key] = {
            ...pin, 
            outer: outer,
            inner: inner,
            colors: [],
            object: this.invisibleObjectAt(pin.config.position),
        };

        let countGroups = pin.group.length;

       

        for(let g=0;g<countGroups;g++){

            let color = document.createElement('div');
            color.setAttribute('class','customizer-pins--color-item');

            
            
            inner.appendChild(color);

            this.elements[pin.key].colors.push({
                itemGroup: pin.group[g],
                element: color,
            });

        }

        outer.appendChild(inner);
        outer.appendChild(textWrapper);
        
        return new Promise(resolve => resolve(outer));
    }

    // object position will be followed by pin 2D
    protected invisibleObjectAt(pos){

        let size = 2;

        var geometry = new THREE.BoxGeometry( size, size, size );
        var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
        var cube = new THREE.Mesh( geometry, material );
        cube.position.set(pos[0], pos[1], pos[2]);
        cube.visible = false;

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, cube);

        return cube;
    }

    /*
        args
        [0] - pin key

        returns vector3
    */
    protected async getPinPosition(...args){

        const key = args[0];

        let position = undefined;
        let pin = this.pinsData.find(item => item.key == key);

        if(pin != undefined){
            position = new THREE.Vector3(pin.position[0], pin.position[1], pin.position[2]);
        }

        return new Promise(resolve => resolve(position))
    }

    protected async getPinConfig(...args){
        
        const key = args[0];
        let pin = this.pinsData.find(item => item.key == key);
        return new Promise(resolve => resolve(pin))
    }
    
    protected async triggerZoomOut(){

        console.log('triggerZoomOut');

        this.state = STATE_ZOOMING_OUT;
        this.locked = false;
        this.show();

        Root.instance.events.call(EVENT_CAMERA_RESET_ANIMATION, null, null);
    }

    protected async triggerZoomIn(groupKey){

        // let pinPosition = await this.getPinPosition(groupKey) as THREE.Vector3;
        let pin = await this.getPinConfig(groupKey) as any;

        if(!isDefined(pin)){
            Root.instance.events.call(EVENT_CAMERA_RESET_ANIMATION, null, null);
            return;
        }

        let pinPosition = new THREE.Vector3(pin.config.position[0], pin.config.position[1], pin.config.position[2]);
        let endCameraPos;
        let prevState = this.state;
        this.state = STATE_ZOOMING_IN;

        if(pin.config.view){
            endCameraPos = new THREE.Vector3(pin.config.view[0], pin.config.view[1], pin.config.view[2]);
        }else{
            endCameraPos = this.calculateCameraPositionInDistance(pinPosition, pin.distance);
        }

        let options : any = {
            transition : prevState == STATE_ZOOMING_IN ? 'direct' : '', // between means in between pins' transition
        }

        this.locked = true;
        this.hide();
        Root.instance.events.call(EVENT_CAMERA_ANIMATION, null, null, endCameraPos, pinPosition, options);

    }


    protected calculateCameraPositionInDistance(pinPosition, distance){

        let position0 = new THREE.Vector3().copy(this.camera.position);
        let position1 = new THREE.Vector3().copy(pinPosition);
        let dist = position0.distanceTo(position1);
        let viewDist = dist - (distance ? distance : this.config.zoomDistance);

        return getPointInBetweenByValue(position0, position1, viewDist ); 
    }
  

    protected admin(){


        console.log('localhost?', isLocalhost() );

        if( ! isLocalhost() ) return;

        const pinsAdmin = new PinsAdmin();
    }

}