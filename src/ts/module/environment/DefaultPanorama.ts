import { Root } from 'Root';
import { EVENT_GET_THREE_CAMERA, EVENT_PRELOADER_FINISHED, EVENT_ADD_TO_SCENE, TYPE_TEXTURE, EVENT_GET_THREE_SCENE } from 'helpers/constants';
import * as THREE from 'three';

export class DefaultPanorama{

    private textures : any = {};
    private loadedTextures : any = {};

    constructor(){


        this.init();
    }

    protected init(){

        this.modifyCamera();

         this.textures.panorama = `environment.jpg`;

        // push textures to list
        for(let name in this.textures){
            Root.instance.preloader.add(name, this.textures[name], TYPE_TEXTURE, true);
        }

        // once textures are loaded - create panorama
        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.texturesLoaded.bind(this));

    }

    protected texturesLoaded(...args){

        this.loadedTextures = args[0];

        this.applyFog();
        this.changeBackground();
        this.createPanorama();
    }

    protected applyFog(){

        Root.instance.events.call(EVENT_GET_THREE_SCENE, null, (_scene)=>{

            _scene.fog = new THREE.Fog( 0xccecff, 500, 10000 );

        });

       
    }

    protected changeBackground(){

        Root.instance.events.call(EVENT_GET_THREE_SCENE, null, (_scene)=>{

            _scene.background = new THREE.Color (0xccecff);

        });
    }

    protected createPanorama(){

       
        var geometry = new THREE.SphereBufferGeometry( 15000, 60, 40 );
        geometry.scale( - 1, 1, 1 );   // invert the geometry on the x-axis so that all of the faces point inward

        var material = new THREE.MeshBasicMaterial( { 
            map: this.loadedTextures.panorama 
        } );

        const mesh = new THREE.Mesh( geometry, material );
        mesh.rotation.set( 0, - Math.PI, 0 );
        
        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, mesh);

    }

    protected async modifyCamera(){

        let camera;

        await Root.instance.events.call(EVENT_GET_THREE_CAMERA, null, (_camera) =>{
            camera = _camera;
        });

        camera.far = 50000;

        return true;
    }
}