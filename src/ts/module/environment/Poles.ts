import { Root } from 'Root';
import { EVENT_GET_MODEL_DIMENSIONS, EVENT_GET_GROUND_CONFIG,  TYPE_TEXTURE, EVENT_ADD_TO_SCENE, EVENT_PRELOADER_LIST } from 'helpers/constants';
import * as THREE from 'three';

export class Poles{ // for calamigos sweater and blanket
 

    // TODO - offset{ x,y,z }

    protected config: any = {
        width: 270, // need width
        size: 5, // diameter of pole
        offsetY: 0, // if product should be above horizontal pole, like if its a sweater and its hanging above top pole
        offsetZ: 0,
        texture: 'wood_texture.jpg',
        textureName: 'pole',
    };

    // TODO - product configuration should have -4.6 offsetY value for pole

    constructor(_config={}){

        this.parsePoleConfig(_config);
        this.prepare();
    }

    public parsePoleConfig(_config){

        if(Object.keys(_config).length === 0) return;

        for (let param in _config) {
            this.config[param] = _config[param];
        }
    }

    protected prepare(){

        Root.instance.preloader.add(this.config.textureName, this.config.texture, TYPE_TEXTURE, true);
    }


    public async init(){

        let loadedTextures;
        await Root.instance.events.call(EVENT_PRELOADER_LIST, null, (_loadedTextures)=>{
            loadedTextures = _loadedTextures;
        });

        const dimensions = await Root.instance.events.call(EVENT_GET_MODEL_DIMENSIONS, null, true);
        const groundConfig = await Root.instance.events.call(EVENT_GET_GROUND_CONFIG, null, true);

        if( ! loadedTextures.hasOwnProperty(this.config.textureName)){
            console.warn('Poles texture not loaded!', this.config.textureName, loadedTextures);
            return;
        }

        let poleTexture = loadedTextures[this.config.textureName];
       
        const poles = this.create(dimensions, groundConfig, poleTexture);

        Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, poles);

    }

    protected create(dimensions, groundConfig, poleTexture){

        const groundY = groundConfig.y || 0;

        const horizontalPoleY = dimensions.center.y + (dimensions.size.y / 2) + this.config.offsetY;
        const posZ = this.config.offsetZ || 0;
        const poleHeight = horizontalPoleY - groundY + this.config.size;


        const vertPoleY = horizontalPoleY - poleHeight/2 + this.config.size/2;

        const poles = new THREE.Group();

        const poleGeo = new THREE.BoxBufferGeometry(5, poleHeight, 5);

        const poleMat = new THREE.MeshStandardMaterial({
            map: poleTexture,
            color: 0xffffff,
            metalness:0.7,
            roughness:0.3
        });

        const castShadow = true;
        const receiveShadow = true;
        

        const horizontalPole = new THREE.Mesh(new THREE.BoxBufferGeometry(this.config.width - this.config.size, this.config.size, this.config.size), poleMat);
        horizontalPole.position.x = 0;
        horizontalPole.receiveShadow = receiveShadow;
        horizontalPole.castShadow = castShadow;
        horizontalPole.position.y = horizontalPoleY;
        
        const leftPole = new THREE.Mesh(poleGeo, poleMat);
        leftPole.position.x = - this.config.width / 2;
        leftPole.receiveShadow = receiveShadow;
        leftPole.castShadow = castShadow;
        leftPole.position.y = vertPoleY;
        
        const rightPole = new THREE.Mesh(poleGeo, poleMat);
        rightPole.position.x = this.config.width / 2;
        rightPole.receiveShadow = receiveShadow;
        rightPole.castShadow = castShadow;
        rightPole.position.y = vertPoleY;

        const bottomSize = 10;
        const poleBottom = new THREE.BoxBufferGeometry(bottomSize, bottomSize, bottomSize);
        const poleBottomY = groundY;

        const poleStandBottomRight = new THREE.Mesh(poleBottom, poleMat);
        poleStandBottomRight.position.x = this.config.width / 2;
        poleStandBottomRight.position.y = poleBottomY;
        poleStandBottomRight.receiveShadow = true;
        poleStandBottomRight.castShadow = true;

        const poleStandBottomLeft = new THREE.Mesh(poleBottom, poleMat);
        poleStandBottomLeft.position.x = - this.config.width / 2;
        poleStandBottomLeft.position.y = poleBottomY;
        poleStandBottomLeft.receiveShadow = true;
        poleStandBottomLeft.castShadow = true;

        poles.add(horizontalPole);
        poles.add(leftPole);
        poles.add(rightPole);
        poles.add(poleStandBottomRight);
        poles.add(poleStandBottomLeft);

        poles.position.z = posZ;

        return poles;

    }

}