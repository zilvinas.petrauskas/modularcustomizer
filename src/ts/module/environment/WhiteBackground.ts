import { Root } from 'Root';
import { EVENT_PRELOADER_FINISHED, EVENT_ADD_TO_SCENE, EVENT_GET_THREE_SCENE, EVENT_GET_THREE_CAMERA } from 'helpers/constants';
import * as THREE from 'three';

const GRADIENT_VERTEXT = require('component/glsl/GradientVertextShader.vert').default;
const GRADIENT_FRAGMENT = require('component/glsl/GradientFragmentShader.frag').default;

export class WhiteBackground{

    protected background;
    protected loadedTextures;

    constructor(){

        Root.instance.events.listen(EVENT_PRELOADER_FINISHED, this.start.bind(this));
    }

    protected start(){

        this.gradient();

    }

    protected gradient(){

        const uniforms = {
            "color1" : {
              type : "c",
              value : new THREE.Color(0xfffbf2), // fffdfa, 0xfffbf2
            },
            "color2" : {
              type : "c",
              value : new THREE.Color(0xe0e0e0), // 0xCCCCCC 0xe0e0e0 0xe8e8e8
            },
          };

          const material = new THREE.ShaderMaterial({
            uniforms: uniforms,
            vertexShader: GRADIENT_VERTEXT,
            fragmentShader: GRADIENT_FRAGMENT,
            side: THREE.DoubleSide,
          });

          const geometry = new THREE.SphereGeometry( 2000, 32, 32 ); // TODO max zoom distance
          var sphere = new THREE.Mesh( geometry, material );

          Root.instance.events.call(EVENT_ADD_TO_SCENE, null, null, sphere);

    }

}
