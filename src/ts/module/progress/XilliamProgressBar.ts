import { ProgressBar } from './ProgressBar';

export class XilliamProgressBar extends ProgressBar{

    constructor(_config){
        super(_config);
    }

    protected delayOnComplete(){
        return 1;
    }

}
