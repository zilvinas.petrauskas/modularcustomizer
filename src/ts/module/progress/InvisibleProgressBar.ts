import { ProgressBar } from './ProgressBar';
import { Root } from 'Root';
import { EVENT_CUSTOMIZER_READY } from 'helpers/constants';


// used for Xilliam demo
export class InvisibleProgressBar extends ProgressBar{

    constructor(_config){
        super(_config);
    }

    protected delayOnComplete(){
        return 1000;
    }

    protected onComplete(){
        // trigger global event to hide text sequance
        
        setTimeout(() => {

            console.log('--- customizer:ready called')
            window.dispatchEvent(new Event('customizer:ready'));

            Root.instance.events.call(EVENT_CUSTOMIZER_READY, null, null); // show customizer
            this.hide();  // hide progress bar
            
        }, this.delayOnComplete()); // give a moment for canvas to start rendering
    }
}
