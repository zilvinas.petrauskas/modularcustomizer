import { Root } from 'Root';
import { EVENT_CUSTOMIZER_READY, EVENT_CUSTOMIZER_LOADING, EVENT_STATE_LOADING, EVENT_STATE_VISIBLE, EVENT_CUSTOMIZER_VISIBLE } from 'helpers/constants';
import { DefaultProgressBar } from './DefaultProgressBar';

export class ProgressBar extends DefaultProgressBar{

    protected config: any = {
        initial: 50, // just load to percentage without waiting for code
        preloader: 40, // shared between all preloader items
        product: 5, // after creation of product
        ui: 5, // after creation of ui
    }

    protected increase: any = {
        initial: 1,
        preloader: 1, // calculate based of queue length in preloader
        product: 5,
        ui: 5,
    }

    protected updateTexts : boolean = false; // if texts in progress should be updated

    protected texts = {
        // 0 : 'Loading...',
        // 50: 'Half way thru!',
        // 75: 'Almost done..',
        // 90: 'Get ready!',
    }
    
    constructor(_config?){
        super(_config);

        // Root.instance.events.listen(EVENT_STATE_VISIBLE, this.hide.bind(this));

        

        // when customizer visible - progress bar hidden
        Root.instance.events.listen(EVENT_CUSTOMIZER_VISIBLE, this.hide.bind(this) );
    }

    protected show() {

        if( Root.instance.events.exists(EVENT_STATE_LOADING) ){
            Root.instance.events.call(EVENT_STATE_LOADING, null, null);
        }else{
            document.getElementsByTagName('html')[0].classList.add('customizer-loading');
        }

    }

    protected hide(){
        
        document.getElementsByTagName('html')[0].classList.remove('customizer-loading');
    }


    /*
        TODO - onComplete call EVENT_STATE_READY - for state manager to handle thigns afterwards
    */
    protected onComplete(){
       
        // trigger inits that requires preloader, but dont show customizer yet
        Root.instance.events.call(EVENT_CUSTOMIZER_READY, null, null);       

        // setTimeout(() => {
        //     this.hide();  // hide progress bar
        // }, this.delayOnComplete()); // give a moment for canvas to start rendering

       

    }

    // protected delayOnComplete(){
        // return 1337;
    // }
    

}