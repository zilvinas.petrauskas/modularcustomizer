import { DefaultProgressBar } from './DefaultProgressBar';
import { Root } from 'Root';
import {  EVENT_HIDE_CUSTOMIZER, EVENT_OUTRO_START, EVENT_OUTRO_ADD } from 'helpers/constants';

export class Outro extends DefaultProgressBar{

    protected config: any = {
        initial: 60, 
    }

    protected increase: any = {
        initial: 0,
        cart: 20, 
        image: 20,
    }

    protected updateTexts : boolean = true; // if texts in progress should be updated

    protected texts = {
        0 : 'Creating Your Garment',
        60: 'Applying Colors',
    }
    
    constructor(_config?){
        super(_config);
    }

    protected listen(){

        Root.instance.events.register(EVENT_OUTRO_START, this.start.bind(this));
        Root.instance.events.register(EVENT_OUTRO_ADD, this.add.bind(this));

    }

    // TODO - global LOCK while outro in progress

    protected show(){

        document.getElementsByTagName('html')[0].classList.add('customizer-ending');
        
    }

    protected hide(){
        
        document.getElementsByTagName('html')[0].classList.remove('customizer-ending');
        Root.instance.events.call(EVENT_HIDE_CUSTOMIZER, null, null);

        setTimeout(()=>{
            this.reset();
        }, 500);

    }

    protected onComplete(){
       
        setTimeout(() => {

            // hide customizer: ui + canvas
            this.hide();  // hide progress bar
            
        }, 1);

       

    }

}