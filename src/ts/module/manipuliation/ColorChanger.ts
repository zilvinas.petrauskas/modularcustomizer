
export class ColorChanger {
    

    /*
        FLOW

        - ui triggers an event that _color_ on a _part_name_ has changed
        - color changer catches _color_ and _part_name_
        - color changers gets a part (three.js model) from Product class thru event trigger, changes its texture
        - triggers an event to update model part with new texture

    */

}