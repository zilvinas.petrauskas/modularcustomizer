import { Root } from 'Root';
import { EVENT_STATE_READY, EVENT_SILENT_LOAD, EVENT_STATE_LOADING, EVENT_STATE_VISIBLE, EVENT_STATE_OUTRO, EVENT_CUSTOMIZER_VISIBLE, EVENT_CUSTOMIZER_READY, EVENT_RESUME_CUSTOMIZER } from 'helpers/constants';
import { isDefined } from 'helpers/helpers';

export class StateManager{
    
    private states : any;
    private currentState : number = 0;
    private done: boolean;
    private silent : boolean = false;
    private showCustomizer : boolean = false;

    constructor(){

        this.parseConfig();

        this.done = false;
        this.init();
        this.listen();

        if( ! this.silent ) this.scrollTop();
    }

    protected parseConfig(){

        let websiteConfig = Root.instance.config.website.get();

        if(isDefined(websiteConfig)){

            if(websiteConfig.hasOwnProperty('silentMode') && websiteConfig.silentMode === true){
                console.log('- Silent mode - ON');
                this.silent = true;
            }

           
        }

    }

    protected parseParams(params?){
        if(isDefined(params)){
            for(let key in params){
                if(this.hasOwnProperty(key)){
                    this[key] = params[key];
                }
            }
        }
    }

    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart
    // TODO - if state is at OUTRO - do not allow color change or add to cart

    // TODO - check if preloader is initiated, if not, this will work as a fallback preloader (not visual) for launching things

    protected init(){

        this.states = {
            LOADING: 1,
            READY: 2,
            VISIBLE : 3,
            OUTRO: 9
        }
        
    }

    protected listen(){

        Root.instance.events.register(EVENT_SILENT_LOAD, this.isSilentLoad.bind(this));

        Root.instance.events.register(EVENT_STATE_LOADING, this.customizerLoading.bind(this));

        // TODO - when READY is called, register what is being loaded - product, textures, environment,etc and wait for their complete callback, then cal VISIBLE state

        Root.instance.events.register(EVENT_STATE_READY, this.customizerReady.bind(this));
        Root.instance.events.register(EVENT_CUSTOMIZER_READY, this.customizerReady.bind(this));

        Root.instance.events.register(EVENT_STATE_VISIBLE, this.customizerVisible.bind(this));

        // Root.instance.events.register(EVENT_STATE_OUTRO, this.TODO_OUTRO_STATE.bind(this));

        Root.instance.events.register(EVENT_RESUME_CUSTOMIZER, this.customizerResume.bind(this));

       

    }

    // triggered by progress bar if customizer is still loading
    public customizerLoading(){

        if(this.currentState < this.states.LOADING) this.currentState = this.states.LOADING

        if( ! this.silent ){
            // .customizer-loading
           this.addLoadingClassName();
        }
    }
    
    protected addLoadingClassName(){
        document.getElementsByTagName('html')[0].classList.add('customizer-loading');
    }

    // triggered by progress bar when loading is complete, but customizer might not be visible (if in silent mode)
    public customizerReady(){

        let delay = 1; // TODO - delay within config ?

        if(this.currentState === this.states.LOADING){
             delay = this.visibilityDelay(); // give time for threejs to create thigns TEMP solution for now
             this.currentState = this.states.READY;
        }

        if( this.showCustomizer || ! this.silent ){
            // .customizer-loaded

            this.currentState = this.states.VISIBLE;

            setTimeout(()=>{
                Root.instance.events.call(EVENT_CUSTOMIZER_VISIBLE, null, null);
            }, delay);

          
        }
    }

    // show customizer 
    public customizerVisible(){
        
        this.scrollTop();
        this.showCustomizer = true;
        this.customizerReady();

    }

    public customizerResume(...args){

        let params = args[0] || {};

        this.scrollTop();

        if(this.currentState === this.states.LOADING){

            if(params.hasOwnProperty('silent')) this.silent = params.silent;
            else this.silent = false;

            this.addLoadingClassName();

        }else if(this.currentState === this.states.READY || this.currentState === this.states.VISIBLE){

            if(params.hasOwnProperty('silent')) this.silent = params.silent;
            this.customizerVisible();

        }

    }

    protected visibilityDelay(){
        return 1337;
    }

    protected scrollTop(){
        window.scrollTo(0,0);
    }


    protected isSilentLoad(){
        return this.silent;
    }

    public isReady(){
        return this.done;
    }
    
}